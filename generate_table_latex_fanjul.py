from util import thebest


def read_file(job, machine, num, inst):
    name_file = f'result_2021/fanjul/{job}_{machine}_{num}_{inst}.csv'
    file = open(name_file)
    lines = [l[:-1].split(';') for l in file.readlines()]
    file.close()
    return lines[0]


# def get_values(values):
#     best = min(values)
#     n = len(values)
#     average = sum(values)/n
#     sum_desvio = sum([pow(x-average, 2) for x in values])/n
#     standard = pow(sum_desvio/n, 0.5)
#     return best, average, standard

def table_big_instances():
    default_top_1 = (
        "\\begin{table}[!ht]\n"
        "    \\centering\n"
        "    \\caption{Objetivo, LB, \emph{gap} e média de tempo (em segundos) da execução de "
        "\\cite{fanjul2019reformulations} para as instâncias com "
    )
    default_top_2 = (
        "}\n"
        "    \\scalebox{0.9}{\n"
        "        \\begin{tabular}{lrrrr}\n"
        "            \\toprule\n"
        "            Instance & Valor & LB & \\emph{gap}(\\%) & Tempo (segundos) \\\\\n"
        "            \\midrule\n"
    )
    default_bottom_1 = (
        "            \\midrule\n"
        "            {media:28} &      &         & {standard_gap:7,.2f} & {standard_time:8,.2f} \\\\\n"
        "            \\bottomrule\n"
    )
    default_bottom_2 = (
        "        \\end{tabular}\n"
        "    }\n"
        "    \\label{tab:"
    )
    label = "total_fanjul_f_{job}_{machine}"
    default_bottom_3 = (
        "}\n"
        "\\end{table}\n"
    )
    caption = "${job}$ tarefas e ${machine}$ máquinas."
    # jobs = [50, 100, 150, 200, 250]
    # machines = [10, 15, 20, 25, 30]
    jobs = [6, 8, 10, 12]
    machines = [2, 3, 4, 5]
    nums = [9, 49, 99, 124]
    instances = [i+1 for i in range(10)]
    format_instance = 'I\\_{job}\\_{machine}\\_S\\_1-{num}\\_{inst}'
    format_line = '            {name_instance:28} & {best:4d} & {lb:7,.2f} & {gap:7.2f} & {time:8,.2f} \\\\'
    # literature = thebest()
    for job in jobs:
        for machine in machines:
            string_caption = caption.format(job=job, machine=machine)
            string_file = ''.join([default_top_1, string_caption, default_top_2])
            sum_time = 0
            sum_gap = 0
            num_instance = 0
            for num in nums:
                for inst in instances:
                    lines = read_file(job, machine, num, inst)
                    objective, lb, time = int(lines[0]), float(lines[2]), float(lines[3])
                    lb = lb if lb <= objective else objective
                    # print(objective, lb, time)
                    sum_time += time
                    num_instance += 1
                    name_instance  = format_instance.format(
                        job=job,
                        machine=machine,
                        num=num,
                        inst=inst
                    )
                    # name = name_instance.replace('\\','')
                    # the_best = literature[name]
                    gap = (objective - lb)/objective*100
                    sum_gap += gap
                    line = format_line.format(
                        name_instance=name_instance,
                        best=objective,
                        lb=lb,
                        gap=gap,
                        time=time,
                    )
                    string_file += line + '\n'
            average_time = sum_time/num_instance
            average_gap = sum_gap/num_instance
            bottom = default_bottom_1.format(
                media='\\textbf{Média}',
                standard_time=average_time,
                standard_gap=average_gap
            )
            string_label = label.format(job=job, machine=machine)
            string_file+=''.join([bottom, default_bottom_2, string_label, default_bottom_3])
            print(string_file)


def table_small_instances():
    jobs = [6, 8, 10, 12]
    machines = [2, 3, 4, 5]
    nums = [9, 49, 99, 124]
    instances = [i+1 for i in range(10)]
    format_instance = '{job}_{machine}'
    format_line = ''
    media_gap = {}
    media_time = {}
    for job in jobs:
        for machine in machines:
            name = format_instance.format(job=job, machine=machine)
            media_gap[name] = 0
            media_time[name] = 0
            counter = 0
            for num in nums:
                for inst in instances:
                    counter += 1
                    lines = read_file(job, machine, num, inst)
                    objective, lb, time = int(lines[0]),  float(lines[2]), float(lines[3])
                    lb = lb if lb <= objective else objective
                    gap = (objective - lb)/objective*100
                    media_gap[name] += gap
                    media_time[name] += time
                    # print(job, machine, num, inst, objective, lb, gap, time)
            media_gap[name] = media_gap[name]/counter
            media_time[name] = media_time[name]/counter
    counter = 1
    sum_gap = 0
    sum_time = 0
    for job in jobs:
        for machine in machines:
            name = format_instance.format(job=job, machine=machine)
            sum_gap += media_gap[name]
            sum_time += media_time[name]
            print(counter, name, f'{media_gap[name]:5.2f}', f'{media_time[name]:8,.2f}')
            counter += 1
        print()
    media_gap_t = sum_gap/counter
    media_time_t = sum_time/counter
    print(None, 'Média', f'{media_gap_t:5.2f}', f'{media_time_t:5.2f}')


if __name__ == '__main__':
    table_small_instances()
    # table_big_instances()