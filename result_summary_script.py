import pandas as pd

method = "%s"
run_seeds = [0, 1, 2, 3, 4]
run_time = 3600
for i in run_seeds:
    file_names = [
              "logs_%s_%ds_%d/I_50_10_S_1-124_5.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_50_15_S_1-99_4.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_50_20_S_1-49_5.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_50_25_S_1-9_4.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_50_30_S_1-9_4.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_100_10_S_1-124_5.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_100_15_S_1-49_4.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_100_20_S_1-124_6.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_100_25_S_1-124_8.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_100_30_S_1-9_8.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_150_10_S_1-99_9.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_150_15_S_1-49_6.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_150_20_S_1-9_1.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_150_25_S_1-124_7.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_150_30_S_1-9_1.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_200_10_S_1-49_6.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_200_15_S_1-99_1.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_200_20_S_1-124_2.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_200_25_S_1-49_5.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_200_30_S_1-124_2.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_250_10_S_1-124_9.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_250_15_S_1-9_8.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_250_20_S_1-49_9.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_250_25_S_1-124_8.txt" % (method, run_time, i),
              "logs_%s_%ds_%d/I_250_30_S_1-49_1.txt" % (method, run_time, i),
              ]

    print("Generating logs_%s_%ds_%d/logs_%s_%ds_%d.csv" % (method, run_time, i, run_time, i))

    res = []

    for file_name in file_names:
        print(file_name)
        file = open(file_name)
        lines = file.readlines()
        file.close()
        init_soln_cost = None
        init_soln_time = None
        fixopt_soln_cost = 9999999
        fixopt_soln_time = 9999999
        for line in lines:
            #if len(line) < 5 or line[4] != '|':
                #Ignore line
            if len(line) > 63 and line[63] == '*':
                init_soln_cost = int(line[39:47])
                init_soln_time = float(line[50:60].strip())
            if len(line) > 84 and line[9] != 'M' and line[9] != '-' and line[84] == '|':
                fixopt_soln_cost_line = int(line[6:19])
                if fixopt_soln_cost_line < fixopt_soln_cost:
                    fixopt_soln_cost = fixopt_soln_cost_line
                    fixopt_soln_time = float(line[70:83].strip()) + 360
        if fixopt_soln_cost >= init_soln_cost:
            fixopt_soln_time = init_soln_time

        res.append([init_soln_cost, init_soln_time, fixopt_soln_cost, fixopt_soln_time])
        print(init_soln_cost, init_soln_time, fixopt_soln_cost, fixopt_soln_time)

    pd.DataFrame(res).to_csv("logs_%s_%ds_%d/logs_%s_%ds_%d.csv" % (method, run_time, i, run_time, i), sep=";", header=None, index=None)
    print("Generated successfully!")