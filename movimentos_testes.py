from problem import Problem
from solution import Solution
from heuristic import construtive
from random import randint

def func():
    arq = '../code_master2/instancia/I_50_30_S_1-124_2.txt'
    problem = Problem(directory = arq)
    solution = construtive(problem)
    #solution.print()
    realoc = True
    #print('=========================')
    for _ in range(1000):
        '''
        m1 = randint(0, problem.M-1)
        while solution.tam_machine[m1] == 0 : 
            m1 = randint(0, problem.M-1)
        p1 = randint(0, solution.tam_machine[m1]-1)
        m2 = randint(0, problem.M-1)
        p2 = randint(0, max(0, solution.tam_machine[m2]-1))
        while (m1, p1) == (m2, p2) or (solution.tam_machine[m2] == 0 and not realoc) :
            m2 = randint(0, problem.M-1)
            p2 = randint(0, max(0, solution.tam_machine[m2]-1))
        #print('%d : %d'%(m1, solution.tam_machine[m1]))
        #print('%d : %d'%(m2, solution.tam_machine[m2]))
        solution_ = solution.copy()
        if realoc :
            #print('(%d, %d) => (%d, %d)'%(m1, p1, m2, p2))
            solution.mv_realocate(m1, p1, m2, p2)
        else :
            #print('(%d, %d) <> (%d, %d)'%(m1, p1, m2, p2))
            solution.mv_switch(m1, p1, m2, p2)
        '''
        # solution.print()
        # solution.todos_tempos()
        # input()
        solution_ = solution.copy()
        solution.perturbation()
        for i in problem.machines :
            if solution.cost[i] != solution.valor_tempo(i) :
                solution_.print()
                print('-----------')
                solution.print()
                print('-----------')
                break 
        realoc = not realoc 
        #print('=========================')


if __name__ == '__main__':
    func()