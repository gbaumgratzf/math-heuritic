counter = 0
for job in [50, 100, 150, 200, 250]:
    for machine in [10, 15, 20, 25, 30]:
        search = False
        for num in [9, 49, 99, 124]:
            for inst in list(range(1,11)):
                try:
                    instance = f'{job}_{machine}_{num}_{inst}'
                    file_result = f'resultado_ms/{instance}.csv'
                    result = open(file_result, 'r')
                    result_fix_opt = result.readlines()[-1][:-1]
                    elements = result_fix_opt.split(';')
                    objetive = elements[0]
                    lower_bound = elements[2]
                    time = elements[3]
                    print(f'{instance} | {objetive} | {lower_bound} | {time}')
                    search = True
                    counter += 1
                    break
                except FileNotFoundError:
                    continue
            if search:
                break
print(counter)