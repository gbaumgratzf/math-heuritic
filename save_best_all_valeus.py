import seaborn as sns
import pandas as pnd
import matplotlib.pyplot as plt

def save_results(name_exec, results):
    sub_string = [f'{key};{value}\n' for key, value in results.items()]
    name_file = f'the_best_{name_exec}.csv'
    file = open(name_file, 'w+')
    file.write(''.join(sub_string))
    file.close()


def save_results_2(name_exec, results):
    sub_string = [f'{key};{value[0]};{value[1]}\n' for key, value in results.items()]
    name_file = f'the_best_{name_exec}.csv'
    file = open(name_file, 'w+')
    file.write(''.join(sub_string))
    file.close()

def save_gap_and_time(name_exec, results):
    sub_string = [f'{key};{value[0], value[1]}\n' for key, value in results.items()]
    name_file = f'the_best_{name_exec}.csv'
    file = open(name_file, 'w+')
    file.write(''.join(sub_string))
    file.close()

def get_best_value(name_file, index):
    file = open(name_file)
    lines = [l[:-1].split(';') for l in file.readlines()]
    file.close()
    objectives = [int(line[index]) for line in lines]
    return min(objectives)


def get_gap_time(name_file):
    file = open(name_file)
    lines = [l[:-1].split(';') for l in file.readlines()][0]
    file.close()
    return [float(lines[0]), float(lines[2]), float(lines[3])]


def get_models_values_file(name_exec):
    name_file = f'the_best_{name_exec}.csv'
    file = open(name_file)
    lines = [l[:-1].split(';') for l in file.readlines()]
    file.close()
    results = {l[0]: [float(l[1]), float(l[2])] for l in lines}
    return results


def set_all_best_values(exec_name, index):
    jobs = [6, 8, 10, 12]
    machines = [2, 3, 4, 5]
    nums = [9, 49, 99, 124]
    instances = [i+1 for i in range(10)]
    result = {}
    for job in jobs:
        for machine in machines:
            for num in nums:
                for inst in instances:
                    path_name = f'result_2021/{exec_name}/{job}_{machine}_{num}_{inst}.csv'
                    try:
                        best = get_best_value(path_name, index)
                    except FileNotFoundError:
                        best = 10000
                        print(path_name)
                    format_instance = f'I_{job}_{machine}_S_1-{num}_{inst}'
                    result[format_instance] = best
    jobs = [50, 100, 150, 200, 250]
    machines = [10, 15, 20, 25, 30]
    for job in jobs:
        for machine in machines:
            for num in nums:
                for inst in instances:
                    path_name = f'result_2021/{exec_name}/{job}_{machine}_{num}_{inst}.csv'
                    try:
                        best = get_best_value(path_name, index)
                    except FileNotFoundError:
                        best = 10000, 0, 3600
                        print(path_name)
                    format_instance = f'I_{job}_{machine}_S_1-{num}_{inst}'
                    result[format_instance] = best
    save_results(exec_name, result)
    print(len(result))

def get_default_model_values(exec_name):
    jobs = [6, 8, 10, 12]
    machines = [2, 3, 4, 5]
    nums = [9, 49, 99, 124]
    instances = [i+1 for i in range(10)]
    result = {}
    for job in jobs:
        for machine in machines:
            for num in nums:
                for inst in instances:
                    path_name = f'result_2021/{exec_name}/{job}_{machine}_{num}_{inst}.csv'
                    try:
                        ub, lb, time = get_gap_time(path_name)
                        lb = lb if lb <= ub else ub
                    except FileNotFoundError:
                        ub, lb, time = 10000, 0, 3600
                        print(path_name)
                    format_instance = f'I_{job}_{machine}_S_1-{num}_{inst}'
                    result[format_instance] = get_gap(ub, lb), time
    jobs = [50, 100, 150, 200, 250]
    machines = [10, 15, 20, 25, 30]
    for job in jobs:
        for machine in machines:
            for num in nums:
                for inst in instances:
                    path_name = f'result_2021/{exec_name}/{job}_{machine}_{num}_{inst}.csv'
                    try:
                        ub, lb, time = get_gap_time(path_name)
                    except FileNotFoundError:
                        ub, lb, time = 10000, 0, 3600
                        print(path_name)
                    format_instance = f'I_{job}_{machine}_S_1-{num}_{inst}'
                    result[format_instance] = get_gap(ub, lb), time
    save_results_2(exec_name, result)
    print(len(result))

def get_solutions_sls(path):
    file = open(path, 'r')
    lines = [l[:-1] for l in file.readlines() if 'Total makespan:' in l]
    file.close()
    results = []
    for line in lines:
        _, _, value = line.split(' ')
        results += [int(value)]
    return results


def fix_all_sls_results():
    jobs = [50, 100, 150, 200, 250]
    machines = [10, 15, 20, 25, 30]
    nums = [9, 49, 99, 124]
    instances = [i+1 for i in range(10)]
    numbers = [1, 2]
    
    for job in jobs[:1]:
        for machine in machines[:1]:
            for num in nums:
                for inst in instances:
                    values = []
                    for number in numbers:
                        default_path = f'solution_2021/sls/spider/I_{job}_{machine}_S_1-{num}_{inst}_{number}.txt'
                        values += get_solutions_sls(default_path)
                    string = f'{job}_{machine}_{num}_{inst}'
                    equal_values = [values[0]==value for value in values[1:]]
                    if not all(equal_values):
                        print(string, values)
                    if len(values) < 5:
                        print(string, values)
                    # name_file = f'result_2021/sls/{job}_{machine}_{num}_{inst}.csv'
                    # write_string = ''
                    # default_string = '{value};3600.00;{value};0.00;0;3600.{value}\n'
                    # for value in values:
                    #     write_string += default_string.format(value=value)
                    # file = open(name_file, 'w+')
                    # file.write(write_string)
                    # file.close()


def get_best(name_exec) -> dict:
    file = open(f'the_best_{name_exec}.csv', 'r')
    lines = file.readlines()
    file.close()
    BKS = {}
    for line in lines:
        [instance, value] = line[:-1].split(';')
        BKS[instance] = int(value)
    return BKS

def get_gap(ub, lb):
    return (ub - lb)/ub * 100


def get_models_values():
    models = ['Avalos', 'Fanjul', 'Vallada', 'Rabadi']
    avalos = get_models_values_file('avalos')
    fanjul = get_models_values_file('fanjul')
    vallada = get_models_values_file('vallada')
    rabadi = get_models_values_file('rabadi')
    time = {name: [] for name in models}
    gap = {name: [] for name in models}
    counter = 0
    for key in avalos.keys():
        counter += 1
        time['Rabadi'] += [rabadi[key][1]]
        gap['Rabadi'] += [rabadi[key][0]]
        time['Vallada'] += [vallada[key][1]]
        gap['Vallada'] += [vallada[key][0]]
        time['Avalos'] += [avalos[key][1]]
        gap['Avalos'] += [avalos[key][0]]
        time['Fanjul'] += [fanjul[key][1]]
        gap['Fanjul'] += [fanjul[key][0]]
        print(f'{counter/640*100:3.2f}')
    x = pnd.DataFrame(time)
    y = pnd.DataFrame(gap)
    f, axes = plt.subplots(1, 2)
    sns.set_theme(style="whitegrid")
    sns.boxplot(data=x, ax=axes[1]).set(ylabel='time', xlabel='Metodo', yscale='log')
    sns.set_theme(style="whitegrid")
    sns.boxplot(data=y, ax=axes[0]).set(ylabel='gap', xlabel='Metodo')
    plt.show()


def box_plot_gap():
    names = ['Fanjul', 'SLS', 'FixOpt - Fanjul']
    print('Loading Fanjul!')
    fanjul = get_best('fanjul')
    print('Loading SLS!')
    sls = get_best('sls')
    print('Loading FixOpt!')
    fix_opt = get_best('fix_opt')
    print('Loading The Bests!')
    one = get_best('one')
    result = {name: [] for name in names}
    print('Generate Gaps')
    counter = 0
    for key in sls.keys():
        counter += 1
        best = one[key]
        result['Fanjul'] += [get_gap(fanjul[key], best)]
        result['SLS'] += [get_gap(sls[key], best)]
        result['FixOpt - Fanjul'] += [get_gap(fix_opt[key], best)]
        print(f'{counter/1000*100:3.2f}')
    x = pnd.DataFrame(result)
    # print(x)
    #custom_params = {"axes.spines.right": False, "axes.spines.top": False}
    sns.set_theme(style="whitegrid")
    sns.boxplot(data=x).set(ylabel='gap', xlabel='Metodo')
    plt.show()

def save_best_one():
    names = ['Fanjul', 'SLS', 'FixOpt - Fanjul']
    print('Loading Fanjul!')
    fanjul = get_best('fanjul')
    print(fanjul.keys())
    print('Loading SLS!')
    sls = get_best('sls')
    print(sls.keys())
    result = {}
    for key in sls.keys():
        result[key] = min(sls[key], fanjul[key])
    save_results('one', result)
    print(len(result))

def box_plot_gap_by_job():
    names = ['Fanjul', 'SLS', 'FixOpt - Fanjul']
    print('Loading Fanjul!')
    fanjul = get_best('fanjul')
    print('Loading SLS!')
    sls = get_best('sls')
    print('Loading FixOpt!')
    fix_opt = get_best('fix_opt')
    print('Loading The Bests!')
    one = get_best('one')


    jobs = [50, 100, 150, 200, 250]
    machines = [10, 15, 20, 25, 30]
    nums = [9, 49, 99, 124]
    instances = [i+1 for i in range(10)]
    result = {j: {m : {name : [] for name in names} for m in machines} for j in jobs}
    # result = {}
    for job in jobs:
        # result[job] = {}
        for machine in machines:
            for num in nums:
                for inst in instances:
                    key = f'I_{job}_{machine}_S_1-{num}_{inst}'
                    best = one[key]
                    result[job][machine][names[0]] += [get_gap(fanjul[key], best)]
                    result[job][machine][names[1]] += [get_gap(sls[key], best)]
                    result[job][machine][names[2]] += [get_gap(fix_opt[key], best)]

        # print(f'{counter/1000*100:3.2f}')
    
    # x_max = 1
    # y_max = 5
    # f, axes = plt.subplots(x_max, y_max)
    # print(x)
    #custom_params = {"axes.spines.right": False, "axes.spines.top": False}
    sns.set_theme(style="whitegrid")
    counter_y = 0
    job = 50
    counter_x = 0
    for job in jobs:
        x_max = 1
        y_max = 5
        f, axes = plt.subplots(x_max, y_max, sharex=True, sharey=True)
        plt.suptitle(f'{job}')
        print('----------------')
        for machine in machines:
            # machine = 10
            # r = [(k, len(v)) for k, v in result[job][machine].items()]
            # print(r)
            # import pytest; pytest.set_trace()
            x = pnd.DataFrame(result[job][machine])
            # print(x[job][machine])
            #sns.boxplot(data=result[job][machine], ax=axes[counter]) #.set(ylabel='gap', xlabel='Metodo')
            print(machine, counter_x, counter_y)
            if x_max == 1:
                ax = axes[counter_y]
            else:
                ax = axes[counter_x][counter_y]
            sns.boxplot(data=x, ax=ax).set(xlabel=f'{machine}')
            sns.set_theme(style="whitegrid")
            counter_y += 1
            if counter_y == y_max:
                counter_y = 0
                counter_x += 1
        # plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
        # plt.grid(False)
        # plt.xlabel('Machines')
        # plt.xlabel('gap')
        print('----------------')
    plt.show()

def box_plot_gap_by_machine():
    names = ['Fanjul', 'SLS', 'FixOpt - Fanjul']
    print('Loading Fanjul!')
    fanjul = get_best('fanjul')
    print('Loading SLS!')
    sls = get_best('sls')
    print('Loading FixOpt!')
    fix_opt = get_best('fix_opt')
    print('Loading The Bests!')
    one = get_best('one')


    jobs = [50, 100, 150, 200, 250]
    machines = [10, 15, 20, 25, 30]
    nums = [9, 49, 99, 124]
    instances = [i+1 for i in range(10)]
    result = {m: {j : {name : [] for name in names} for j in jobs} for m in machines}
    for machine in machines:
        for job in jobs:
            for num in nums:
                for inst in instances:
                    key = f'I_{job}_{machine}_S_1-{num}_{inst}'
                    best = one[key]
                    result[machine][job][names[0]] += [get_gap(fanjul[key], best)]
                    result[machine][job][names[1]] += [get_gap(sls[key], best)]
                    result[machine][job][names[2]] += [get_gap(fix_opt[key], best)]

        # print(f'{counter/1000*100:3.2f}')
    
    # x_max = 1
    # y_max = 5
    # f, axes = plt.subplots(x_max, y_max)
    # print(x)
    #custom_params = {"axes.spines.right": False, "axes.spines.top": False}
    sns.set_theme(style="whitegrid")
    counter_y = 0
    job = 50
    counter_x = 0
    for machine in machines:
        x_max = 1
        y_max = 5
        f, axes = plt.subplots(x_max, y_max, sharex=True, sharey=True)
        plt.suptitle(f'{machine}')
        print('----------------')
        for job in jobs:
            # machine = 10
            # r = [(k, len(v)) for k, v in result[job][machine].items()]
            # print(r)
            # import pytest; pytest.set_trace()
            x = pnd.DataFrame(result[machine][job])
            # print(x[job][machine])
            #sns.boxplot(data=result[job][machine], ax=axes[counter]) #.set(ylabel='gap', xlabel='Metodo')
            print(machine, counter_x, counter_y)
            if x_max == 1:
                ax = axes[counter_y]
            else:
                ax = axes[counter_x][counter_y]
            sns.boxplot(data=x, ax=ax).set(xlabel=f'{job}')
            sns.set_theme(style="whitegrid")
            counter_y += 1
            if counter_y == y_max:
                counter_y = 0
                counter_x += 1
        # plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
        # plt.grid(False)
        # plt.xlabel('Machines')
        # plt.xlabel('gap')
        print('----------------')
    plt.show()


def generate_table_fanjul_by_job_machine():
    fanjul = get_models_values_file('fanjul')
    counter = 0
    jobs = [50, 100, 150, 200, 250]
    machines = [10, 15, 20, 25, 30]
    nums = [9, 49, 99, 124]
    instances = [i+1 for i in range(10)]
    gaps = {j: {m : [] for m in machines} for j in jobs}
    times = {j: {m : [] for m in machines} for j in jobs}
    sum_gap = 0
    sum_time = 0
    sum_otimos = 0
    counter = 0
    for job in jobs:
        for machine in machines:
            otimos = 0
            for num in nums:
                for inst in instances:
                    key = f'I_{job}_{machine}_S_1-{num}_{inst}'
                    gaps[job][machine] += [fanjul[key][0]]
                    if fanjul[key][0] ==0.00:
                        otimos += 1
                    times[job][machine] += [fanjul[key][1]]
                    counter += 1
                    sum_gap += fanjul[key][0]
                    sum_time += fanjul[key][1]
            sum_otimos += otimos
            average_gap = sum(gaps[job][machine])/len(gaps[job][machine])
            average_time = sum(times[job][machine])/len(times[job][machine])
            print(f'{job}_{machine} & {average_gap:5.2f} & {average_time:8.2f} & {otimos:2d} \\\\')
    
    average_gap = sum_gap/counter
    average_time = sum_time/counter
    average_otimos = sum_otimos/counter
    print(f'Média & {average_gap:5.2f} & {average_time:8.2f} \\\\')
    print(f'Total & {sum_otimos:3d} \\\\')




if __name__ == '__main__':
    # ite = [('fanjul', 0), ('sls', 2), ('fix_opt', 2)]
    # for name, index in ite[:1]:
    #     print(name)
    #     set_all_best_values(name, index)
    # names = [i[0] for i in ite]
    # result = [get_best(name) for name in names]
    # results = {}
    # for key in result[0].keys():
    #     best = min([d[key] for d in result])
    #     results[key] = best
    # print(len(results))
    # save_results('one', results)
    # save_best_one()
    box_plot_gap()
    # box_plot_gap_by_job()
    # box_plot_gap_by_machine()
    # get_default_model_values('fanjul')
    # generate_table_fanjul_by_job_machine()