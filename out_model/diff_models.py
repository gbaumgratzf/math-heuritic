
MASTER_FILE = 'model_master.lp'
SEQUENCE_FILE = 'model_sequence_{}.lp'
SUB_MASTER_FILE = '_'.join(['sub', MASTER_FILE])
SUB_SEQUENCE_FILE = '_'.join(['sub', SEQUENCE_FILE])


def break_var(var: str) -> (str, str):
    i = 0
    name = ''
    str_mult = ''
    while i < len(var):
        elem = var[i]
        if elem in ['x', 'U']:
            name = var[i:]
            i = len(var)
        else:
            str_mult += var[i]
        i += 1

    if str_mult == '':
        mult = 1
    elif str_mult == '-':
        mult = -1
    else:
        mult = int(str_mult)

    return (name, mult)

def get_objective_and_constraints(lines):
    i = 0
    objective = ''
    while 'Minimize' not in lines[i]:
        i += 1
    i += 1
    while 'Subject To' not in lines[i]:
        objective  += lines[i]
        i += 1
    i += 1
    div_objective = objective.replace('\n', '').replace('\t', '').replace('  ', '').replace(' + ', '|')
    elements_objective = div_objective.split('|')

    tuple_objective = {}
    for elem in elements_objective:
        aux = elem.split(' ')
        if len(aux) == 1:
            tuple_objective[aux[0]] = 1
        elif len(aux) == 2:
            num = int(aux[0])
            tuple_objective[aux[1]] = num

    # print(objective)
    # print(tuple_objective)
    # print(elements_objective)
    constraint_list = []
    subject = -1
    while 'Bounds' not in lines[i]:
        line = lines[i].replace('\n', '').replace(': ', '|')
        line = line.replace(' + ', '+').replace(' = ', '=')
        line = line.replace(' <= ', '<').replace(' >= ', '>')
        line = line.replace(' - ', '+-')
        line = line.replace(' ', '')
        if ':' in lines[i]:
            constraint_list.append(line)
        else:
            constraint_list[subject] += line
        i += 1

    constraint = {}
    for subject in constraint_list:
        [name, const] = subject.split('|')
        signal = ''
        if '<' in const:
            signal = '<'
            [var_sum, value_total] = const.split('<')
        elif '>' in const:
            signal = '>'
            [var_sum, value_total] = const.split('>')
        elif '=' in const:
            signal = '='
            [var_sum, value_total] = const.split('=')

        list_var = var_sum.split('+')

        dict_const = {}
        for var in list_var:
            (name_var, mult_var) = break_var(var)
            dict_const[name_var] = mult_var

        constraint[name] = (signal, int(value_total), dict_const)
    return(tuple_objective, constraint)


if __name__ == '__main__':
    # Master
    file_name = SEQUENCE_FILE.format(0)
    print(f'File : {file_name}')
    file_master = open(file_name, 'r')
    lines_master = file_master.readlines()
    file_master.close()
    (objective_master, constraint_master) = get_objective_and_constraints(lines_master)
    # print(constraint_master)

    # SubMaster
    file_name = SUB_SEQUENCE_FILE.format(0)
    print(f'File : {file_name}')
    file_submaster = open(file_name, 'r')
    lines_submaster = file_submaster.readlines()
    file_submaster.close()
    (objective_submaster, constraint_submaster) = get_objective_and_constraints(lines_submaster)

    # print(constraint_submaster)
    error = {}

    # Diff master
    # for key in objective_master.keys():
    #     master = objective_master[key]
    #     submaster = objective_submaster[key]
    #     if master != submaster:
    #         error[key] = (master, submaster)

    # Diff constraint
    for key in constraint_master.keys():
        master = constraint_master[key]
        submaster = constraint_submaster[key]
        if master != submaster:
            error[key] = (master, submaster)

    counter_error = 0
    for key in error.keys():
        (m, sm) = error[key]
        # print(f'{key} : {m} <> {sm}')
        counter_error += 1

    print(f'Counter error : {counter_error}')
    print(f'Total constraint : {len(constraint_master.keys())}')


    signal_error = 'Erro: sinal incorreto.\n\t\tMain: {signal_m} - Sub : {signal_s}'
    total_error = 'Erro: total da equação.\n\t\tMain: {total_m} - Sub : {total_s}'
    var_error = 'Erro: variável não existente na equação.\n\t\tNome : {var} - Local que tem : {model}'
    mult_error = 'Erro: fator multiplicador incorreto na variável {var}.\n\t\tMain: {mult_m} - Sub : {mult_s}'

    error_nivel1 = {}
    for key in error.keys():
        error_list = []
        (error_m, error_s) = error[key]
        (signal_m, total_m, dict_var_m) = error_m
        (signal_s, total_s, dict_var_s) = error_s
        if signal_m != signal_s:
            msg_error = signal_error.format(
                signal_m=signal_m,
                signal_s=signal_s
            )
            error_list.append(msg_error)
        if total_m != total_s:
            msg_error = total_error.format(
                total_m=total_m,
                total_s=total_s
            )
            error_list.append(msg_error)

        no_var_s = [x for x in dict_var_m.keys() if x not in dict_var_s.keys()]
        for var in no_var_s:
            msg_error = var_error.format(
                var=var,
                model='MAIN'
            )
            error_list.append(msg_error)

        no_var_m = [x for x in dict_var_s.keys() if x not in dict_var_m.keys()]
        for var in no_var_m:
            msg_error = var_error.format(
                var=var,
                model='SUB'
            )
            error_list.append(msg_error)

        for var in dict_var_m.keys():
            if var not in no_var_s:
                mult_m = dict_var_m[var]
                mult_s = dict_var_s[var]
                if mult_m != mult_s:
                    msg_error = mult_error.format(
                        var=var,
                        mult_m=mult_m,
                        mult_s=mult_s
                    )
                    error_list.append(msg_error)

        error_nivel1[key] = error_list

    for key in error_nivel1.keys():
        print('------------------------------------------------')
        print(f'Constraint : {key}')
        print('Error list')
        for error in error_nivel1[key]:
            print(f'\t {error}')
        print('------------------------------------------------')
    # print(error['cycle_u_4_2'])
    # print(error_nivel1['cycle_u_4_2'])
