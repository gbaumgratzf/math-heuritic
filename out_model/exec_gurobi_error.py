from os import system

MASTER_FILE = 'model_master'
SEQUENCE_FILE = 'model_sequence_{}'
SUB_MASTER_FILE = '_'.join(['sub', MASTER_FILE])
SUB_SEQUENCE_FILE = '_'.join(['sub', SEQUENCE_FILE])

if __name__ == '__main__':
    comand = 'gurobi_cl TimeLimit=3600 LogFile=log.txt ResultFile={output}.ilp {input}.lp'

    # Commun exec - sequence
    full_command = comand.format(
        output=MASTER_FILE,
        input=MASTER_FILE
    )
    for i in range(0, 9):
        full_command = comand.format(
            output=SEQUENCE_FILE.format(i),
            input=SEQUENCE_FILE.format(i)
        )
        system(full_command)
    
    # When submodel - sequence
    full_command = comand.format(
        output=SUB_MASTER_FILE,
        input=SUB_MASTER_FILE
    )
    for i in range(0, 1):
        full_command = comand.format(
            output=SUB_SEQUENCE_FILE.format(i),
            input=SUB_SEQUENCE_FILE.format(i)
        )
        system(full_command)