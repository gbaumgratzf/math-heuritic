# Math-Heuristic to UPMSP

The algorithm to find the solution the **UPMSP** (*Unrelated Parallel Machine Scheduling Problem with Sequence Dependent and Setup Times*) when it is use a **math-heuristic**.

***In progress***

## Install

The project **required** install:

 - [Python](https://www.python.org/) >= 3.6
 - [Pip](https://pypi.org/project/pip/) >= 20.1.1
 - [MIP](https://python-mip.readthedocs.io/) >= 1.8.1

It is **recommend** use:

 - [Pypy](https://www.pypy.org/) >= 7.3.1
 - [Gurobi](https://www.gurobi.com/) >= 9.0
 - [Java](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html) >= 1.8.0_252

## Execute

To execute the project the command is:

```bash
pypy3 main_nw.py $DIR_INSTANCE $PARAMETERS
```

The **$DIR_INSTANCE** put the directory of instance and it is **required**.

### Parameters

The **$PARAMETERS** ins't required.

The parameters is:

 * *init_sol* : the initial solution to UPMSP. There is a three options:
    * *Greedy* : use greedy algorithm (create by myself).
    * *SLS* : use a Simulated Anneling (create by Toffolo and Santos) and use the default parameter of them. It need a minimal 70 seconds. **Required Java.**
    * Read a file. ***In progress*** 
 * *iterate* : the number of iteration without change the value in the best solution.
 * *exec_time* : the maximum time (in seconds) to MIP solver execute.
 * *max_jobs* : the number of the initial jobs to work in algorithm;
    * If *max_jobs* is low then amount of jobs on two machines, the algorithm get all jobs in this two machines. (Ignored the initial value of *max_jobs*)
 * *ite_jobs* : percentage of *iterate* to increment or decrement of the value of *max_jobs* when execute the algorithm.
 * *total_time* : Maximum time (in seconds) to algorithm execute.

If doesn't pass a values to parameters, the default values each parameter is:

 - *init_sol* = 'SLS'
 - *iterate* = 751
 - *exec_time* = 7
 - *max_jobs* = 10 
 - *ite_jobs* = 0.02
 - *total_time* = 1800 (30 minutes)

### Example

```bash
pypy3 main_nw.py instances/I_50_20_S_1-9_1.txt -max_time 600 -exec_time 30 -iterate 500
```

In this example, don't have the parameters *init_sol*, *max_jobs* and *ite_jobs* and they receveid the default value.
Don't matter the order of the parameters, only the name.

## Results

The result's file have the information laki inn the table below:

| A | B | C | D | E | F | G |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| objetive_solution |  mid_time | objetive_heuristic | heuristic_time | heuristic.cont | total_time | gap |