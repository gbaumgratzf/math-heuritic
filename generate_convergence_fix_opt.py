import numpy as np
import matplotlib.pyplot as plt

from util import thebest

def read_file_fix_opt(job, machine, num, inst, i):
    name_file = f'output_julho_2021/fix_opt/I_{job}_{machine}_S_1-{num}_{inst}_{i}.txt'
    file = open(name_file)
    lines = [l[:-1] for l in file.readlines() if 'Academic' not in l if 'Warning' not in l if 'execução' not in l if 'gurobi' not in l if l != '']
    file.close()
    return lines

if __name__ == '__main__':
    jobs = [50, 100, 150, 200, 250]
    machines = [10, 15, 20, 25, 30]
    nums = [9, 49, 99, 124]
    instances = [i+1 for i in range(10)]
    literature = thebest()
    for machine in machines[2:3]:
        for job in jobs[3:4]:
            for num in nums[:1]:
                for inst in instances[:4]:
                    name = f'I_{job}_{machine}_S_1-{num}_{inst}'
                    i = 3
                    # for i in range(5):
                    name_print = name + f'_{i}'
                    print(name_print)
                    lines = read_file_fix_opt(job, machine, num, inst, i)
                    lines = [l.split('|') for l in lines if '*' in l]
                    tuple_value = [(int(l[1]), float(l[5])) for l in lines]
                    the_best = literature[name]
                    tuple_gap = [((value - the_best)/value*100, time) for value, time in tuple_value]
                    gaps = [t[0] for t in tuple_gap]
                    times = [t[1] for t in tuple_gap]
                    # if abs(times[-1] - 3600) > 1:
                    #     times += [3600]
                    #     gaps += [gaps[-1]] 
                    plt.plot(times, gaps, 'o--', label=name)
                    for l in tuple_gap:
                        print(l)
                    # print(lines)
                    print('------------------------')
            plt.legend()
            plt.show()