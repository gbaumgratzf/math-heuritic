from mip import *

if __name__ == '__main__':
    weight = [20, 20, 30, 30]
    value = [20, 30, 20, 40]
    capacity = 50
    n = len(weight)
    model = Model(sense=MAXIMIZE, solver_name=GUROBI)
    I = list(range(n))
    x = [model.add_var(var_type=BINARY, name="x_%d"%(i)) for i in I]
    start = [(x[0], 1)] #, (x[1], 1), (x[2], 1), (x[3], 1)]
    model.start = start
    model.add_constr(xsum([weight[i] * x[i] for i in I]) <= capacity)
    model.objective = xsum(value[i] * x[i] for i in I)
    model.optimize(max_seconds=0.01)
    
    # Print vars
    print("Name : Weigth - Value")
    for i in I:
        if x[i].x > 1e-4:
            print(" %s : %6d - %5d"%(x[i].name, weight[i], value[i]))
