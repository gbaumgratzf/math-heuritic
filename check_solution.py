from problem import Problem
from solution import Solution

def check_solution(problem:Problem):

    format_file_solution = 'solution_ms/I_{job}_{machine}_S_1-{num}_{instance}.txt'.format(
        job=problem.jobs,
        machine=problem.machines,
        num=problem.num,
        instance=problem.instance
    )
    file_solution = open(format_file_solution, 'r')
    lines = [line[:-1].split(' ') for line in file_solution.readlines()]
    file_solution.close()
    # for line in lines:
    #     print(line)

    seek_line = 0
    count_lines = len(lines)
    num_solution = 0
    correct_solution = 0

    while seek_line < count_lines:
        print('Execs : %d'%(num_solution))
        (seek_line, right_answer) = one_solution(problem, seek_line, lines)
        num_solution += 1
        if right_answer:
            correct_solution += 1
        print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')

    print('Num of solution : %d'%(num_solution))
    print('Num of correct solution : %d'%(correct_solution))

def one_solution(problem:Problem, seek_line:int, lines:int) -> (int, bool):
    first_line = lines[seek_line]
    seek_line += 1
    solution = Solution(problem)
    if len(first_line) == 1:
        if first_line[0] == str(problem.machines):
            for i in problem.M:
                machine = [int(elem)+1 for elem in lines[seek_line] if elem != '']
                for j in machine[1:]:
                    solution.allocate(i, j)
                seek_line += 1
    while lines[seek_line][0] != "Total":
        seek_line += 1
    right_answer = solution.makespan == int(lines[seek_line][-1])
    if not right_answer:
        print('==============================')
        solution.print()
        print('Answer : %d'%(int(lines[seek_line][-1])))
        print('==============================')

    seek_line += 2

    return (seek_line, right_answer)


if __name__ == '__main__':
    file_instance = 'instances/I_50_20_S_1-49_5.txt'
    problem = Problem(directory=file_instance)
    check_solution(problem)