from util import thebest


def generate_table():
    jobs = [50, 100, 150, 200, 250]
    machines = [10, 15, 20, 25, 30]
    num = [9, 49, 99, 124]
    instance = list(range(1, 11))


    folder = 'result_julho_2021/fix_opt/{inst}.csv'

    the_best = thebest()

    result = {}
    for j in jobs:
        for m in machines:
            sum_times = 0
            sum_objectives = 0
            sum_gaps = 0
            count = 0
            for n in num:
                for i in instance:
                    inst = f'{j}_{m}_{n}_{i}'
                    key = f'I_{j}_{m}_S_1-{n}_{i}'
                    file = open(folder.format(inst=inst), 'r')
                    lines = file.readlines()
                    file.close()
                    for line in lines:
                        count += 1
                        lin = line[:-1]
                        values = lin.split(';')
                        time = float(values[5])
                        objective = float(values[2])
                        gap = (objective - the_best[key])/objective*100
                        objective = int(values[2])
                        # Sum value
                        sum_objectives += objective
                        sum_times += time
                        sum_gaps += gap
            result[j, m] = [sum_objectives/count, sum_gaps/count, sum_times/count]
    return result


