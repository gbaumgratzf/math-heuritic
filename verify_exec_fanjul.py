
def read_file(job, machine, num, inst):
    name_file = f'result_2021/fanjul/{job}_{machine}_{num}_{inst}.csv'
    file = open(name_file)
    lines = [l[:-1].split(';') for l in file.readlines()]
    file.close()

def read_output_file(job, machine, num, inst):
    name_file = f'output_2021/fanjul/I_{job}_{machine}_S_1-{num}_{inst}.txt'
    file = open(name_file)
    lines = [l[:-1].split(';') for l in file.readlines()]
    file.close()
    return lines

if __name__ == '__main__':
    jobs = [50, 100, 150, 200, 250]
    machines = [10, 15, 20, 25, 30]
    # jobs = [6, 8, 10, 12]
    # machines = [2, 3, 4, 5]
    nums = [9, 49, 99, 124]
    instances = [i+1 for i in range(10)]
    files = []
    for job in jobs:
        for machine in machines:
            for num in nums:
                for inst in instances:
                    try:
                        read_file(job, machine, num, inst)
                    except FileNotFoundError:
                        files += [(job, machine, num, inst)]
                        lines = read_output_file(job, machine, num, inst)
                        print(lines)
                    
    print(files)
    # total_time = '-total_time 3600'
    # fanjul_init = 'pypy3 exec_master_sequence.py'
    # path_save = 'spider'
    # script_string = ''
    # # fanjul
    # save = 'spider'
    # for j, m, t, i in files:
    #     ints = f'I_{j}_{m}_S_1-{t}_{i}.txt'
    #     file_inst = f'instances/{ints}'
    #     solution = f'-output solution_julho_2021/fanjul/{save}/{ints}'
    #     result = f'-save_result result_julho_2021/{save}'
    #     command = ' '.join([fanjul_init, file_inst, total_time, solution, result])
    #     out_file = f'> output_2021/fanjul/{ints}'
    #     script_string += ' '.join([command, out_file, '\n'])
    
    # file_script = open(f'script_{path_save}_2021.sh', 'w')
    # file_script.write(script_string)
    # file_script.close()