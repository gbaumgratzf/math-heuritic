from construtives import (
    construtive,
    construtive_random
)
from problem import Problem


def standard_deviation(list_num: list) -> float:
    minimum = min(list_makespan)
    list_min = [element/minimum for element in list_num]
    average = sum(list_min) / qtd_random
    soma = 0.0
    for num in list_min:
        diff = num - average
        soma += pow(diff, 2)
    sub_calcule = soma / len(list_min)
    deviation = pow(sub_calcule, 0.5)
    return deviation


if __name__ == "__main__":
    jobs = [50, 100, 150, 200, 250]
    machines = [10, 15, 20, 25, 30]
    set_num = [9, 49, 99, 124]
    instances = list(range(1, 11))
    default_dir = '../Conjunto_Instancias/Vallada/I_%d_%d_S_1-%d_%d.txt'
    all_instances = [
        str(default_dir % (job, machine, num, instance))
        for job in jobs for machine in machines
        for num in set_num for instance in instances
    ]
    total_intances = len(all_instances)
    total_construtive = 0.0
    total_cons_random = 0.0
    draw = 0.0
    list_standard = []
    list_average = []
    qtd = 100
    i = 0
    # n = 0
    count = 0.0
    try:
        for i in range(qtd):
            count += 1
            print('================================')
            print('Iteração : %d' % (i+1))
            n_perc = 0.0
            count_instance = 0.0
            for instance in all_instances:
                count_instance += 1.0
                perc = count_instance/total_intances*100
                if perc >= n_perc:
                    print('%6.2f'%(perc))
                    n_perc += 10
                # print('Instance : %s' % (instance))
                problem = Problem(directory=instance)
                # Construtive
                # print('Construtive!')
                solution_const = construtive(problem)
                
                qtd_random = 10
                solution_random = []

                # print('Random Construtive!')
                for _ in range(qtd_random):
                    solution_random.append(construtive_random(problem))
                # print('End Random Construtive!')
                min_const = solution_const.makespan

                list_makespan = [
                    solution.makespan for solution in solution_random]
                min_random = min(list_makespan)
                average = sum(list_average)/qtd_random
                list_average.append(average/min_random)
                list_standard.append(standard_deviation(list_makespan))
                # print(list_average)
                # print(list_standard)
                # input()
                if min_const < min_random:
                    total_construtive += 1.0
                elif min_const > min_random:
                    total_cons_random += 1.0
                else:
                    draw += 1.0
                # print('Contabilizado!')

    except KeyboardInterrupt:
        pass
    standard_s_random = sum(list_standard) / (count)
    standard_m_random = sum(list_average) / (count)
    print('Total old construtive : %f' % (total_construtive/count))
    print('Total random construtive : %f' % (total_cons_random/count))
    print('   - Media de desvio padrão : %f' % (standard_s_random))
    print('   - Media das medias  : %f' % (standard_m_random))
    print('Total Draw : %f' % (draw/count))
