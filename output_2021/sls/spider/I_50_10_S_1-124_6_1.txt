Instance....: instances/I_50_10_S_1-124_6.txt
Algorithm...: Simulated Annealing (alpha=0.990, saMax=10M, t0=1)
Other params: maxIters=100M, seed=-1, timeLimit=3600.00s

    /--------------------------------------------------------\
    |     Iter |   RDP(%) |       S* |       S' |       Time | 
    |----------|----------|----------|----------|------------|
    |        0 |        - |      711 |      711 |       0.00 | s0
    |        4 |        - |      668 |      668 |       0.04 | *
    |        6 |        - |      643 |      643 |       0.04 | *
    |        8 |        - |      628 |      628 |       0.04 | *
    |       13 |        - |      617 |      617 |       0.04 | *
    |       21 |        - |      609 |      609 |       0.05 | *
    |       23 |        - |      585 |      585 |       0.05 | *
    |       24 |        - |      548 |      548 |       0.05 | *
    |       47 |        - |      503 |      503 |       0.06 | *
    |       51 |        - |      502 |      502 |       0.06 | *
    |       61 |        - |      483 |      483 |       0.07 | *
    |       63 |        - |      441 |      441 |       0.07 | *
    |       64 |        - |      433 |      433 |       0.07 | *
    |       68 |        - |      408 |      408 |       0.07 | *
    |       99 |        - |      402 |      402 |       0.08 | *
    |      104 |        - |      399 |      399 |       0.08 | *
    |      111 |        - |      398 |      398 |       0.08 | *
    |      119 |        - |      388 |      388 |       0.08 | *
    |      120 |        - |      383 |      383 |       0.08 | *
    |      136 |        - |      376 |      376 |       0.08 | *
    |      137 |        - |      359 |      359 |       0.08 | *
    |      138 |        - |      357 |      357 |       0.09 | *
    |      163 |        - |      354 |      354 |       0.09 | *
    |      169 |        - |      346 |      346 |       0.09 | *
    |      174 |        - |      337 |      337 |       0.09 | *
    |      182 |        - |      336 |      336 |       0.09 | *
    |      241 |        - |      332 |      332 |       0.10 | *
    |      260 |        - |      314 |      314 |       0.10 | *
    |      296 |        - |      311 |      311 |       0.10 | *
    |      301 |        - |      309 |      309 |       0.10 | *
    |      302 |        - |      293 |      293 |       0.10 | *
    |      337 |        - |      291 |      291 |       0.11 | *
    |      344 |        - |      288 |      288 |       0.11 | *
    |      354 |        - |      281 |      281 |       0.11 | *
    |      358 |        - |      279 |      279 |       0.11 | *
    |      374 |        - |      277 |      277 |       0.11 | *
    |      417 |        - |      270 |      270 |       0.11 | *
    |      421 |        - |      269 |      269 |       0.12 | *
    |      647 |        - |      263 |      263 |       0.12 | *
    |      695 |        - |      262 |      262 |       0.12 | *
    |      725 |        - |      260 |      260 |       0.12 | *
    |      741 |        - |      259 |      259 |       0.12 | *
    |      749 |        - |      256 |      256 |       0.13 | *
    |      853 |        - |      254 |      254 |       0.13 | *
    |      878 |        - |      252 |      252 |       0.13 | *
    |     1111 |        - |      251 |      251 |       0.13 | *
    |     1198 |        - |      246 |      246 |       0.13 | *
    |     1200 |        - |      239 |      239 |       0.14 | *
    |     1331 |        - |      237 |      237 |       0.14 | *
    |     1382 |        - |      236 |      236 |       0.14 | *
    |     1397 |        - |      234 |      234 |       0.14 | *
    |     1401 |        - |      232 |      232 |       0.14 | *
    |     1445 |        - |      230 |      230 |       0.14 | *
    |     1456 |        - |      227 |      227 |       0.14 | *
    |     1519 |        - |      226 |      226 |       0.15 | *
    |     1627 |        - |      224 |      224 |       0.15 | *
    |     1649 |        - |      221 |      221 |       0.15 | *
    |     1680 |        - |      217 |      217 |       0.15 | *
    |     1750 |        - |      216 |      216 |       0.15 | *
    |     1754 |        - |      215 |      215 |       0.15 | *
    |     1801 |        - |      214 |      214 |       0.16 | *
    |     1943 |        - |      210 |      210 |       0.16 | *
    |     1976 |        - |      207 |      207 |       0.16 | *
    |     2052 |        - |      205 |      205 |       0.17 | *
    |     2241 |        - |      203 |      203 |       0.17 | *
    |     2538 |        - |      199 |      199 |       0.17 | *
    |     2687 |        - |      198 |      198 |       0.18 | *
    |     2692 |        - |      197 |      197 |       0.18 | *
    |     4742 |        - |      196 |      196 |       0.19 | *
    |     4933 |        - |      195 |      195 |       0.19 | *
    |     4966 |        - |      193 |      193 |       0.19 | *
    |     5888 |        - |      190 |      190 |       0.20 | *
    |     6295 |        - |      187 |      187 |       0.20 | *
    |     6661 |        - |      185 |      185 |       0.21 | *
    |     8251 |        - |      184 |      184 |       0.22 | *
    |     8262 |        - |      180 |      180 |       0.22 | *
    |     8506 |        - |      178 |      178 |       0.22 | *
    |     8539 |        - |      177 |      177 |       0.22 | *
    |     9461 |        - |      173 |      173 |       0.23 | *
    |     9608 |        - |      172 |      172 |       0.23 | *
    |      11K |        - |      171 |      171 |       0.24 | *
    |      14K |        - |      170 |      170 |       0.25 | *
    |      15K |        - |      169 |      169 |       0.25 | *
    |      20K |        - |      164 |      164 |       0.26 | *
    |      20K |        - |      162 |      162 |       0.26 | *
    |      29K |        - |      161 |      161 |       0.29 | *
    |      30K |        - |      159 |      159 |       0.29 | *
    |      30K |        - |      158 |      158 |       0.30 | *
    |      31K |        - |      153 |      153 |       0.30 | *
    |      45K |        - |      152 |      152 |       0.34 | *
    |      45K |        - |      151 |      151 |       0.34 | *
    |      45K |        - |      147 |      147 |       0.35 | *
    |      45K |        - |      146 |      146 |       0.35 | *
    |      47K |        - |      145 |      145 |       0.37 | *
    |      47K |        - |      143 |      143 |       0.37 | *
    |      48K |        - |      138 |      138 |       0.38 | *
    |      63K |        - |      133 |      133 |       0.44 | *
    |      75K |        - |      132 |      132 |       0.47 | *
    |      75K |        - |      131 |      131 |       0.47 | *
    |      75K |        - |      130 |      130 |       0.47 | *
    |      81K |        - |      129 |      129 |       0.48 | *
    |      81K |        - |      128 |      128 |       0.49 | *
    |     191K |        - |      127 |      127 |       0.97 | *
    |     872K |        - |      126 |      126 |       2.74 | *
    |     979K |        - |      125 |      125 |       2.97 | *
    |     979K |        - |      123 |      123 |       2.97 | *
    |    1059K |        - |      121 |      121 |       3.07 | *
    |      13M |        - |      120 |      120 |      11.46 | *
    |      13M |        - |      119 |      119 |      11.46 | *
    |      14M |        - |      118 |      118 |      11.85 | *
    |      14M |        - |      117 |      117 |      11.96 | *
    |      14M |        - |      116 |      116 |      11.96 | *
    |      28M |        - |      114 |      114 |      20.37 | *
    \--------------------------------------------------------/

Neighborhoods statistics (values in %):

    /----------------------------------------------------------------\
    | Move               | Improvs. | Sideways |  Accepts |  Rejects |
    |--------------------|----------|----------|----------|----------|
    | Shift(mk)          |      14K |    2202K |    2218K |     272M |
    | Shift              |     1403 |    7956K |    7972K |     266M |
    | ShiftSmart(mk)     |      56K |    8810K |    8874K |     265M |
    | ShiftSmart         |     5616 |      32M |      32M |     242M |
    | SimpSwap(mk)       |       66 |      546 |      620 |     274M |
    | SimpSwap           |        8 |      926 |     1014 |     274M |
    | SimpSwapSmart(mk)  |      286 |     2648 |     2974 |     274M |
    | SimpSwapSmart      |       58 |     4006 |     4369 |     274M |
    | Swap(mk)           |       41 |       99 |      170 |     274M |
    | Swap               |       14 |      310 |      354 |     274M |
    | SwapSmart(mk)      |      726 |     1049 |     2440 |     274M |
    | SwapSmart          |      172 |     4172 |     4924 |     274M |
    | Switch(mk)         |     1664 |      56M |      56M |     218M |
    | Switch             |      190 |      59M |      59M |     215M |
    | SwitchSmart(mk)    |     7049 |    8812K |    8819K |     265M |
    | SwitchSmart        |      702 |      11M |      11M |     263M |
    | TaskMove(mk)       |     1068 |      13K |      14K |     274M |
    | TaskMove           |      121 |     4193 |     4949 |     274M |
    | TaskMoveSmart(mk)  |     5311 |      89K |      95K |     274M |
    | TaskMoveSmart      |      554 |      23K |      28K |     274M |
    | 2-Shift(mk)        |     8591 |      14M |      14M |     259M |
    | 2-Shift            |      829 |      20M |      20M |     254M |
    | 2-ShiftSmart(mk)   |      28K |      15M |      15M |     259M |
    | 2-ShiftSmart       |     2754 |      35M |      35M |     239M |
    \----------------------------------------------------------------/

Best makespan.....: 114
N. of Iterations..: 6573495321
Total runtime.....: 3600.03s
Using Python-MIP package version 1.6.8
Começo a execução
    /-------------------------------------------------------------------------------\
    |    Makespan   |   Free Jobs   | Free Machines |    It. Time   |   Total Time  | 
    |-------------------------------------------------------------------------------|
    \-------------------------------------------------------------------------------/
Terminou a execução
