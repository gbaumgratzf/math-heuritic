Instance....: instances/I_50_10_S_1-49_2.txt
Algorithm...: Simulated Annealing (alpha=0.990, saMax=10M, t0=1)
Other params: maxIters=100M, seed=-1, timeLimit=3600.00s

    /--------------------------------------------------------\
    |     Iter |   RDP(%) |       S* |       S' |       Time | 
    |----------|----------|----------|----------|------------|
    |        0 |        - |      472 |      472 |       0.00 | s0
    |        1 |        - |      457 |      457 |       0.03 | *
    |        4 |        - |      445 |      445 |       0.04 | *
    |        7 |        - |      444 |      444 |       0.04 | *
    |        8 |        - |      419 |      419 |       0.05 | *
    |        9 |        - |      406 |      406 |       0.05 | *
    |       18 |        - |      392 |      392 |       0.05 | *
    |       24 |        - |      387 |      387 |       0.05 | *
    |       26 |        - |      371 |      371 |       0.06 | *
    |       29 |        - |      363 |      363 |       0.06 | *
    |       37 |        - |      361 |      361 |       0.07 | *
    |       47 |        - |      354 |      354 |       0.07 | *
    |       49 |        - |      348 |      348 |       0.07 | *
    |       51 |        - |      346 |      346 |       0.07 | *
    |       58 |        - |      342 |      342 |       0.08 | *
    |       59 |        - |      335 |      335 |       0.08 | *
    |       65 |        - |      328 |      328 |       0.08 | *
    |       70 |        - |      305 |      305 |       0.08 | *
    |       72 |        - |      298 |      298 |       0.08 | *
    |       73 |        - |      286 |      286 |       0.08 | *
    |       75 |        - |      285 |      285 |       0.08 | *
    |       78 |        - |      282 |      282 |       0.09 | *
    |       96 |        - |      281 |      281 |       0.09 | *
    |      100 |        - |      277 |      277 |       0.09 | *
    |      105 |        - |      275 |      275 |       0.09 | *
    |      106 |        - |      274 |      274 |       0.09 | *
    |      115 |        - |      270 |      270 |       0.10 | *
    |      128 |        - |      257 |      257 |       0.10 | *
    |      136 |        - |      255 |      255 |       0.10 | *
    |      159 |        - |      251 |      251 |       0.10 | *
    |      161 |        - |      245 |      245 |       0.10 | *
    |      209 |        - |      235 |      235 |       0.10 | *
    |      217 |        - |      234 |      234 |       0.10 | *
    |      220 |        - |      232 |      232 |       0.11 | *
    |      226 |        - |      231 |      231 |       0.11 | *
    |      297 |        - |      230 |      230 |       0.11 | *
    |      302 |        - |      227 |      227 |       0.11 | *
    |      315 |        - |      215 |      215 |       0.11 | *
    |      322 |        - |      211 |      211 |       0.12 | *
    |      324 |        - |      209 |      209 |       0.12 | *
    |      336 |        - |      194 |      194 |       0.12 | *
    |      354 |        - |      188 |      188 |       0.13 | *
    |      385 |        - |      184 |      184 |       0.13 | *
    |      413 |        - |      181 |      181 |       0.13 | *
    |      414 |        - |      179 |      179 |       0.13 | *
    |      419 |        - |      177 |      177 |       0.13 | *
    |      432 |        - |      174 |      174 |       0.13 | *
    |      434 |        - |      171 |      171 |       0.14 | *
    |      435 |        - |      167 |      167 |       0.14 | *
    |      448 |        - |      166 |      166 |       0.14 | *
    |      535 |        - |      165 |      165 |       0.14 | *
    |      562 |        - |      163 |      163 |       0.14 | *
    |      861 |        - |      162 |      162 |       0.14 | *
    |      891 |        - |      161 |      161 |       0.14 | *
    |      947 |        - |      160 |      160 |       0.15 | *
    |      957 |        - |      157 |      157 |       0.15 | *
    |      982 |        - |      155 |      155 |       0.15 | *
    |     1110 |        - |      154 |      154 |       0.15 | *
    |     1117 |        - |      153 |      153 |       0.15 | *
    |     1126 |        - |      151 |      151 |       0.15 | *
    |     1129 |        - |      149 |      149 |       0.15 | *
    |     1242 |        - |      147 |      147 |       0.15 | *
    |     1304 |        - |      146 |      146 |       0.16 | *
    |     1407 |        - |      145 |      145 |       0.16 | *
    |     1770 |        - |      143 |      143 |       0.17 | *
    |     1787 |        - |      142 |      142 |       0.17 | *
    |     2164 |        - |      141 |      141 |       0.17 | *
    |     2196 |        - |      139 |      139 |       0.17 | *
    |     2507 |        - |      137 |      137 |       0.18 | *
    |     3165 |        - |      136 |      136 |       0.19 | *
    |     3341 |        - |      132 |      132 |       0.19 | *
    |     4867 |        - |      131 |      131 |       0.21 | *
    |     6451 |        - |      130 |      130 |       0.23 | *
    |     6473 |        - |      128 |      128 |       0.23 | *
    |     6535 |        - |      123 |      123 |       0.24 | *
    |     6537 |        - |      122 |      122 |       0.24 | *
    |     6571 |        - |      121 |      121 |       0.24 | *
    |     6578 |        - |      120 |      120 |       0.24 | *
    |     7343 |        - |      119 |      119 |       0.24 | *
    |     7349 |        - |      117 |      117 |       0.24 | *
    |     7952 |        - |      115 |      115 |       0.26 | *
    |     7957 |        - |      114 |      114 |       0.26 | *
    |     8434 |        - |      113 |      113 |       0.27 | *
    |      13K |        - |      112 |      112 |       0.29 | *
    |      14K |        - |      109 |      109 |       0.30 | *
    |      14K |        - |      108 |      108 |       0.30 | *
    |      14K |        - |      103 |      103 |       0.30 | *
    |      16K |        - |      102 |      102 |       0.32 | *
    |      21K |        - |      101 |      101 |       0.33 | *
    |      21K |        - |      100 |      100 |       0.33 | *
    |      21K |        - |       98 |       98 |       0.34 | *
    |      21K |        - |       95 |       95 |       0.34 | *
    |      21K |        - |       94 |       94 |       0.34 | *
    |      23K |        - |       93 |       93 |       0.34 | *
    |      23K |        - |       92 |       92 |       0.35 | *
    |     216K |        - |       90 |       90 |       0.83 | *
    |     217K |        - |       89 |       89 |       0.83 | *
    |     341K |        - |       88 |       88 |       1.51 | *
    |     348K |        - |       86 |       86 |       1.57 | *
    |    1955K |        - |       85 |       85 |       4.21 | *
    |    1998K |        - |       83 |       83 |       4.25 | *
    |      71M |        - |       81 |       81 |      44.82 | *
    \--------------------------------------------------------/

Neighborhoods statistics (values in %):

    /----------------------------------------------------------------\
    | Move               | Improvs. | Sideways |  Accepts |  Rejects |
    |--------------------|----------|----------|----------|----------|
    | Shift(mk)          |      29K |      95K |     135K |     277M |
    | Shift              |     2825 |    2853K |    2879K |     275M |
    | ShiftSmart(mk)     |     101K |     317K |     458K |     277M |
    | ShiftSmart         |     9920 |    8004K |    8086K |     269M |
    | SimpSwap(mk)       |      269 |      214 |      591 |     278M |
    | SimpSwap           |       59 |      988 |     1307 |     278M |
    | SimpSwapSmart(mk)  |      990 |      734 |     2113 |     278M |
    | SimpSwapSmart      |      215 |     4072 |     5307 |     278M |
    | Swap(mk)           |      100 |      123 |      278 |     278M |
    | Swap               |       28 |      693 |      842 |     278M |
    | SwapSmart(mk)      |      928 |     1800 |     3171 |     278M |
    | SwapSmart          |      215 |     7881 |     8959 |     278M |
    | Switch(mk)         |      11K |      86M |      86M |     192M |
    | Switch             |     1099 |      61M |      61M |     216M |
    | SwitchSmart(mk)    |      35K |      36K |      75K |     278M |
    | SwitchSmart        |     3506 |    2104K |    2145K |     276M |
    | TaskMove(mk)       |     1118 |     1779 |     3336 |     278M |
    | TaskMove           |      109 |      22K |      23K |     278M |
    | TaskMoveSmart(mk)  |     5172 |     7993 |      15K |     278M |
    | TaskMoveSmart      |      609 |      76K |      81K |     278M |
    | 2-Shift(mk)        |      18K |      41M |      41M |     237M |
    | 2-Shift            |     1697 |      21M |      21M |     256M |
    | 2-ShiftSmart(mk)   |      52K |      80M |      80M |     198M |
    | 2-ShiftSmart       |     5203 |      29M |      29M |     249M |
    \----------------------------------------------------------------/

Best makespan.....: 81
N. of Iterations..: 6665309956
Total runtime.....: 3600.03s
Using Python-MIP package version 1.6.8
Começo a execução
    /-------------------------------------------------------------------------------\
    |    Makespan   |   Free Jobs   | Free Machines |    It. Time   |   Total Time  | 
    |-------------------------------------------------------------------------------|
    \-------------------------------------------------------------------------------/
Terminou a execução
