Instance....: instances/I_200_20_S_1-99_8.txt
Algorithm...: Simulated Annealing (alpha=0.990, saMax=10M, t0=1)
Other params: maxIters=100M, seed=-1, timeLimit=3600.00s

    /--------------------------------------------------------\
    |     Iter |   RDP(%) |       S* |       S' |       Time | 
    |----------|----------|----------|----------|------------|
    |        0 |        - |     1514 |     1514 |       0.00 | s0
    |        1 |        - |     1417 |     1417 |       0.01 | *
    |        2 |        - |     1394 |     1394 |       0.01 | *
    |        3 |        - |     1331 |     1331 |       0.01 | *
    |        4 |        - |     1285 |     1285 |       0.01 | *
    |        7 |        - |     1283 |     1283 |       0.01 | *
    |        9 |        - |     1273 |     1273 |       0.01 | *
    |       13 |        - |     1272 |     1272 |       0.03 | *
    |       15 |        - |     1261 |     1261 |       0.03 | *
    |       18 |        - |     1221 |     1221 |       0.03 | *
    |       19 |        - |     1207 |     1207 |       0.03 | *
    |       27 |        - |     1181 |     1181 |       0.04 | *
    |       29 |        - |     1173 |     1173 |       0.04 | *
    |       31 |        - |     1167 |     1167 |       0.04 | *
    |       35 |        - |     1158 |     1158 |       0.04 | *
    |       41 |        - |     1152 |     1152 |       0.04 | *
    |       47 |        - |     1141 |     1141 |       0.04 | *
    |       48 |        - |     1108 |     1108 |       0.04 | *
    |       50 |        - |     1095 |     1095 |       0.04 | *
    |       52 |        - |     1085 |     1085 |       0.04 | *
    |       54 |        - |     1069 |     1069 |       0.04 | *
    |       61 |        - |     1048 |     1048 |       0.04 | *
    |       62 |        - |     1044 |     1044 |       0.04 | *
    |       64 |        - |     1042 |     1042 |       0.04 | *
    |       74 |        - |     1040 |     1040 |       0.05 | *
    |       77 |        - |     1030 |     1030 |       0.05 | *
    |       82 |        - |     1022 |     1022 |       0.05 | *
    |       90 |        - |     1012 |     1012 |       0.05 | *
    |       94 |        - |     1011 |     1011 |       0.05 | *
    |      115 |        - |     1008 |     1008 |       0.05 | *
    |      132 |        - |      991 |      991 |       0.05 | *
    |      135 |        - |      984 |      984 |       0.05 | *
    |      138 |        - |      970 |      970 |       0.05 | *
    |      141 |        - |      953 |      953 |       0.05 | *
    |      153 |        - |      936 |      936 |       0.05 | *
    |      159 |        - |      918 |      918 |       0.05 | *
    |      164 |        - |      916 |      916 |       0.05 | *
    |      176 |        - |      908 |      908 |       0.05 | *
    |      179 |        - |      901 |      901 |       0.06 | *
    |      194 |        - |      886 |      886 |       0.06 | *
    |      197 |        - |      883 |      883 |       0.06 | *
    |      199 |        - |      882 |      882 |       0.06 | *
    |      209 |        - |      881 |      881 |       0.06 | *
    |      220 |        - |      878 |      878 |       0.06 | *
    |      221 |        - |      877 |      877 |       0.06 | *
    |      224 |        - |      873 |      873 |       0.06 | *
    |      231 |        - |      870 |      870 |       0.06 | *
    |      233 |        - |      869 |      869 |       0.06 | *
    |      241 |        - |      865 |      865 |       0.06 | *
    |      264 |        - |      864 |      864 |       0.06 | *
    |      271 |        - |      857 |      857 |       0.06 | *
    |      272 |        - |      855 |      855 |       0.07 | *
    |      275 |        - |      844 |      844 |       0.07 | *
    |      283 |        - |      835 |      835 |       0.07 | *
    |      308 |        - |      834 |      834 |       0.07 | *
    |      323 |        - |      833 |      833 |       0.07 | *
    |      328 |        - |      826 |      826 |       0.07 | *
    |      332 |        - |      825 |      825 |       0.07 | *
    |      346 |        - |      824 |      824 |       0.07 | *
    |      358 |        - |      823 |      823 |       0.07 | *
    |      365 |        - |      821 |      821 |       0.07 | *
    |      375 |        - |      816 |      816 |       0.07 | *
    |      388 |        - |      812 |      812 |       0.07 | *
    |      400 |        - |      810 |      810 |       0.07 | *
    |      429 |        - |      809 |      809 |       0.07 | *
    |      434 |        - |      806 |      806 |       0.07 | *
    |      448 |        - |      805 |      805 |       0.07 | *
    |      454 |        - |      804 |      804 |       0.08 | *
    |      456 |        - |      796 |      796 |       0.08 | *
    |      460 |        - |      793 |      793 |       0.08 | *
    |      466 |        - |      791 |      791 |       0.08 | *
    |      467 |        - |      783 |      783 |       0.08 | *
    |      469 |        - |      780 |      780 |       0.08 | *
    |      470 |        - |      778 |      778 |       0.08 | *
    |      474 |        - |      771 |      771 |       0.08 | *
    |      483 |        - |      768 |      768 |       0.08 | *
    |      488 |        - |      767 |      767 |       0.08 | *
    |      512 |        - |      766 |      766 |       0.08 | *
    |      514 |        - |      761 |      761 |       0.08 | *
    |      528 |        - |      758 |      758 |       0.08 | *
    |      533 |        - |      757 |      757 |       0.08 | *
    |      544 |        - |      753 |      753 |       0.08 | *
    |      550 |        - |      750 |      750 |       0.08 | *
    |      594 |        - |      739 |      739 |       0.09 | *
    |      596 |        - |      737 |      737 |       0.09 | *
    |      602 |        - |      734 |      734 |       0.09 | *
    |      616 |        - |      732 |      732 |       0.09 | *
    |      621 |        - |      728 |      728 |       0.09 | *
    |      624 |        - |      716 |      716 |       0.09 | *
    |      625 |        - |      715 |      715 |       0.09 | *
    |      630 |        - |      710 |      710 |       0.09 | *
    |      632 |        - |      709 |      709 |       0.09 | *
    |      660 |        - |      708 |      708 |       0.09 | *
    |      679 |        - |      703 |      703 |       0.09 | *
    |      682 |        - |      702 |      702 |       0.09 | *
    |      696 |        - |      698 |      698 |       0.09 | *
    |      701 |        - |      696 |      696 |       0.09 | *
    |      720 |        - |      693 |      693 |       0.09 | *
    |      729 |        - |      688 |      688 |       0.09 | *
    |      731 |        - |      685 |      685 |       0.09 | *
    |      774 |        - |      684 |      684 |       0.10 | *
    |      787 |        - |      682 |      682 |       0.10 | *
    |      806 |        - |      680 |      680 |       0.10 | *
    |      852 |        - |      676 |      676 |       0.10 | *
    |      872 |        - |      675 |      675 |       0.10 | *
    |      873 |        - |      667 |      667 |       0.10 | *
    |      877 |        - |      663 |      663 |       0.10 | *
    |      887 |        - |      662 |      662 |       0.10 | *
    |      890 |        - |      656 |      656 |       0.10 | *
    |      920 |        - |      649 |      649 |       0.10 | *
    |      931 |        - |      647 |      647 |       0.10 | *
    |      933 |        - |      646 |      646 |       0.10 | *
    |      937 |        - |      643 |      643 |       0.10 | *
    |      940 |        - |      642 |      642 |       0.10 | *
    |      942 |        - |      637 |      637 |       0.10 | *
    |      946 |        - |      633 |      633 |       0.10 | *
    |      967 |        - |      632 |      632 |       0.10 | *
    |      970 |        - |      631 |      631 |       0.11 | *
    |      979 |        - |      629 |      629 |       0.11 | *
    |     1009 |        - |      628 |      628 |       0.11 | *
    |     1016 |        - |      627 |      627 |       0.11 | *
    |     1037 |        - |      625 |      625 |       0.11 | *
    |     1055 |        - |      623 |      623 |       0.11 | *
    |     1075 |        - |      619 |      619 |       0.11 | *
    |     1086 |        - |      618 |      618 |       0.11 | *
    |     1110 |        - |      614 |      614 |       0.11 | *
    |     1135 |        - |      611 |      611 |       0.11 | *
    |     1156 |        - |      607 |      607 |       0.11 | *
    |     1192 |        - |      601 |      601 |       0.11 | *
    |     1269 |        - |      599 |      599 |       0.11 | *
    |     1276 |        - |      598 |      598 |       0.11 | *
    |     1278 |        - |      594 |      594 |       0.11 | *
    |     1291 |        - |      585 |      585 |       0.12 | *
    |     1313 |        - |      584 |      584 |       0.12 | *
    |     1321 |        - |      581 |      581 |       0.12 | *
    |     1325 |        - |      580 |      580 |       0.12 | *
    |     1353 |        - |      579 |      579 |       0.12 | *
    |     1412 |        - |      576 |      576 |       0.12 | *
    |     1457 |        - |      574 |      574 |       0.12 | *
    |     1475 |        - |      573 |      573 |       0.12 | *
    |     1476 |        - |      572 |      572 |       0.12 | *
    |     1480 |        - |      569 |      569 |       0.12 | *
    |     1486 |        - |      568 |      568 |       0.12 | *
    |     1488 |        - |      566 |      566 |       0.12 | *
    |     1503 |        - |      563 |      563 |       0.12 | *
    |     1591 |        - |      562 |      562 |       0.12 | *
    |     1594 |        - |      560 |      560 |       0.12 | *
    |     1600 |        - |      556 |      556 |       0.13 | *
    |     1686 |        - |      555 |      555 |       0.13 | *
    |     1692 |        - |      554 |      554 |       0.13 | *
    |     1715 |        - |      550 |      550 |       0.13 | *
    |     1832 |        - |      549 |      549 |       0.13 | *
    |     1833 |        - |      548 |      548 |       0.13 | *
    |     1871 |        - |      545 |      545 |       0.13 | *
    |     1900 |        - |      544 |      544 |       0.13 | *
    |     1930 |        - |      543 |      543 |       0.13 | *
    |     1938 |        - |      536 |      536 |       0.13 | *
    |     2006 |        - |      530 |      530 |       0.13 | *
    |     2012 |        - |      529 |      529 |       0.14 | *
    |     2014 |        - |      528 |      528 |       0.14 | *
    |     2043 |        - |      527 |      527 |       0.14 | *
    |     2063 |        - |      523 |      523 |       0.14 | *
    |     2066 |        - |      522 |      522 |       0.14 | *
    |     2105 |        - |      521 |      521 |       0.14 | *
    |     2110 |        - |      520 |      520 |       0.14 | *
    |     2166 |        - |      519 |      519 |       0.14 | *
    |     2295 |        - |      518 |      518 |       0.14 | *
    |     2345 |        - |      516 |      516 |       0.14 | *
    |     2351 |        - |      508 |      508 |       0.15 | *
    |     2354 |        - |      507 |      507 |       0.15 | *
    |     2361 |        - |      503 |      503 |       0.15 | *
    |     2434 |        - |      502 |      502 |       0.15 | *
    |     2577 |        - |      499 |      499 |       0.15 | *
    |     2664 |        - |      498 |      498 |       0.15 | *
    |     2668 |        - |      497 |      497 |       0.15 | *
    |     2783 |        - |      496 |      496 |       0.15 | *
    |     2784 |        - |      495 |      495 |       0.15 | *
    |     2796 |        - |      494 |      494 |       0.15 | *
    |     2826 |        - |      493 |      493 |       0.15 | *
    |     2839 |        - |      490 |      490 |       0.15 | *
    |     2848 |        - |      489 |      489 |       0.15 | *
    |     2851 |        - |      487 |      487 |       0.16 | *
    |     2865 |        - |      482 |      482 |       0.16 | *
    |     2870 |        - |      478 |      478 |       0.16 | *
    |     2874 |        - |      477 |      477 |       0.16 | *
    |     2882 |        - |      476 |      476 |       0.16 | *
    |     2949 |        - |      475 |      475 |       0.16 | *
    |     3112 |        - |      474 |      474 |       0.16 | *
    |     3121 |        - |      471 |      471 |       0.16 | *
    |     3197 |        - |      468 |      468 |       0.16 | *
    |     3263 |        - |      467 |      467 |       0.16 | *
    |     3297 |        - |      466 |      466 |       0.16 | *
    |     3343 |        - |      464 |      464 |       0.16 | *
    |     3374 |        - |      462 |      462 |       0.17 | *
    |     3425 |        - |      459 |      459 |       0.17 | *
    |     3469 |        - |      454 |      454 |       0.17 | *
    |     3479 |        - |      453 |      453 |       0.17 | *
    |     3484 |        - |      452 |      452 |       0.17 | *
    |     3503 |        - |      449 |      449 |       0.17 | *
    |     3523 |        - |      448 |      448 |       0.17 | *
    |     3593 |        - |      445 |      445 |       0.17 | *
    |     3664 |        - |      443 |      443 |       0.17 | *
    |     3700 |        - |      442 |      442 |       0.17 | *
    |     3713 |        - |      441 |      441 |       0.17 | *
    |     3747 |        - |      437 |      437 |       0.17 | *
    |     3791 |        - |      436 |      436 |       0.17 | *
    |     3809 |        - |      434 |      434 |       0.17 | *
    |     3881 |        - |      432 |      432 |       0.18 | *
    |     4040 |        - |      430 |      430 |       0.18 | *
    |     4258 |        - |      428 |      428 |       0.18 | *
    |     4275 |        - |      425 |      425 |       0.18 | *
    |     4430 |        - |      423 |      423 |       0.18 | *
    |     4503 |        - |      422 |      422 |       0.18 | *
    |     4516 |        - |      421 |      421 |       0.18 | *
    |     4539 |        - |      419 |      419 |       0.18 | *
    |     4543 |        - |      418 |      418 |       0.18 | *
    |     4611 |        - |      416 |      416 |       0.18 | *
    |     4641 |        - |      413 |      413 |       0.18 | *
    |     4691 |        - |      412 |      412 |       0.18 | *
    |     4766 |        - |      411 |      411 |       0.19 | *
    |     4777 |        - |      410 |      410 |       0.19 | *
    |     4855 |        - |      409 |      409 |       0.19 | *
    |     4868 |        - |      407 |      407 |       0.19 | *
    |     4925 |        - |      406 |      406 |       0.19 | *
    |     4936 |        - |      405 |      405 |       0.19 | *
    |     4982 |        - |      402 |      402 |       0.19 | *
    |     5004 |        - |      400 |      400 |       0.19 | *
    |     5266 |        - |      399 |      399 |       0.19 | *
    |     5276 |        - |      397 |      397 |       0.19 | *
    |     5436 |        - |      392 |      392 |       0.19 | *
    |     5761 |        - |      391 |      391 |       0.19 | *
    |     5887 |        - |      390 |      390 |       0.19 | *
    |     6225 |        - |      389 |      389 |       0.20 | *
    |     6432 |        - |      388 |      388 |       0.20 | *
    |     6561 |        - |      385 |      385 |       0.20 | *
    |     6711 |        - |      384 |      384 |       0.20 | *
    |     6832 |        - |      382 |      382 |       0.20 | *
    |     6863 |        - |      381 |      381 |       0.20 | *
    |     6908 |        - |      378 |      378 |       0.20 | *
    |     6910 |        - |      377 |      377 |       0.20 | *
    |     7103 |        - |      375 |      375 |       0.20 | *
    |     7118 |        - |      373 |      373 |       0.20 | *
    |     7133 |        - |      372 |      372 |       0.20 | *
    |     7591 |        - |      371 |      371 |       0.21 | *
    |     7667 |        - |      370 |      370 |       0.21 | *
    |     7741 |        - |      368 |      368 |       0.21 | *
    |     7790 |        - |      366 |      366 |       0.21 | *
    |     7798 |        - |      365 |      365 |       0.21 | *
    |     7844 |        - |      364 |      364 |       0.21 | *
    |     7899 |        - |      363 |      363 |       0.21 | *
    |     8317 |        - |      360 |      360 |       0.21 | *
    |     8368 |        - |      359 |      359 |       0.21 | *
    |     8418 |        - |      358 |      358 |       0.22 | *
    |     9093 |        - |      356 |      356 |       0.22 | *
    |     9886 |        - |      355 |      355 |       0.22 | *
    |     9939 |        - |      353 |      353 |       0.22 | *
    |     9985 |        - |      352 |      352 |       0.22 | *
    |      11K |        - |      350 |      350 |       0.23 | *
    |      11K |        - |      348 |      348 |       0.23 | *
    |      11K |        - |      346 |      346 |       0.23 | *
    |      11K |        - |      345 |      345 |       0.23 | *
    |      11K |        - |      344 |      344 |       0.23 | *
    |      11K |        - |      342 |      342 |       0.23 | *
    |      11K |        - |      339 |      339 |       0.23 | *
    |      12K |        - |      336 |      336 |       0.23 | *
    |      12K |        - |      334 |      334 |       0.23 | *
    |      12K |        - |      333 |      333 |       0.23 | *
    |      12K |        - |      331 |      331 |       0.23 | *
    |      12K |        - |      330 |      330 |       0.24 | *
    |      12K |        - |      329 |      329 |       0.24 | *
    |      12K |        - |      328 |      328 |       0.24 | *
    |      12K |        - |      327 |      327 |       0.24 | *
    |      12K |        - |      326 |      326 |       0.24 | *
    |      13K |        - |      325 |      325 |       0.24 | *
    |      13K |        - |      324 |      324 |       0.24 | *
    |      13K |        - |      323 |      323 |       0.24 | *
    |      13K |        - |      321 |      321 |       0.24 | *
    |      14K |        - |      317 |      317 |       0.25 | *
    |      14K |        - |      316 |      316 |       0.25 | *
    |      14K |        - |      315 |      315 |       0.25 | *
    |      14K |        - |      314 |      314 |       0.25 | *
    |      14K |        - |      310 |      310 |       0.25 | *
    |      14K |        - |      309 |      309 |       0.25 | *
    |      15K |        - |      308 |      308 |       0.25 | *
    |      15K |        - |      307 |      307 |       0.25 | *
    |      15K |        - |      306 |      306 |       0.25 | *
    |      15K |        - |      305 |      305 |       0.25 | *
    |      15K |        - |      302 |      302 |       0.26 | *
    |      16K |        - |      301 |      301 |       0.26 | *
    |      17K |        - |      300 |      300 |       0.26 | *
    |      17K |        - |      299 |      299 |       0.26 | *
    |      18K |        - |      297 |      297 |       0.26 | *
    |      18K |        - |      294 |      294 |       0.26 | *
    |      18K |        - |      291 |      291 |       0.26 | *
    |      18K |        - |      290 |      290 |       0.27 | *
    |      19K |        - |      289 |      289 |       0.27 | *
    |      19K |        - |      286 |      286 |       0.27 | *
    |      20K |        - |      285 |      285 |       0.27 | *
    |      21K |        - |      284 |      284 |       0.28 | *
    |      21K |        - |      283 |      283 |       0.28 | *
    |      21K |        - |      282 |      282 |       0.28 | *
    |      21K |        - |      281 |      281 |       0.28 | *
    |      22K |        - |      278 |      278 |       0.28 | *
    |      22K |        - |      277 |      277 |       0.28 | *
    |      22K |        - |      276 |      276 |       0.28 | *
    |      23K |        - |      274 |      274 |       0.29 | *
    |      23K |        - |      273 |      273 |       0.29 | *
    |      23K |        - |      272 |      272 |       0.29 | *
    |      23K |        - |      271 |      271 |       0.29 | *
    |      23K |        - |      270 |      270 |       0.29 | *
    |      23K |        - |      269 |      269 |       0.29 | *
    |      24K |        - |      268 |      268 |       0.29 | *
    |      24K |        - |      267 |      267 |       0.29 | *
    |      25K |        - |      266 |      266 |       0.29 | *
    |      25K |        - |      265 |      265 |       0.29 | *
    |      25K |        - |      264 |      264 |       0.30 | *
    |      25K |        - |      263 |      263 |       0.30 | *
    |      29K |        - |      262 |      262 |       0.31 | *
    |      30K |        - |      261 |      261 |       0.31 | *
    |      30K |        - |      260 |      260 |       0.31 | *
    |      30K |        - |      259 |      259 |       0.31 | *
    |      31K |        - |      257 |      257 |       0.32 | *
    |      31K |        - |      256 |      256 |       0.32 | *
    |      32K |        - |      255 |      255 |       0.32 | *
    |      32K |        - |      254 |      254 |       0.32 | *
    |      33K |        - |      253 |      253 |       0.32 | *
    |      33K |        - |      252 |      252 |       0.32 | *
    |      33K |        - |      251 |      251 |       0.32 | *
    |      33K |        - |      250 |      250 |       0.32 | *
    |      33K |        - |      249 |      249 |       0.33 | *
    |      34K |        - |      248 |      248 |       0.33 | *
    |      35K |        - |      247 |      247 |       0.33 | *
    |      41K |        - |      246 |      246 |       0.35 | *
    |      42K |        - |      245 |      245 |       0.35 | *
    |      42K |        - |      243 |      243 |       0.35 | *
    |      43K |        - |      242 |      242 |       0.35 | *
    |      45K |        - |      241 |      241 |       0.36 | *
    |      45K |        - |      240 |      240 |       0.36 | *
    |      45K |        - |      239 |      239 |       0.36 | *
    |      46K |        - |      238 |      238 |       0.36 | *
    |      49K |        - |      237 |      237 |       0.37 | *
    |      50K |        - |      236 |      236 |       0.38 | *
    |      55K |        - |      235 |      235 |       0.40 | *
    |      55K |        - |      234 |      234 |       0.40 | *
    |      58K |        - |      233 |      233 |       0.40 | *
    |      58K |        - |      232 |      232 |       0.41 | *
    |      60K |        - |      231 |      231 |       0.41 | *
    |      63K |        - |      229 |      229 |       0.42 | *
    |      63K |        - |      228 |      228 |       0.42 | *
    |      68K |        - |      227 |      227 |       0.43 | *
    |      69K |        - |      226 |      226 |       0.43 | *
    |      70K |        - |      225 |      225 |       0.43 | *
    |      71K |        - |      224 |      224 |       0.43 | *
    |      72K |        - |      223 |      223 |       0.44 | *
    |      72K |        - |      222 |      222 |       0.44 | *
    |      81K |        - |      221 |      221 |       0.45 | *
    |      85K |        - |      220 |      220 |       0.46 | *
    |      87K |        - |      219 |      219 |       0.47 | *
    |      87K |        - |      218 |      218 |       0.47 | *
    |      89K |        - |      216 |      216 |       0.47 | *
    |      92K |        - |      215 |      215 |       0.48 | *
    |      92K |        - |      214 |      214 |       0.48 | *
    |      92K |        - |      213 |      213 |       0.48 | *
    |      93K |        - |      211 |      211 |       0.49 | *
    |     106K |        - |      210 |      210 |       0.52 | *
    |     106K |        - |      209 |      209 |       0.53 | *
    |     120K |        - |      208 |      208 |       0.56 | *
    |     122K |        - |      207 |      207 |       0.57 | *
    |     123K |        - |      206 |      206 |       0.57 | *
    |     124K |        - |      205 |      205 |       0.58 | *
    |     125K |        - |      204 |      204 |       0.58 | *
    |     126K |        - |      203 |      203 |       0.58 | *
    |     129K |        - |      202 |      202 |       0.59 | *
    |     129K |        - |      201 |      201 |       0.59 | *
    |     129K |        - |      200 |      200 |       0.59 | *
    |     134K |        - |      199 |      199 |       0.61 | *
    |     137K |        - |      198 |      198 |       0.62 | *
    |     153K |        - |      197 |      197 |       0.67 | *
    |     174K |        - |      196 |      196 |       0.75 | *
    |     182K |        - |      195 |      195 |       0.78 | *
    |     183K |        - |      194 |      194 |       0.78 | *
    |     191K |        - |      193 |      193 |       0.81 | *
    |     191K |        - |      192 |      192 |       0.81 | *
    |     194K |        - |      191 |      191 |       0.82 | *
    |     197K |        - |      190 |      190 |       0.83 | *
    |     205K |        - |      189 |      189 |       0.85 | *
    |     217K |        - |      188 |      188 |       0.88 | *
    |     218K |        - |      187 |      187 |       0.88 | *
    |     220K |        - |      185 |      185 |       0.89 | *
    |     223K |        - |      184 |      184 |       0.90 | *
    |     224K |        - |      183 |      183 |       0.90 | *
    |     224K |        - |      181 |      181 |       0.90 | *
    |     227K |        - |      180 |      180 |       0.91 | *
    |     288K |        - |      179 |      179 |       1.04 | *
    |     291K |        - |      178 |      178 |       1.05 | *
    |     374K |        - |      177 |      177 |       1.18 | *
    |     374K |        - |      176 |      176 |       1.19 | *
    |     389K |        - |      175 |      175 |       1.20 | *
    |     411K |        - |      174 |      174 |       1.24 | *
    |     412K |        - |      173 |      173 |       1.24 | *
    |     414K |        - |      172 |      172 |       1.24 | *
    |     427K |        - |      171 |      171 |       1.27 | *
    |     517K |        - |      170 |      170 |       1.42 | *
    |     549K |        - |      169 |      169 |       1.47 | *
    |     564K |        - |      168 |      168 |       1.49 | *
    |     571K |        - |      167 |      167 |       1.51 | *
    |     795K |        - |      166 |      166 |       1.81 | *
    |     795K |        - |      165 |      165 |       1.81 | *
    |     804K |        - |      164 |      164 |       1.82 | *
    |     866K |        - |      163 |      163 |       1.86 | *
    |     874K |        - |      162 |      162 |       1.87 | *
    |     882K |        - |      161 |      161 |       1.88 | *
    |    1025K |        - |      160 |      160 |       2.02 | *
    |    1245K |        - |      159 |      159 |       2.18 | *
    |    2529K |        - |      158 |      158 |       3.35 | *
    |    2630K |        - |      157 |      157 |       3.47 | *
    |    2632K |        - |      156 |      156 |       3.48 | *
    |    2688K |        - |      155 |      155 |       3.53 | *
    |    2716K |        - |      154 |      154 |       3.56 | *
    |    3697K |        - |      153 |      153 |       4.43 | *
    |    4935K |        - |      152 |      152 |       5.41 | *
    |    5405K |        - |      151 |      151 |       5.91 | *
    |    6774K |        - |      150 |      150 |       7.09 | *
    |    6784K |        - |      149 |      149 |       7.10 | *
    |    6809K |        - |      148 |      148 |       7.12 | *
    |    6884K |        - |      147 |      147 |       7.20 | *
    |    6896K |        - |      146 |      146 |       7.22 | *
    |    7004K |        - |      145 |      145 |       7.35 | *
    |      13M |        - |      144 |      144 |      13.03 | *
    |      13M |        - |      142 |      142 |      13.28 | *
    |      17M |        - |      141 |      141 |      16.30 | *
    |      21M |        - |      140 |      140 |      19.07 | *
    |      21M |        - |      139 |      139 |      19.19 | *
    |      22M |        - |      138 |      138 |      19.56 | *
    |      23M |        - |      137 |      137 |      20.31 | *
    |      23M |        - |      136 |      136 |      20.47 | *
    |      26M |        - |      135 |      135 |      22.73 | *
    |      29M |        - |      134 |      134 |      24.67 | *
    |      46M |        - |      133 |      133 |      36.93 | *
    |      47M |        - |      132 |      132 |      37.13 | *
    |      88M |        - |      131 |      131 |      66.34 | *
    |     226M |        - |      130 |      130 |     164.11 | *
    |     349M |        - |      129 |      129 |     251.33 | *
    |     357M |        - |      128 |      128 |     257.26 | *
    |     359M |        - |      127 |      127 |     258.71 | *
    |     543M |        - |      126 |      126 |     388.82 | *
    |     554M |        - |      125 |      125 |     395.97 | *
    |     584M |        - |      124 |      124 |     417.18 | *
    |     615M |        - |      123 |      123 |     439.09 | *
    |     645M |        - |      122 |      122 |     460.40 | *
    \--------------------------------------------------------/

Neighborhoods statistics (values in %):

    /----------------------------------------------------------------\
    | Move               | Improvs. | Sideways |  Accepts |  Rejects |
    |--------------------|----------|----------|----------|----------|
    | Shift(mk)          |      14K |      19K |      38K |     217M |
    | Shift              |      674 |     398K |     409K |     217M |
    | ShiftSmart(mk)     |     107K |     152K |     299K |     217M |
    | ShiftSmart         |     5344 |    2750K |    2826K |     215M |
    | SimpSwap(mk)       |       77 |      101 |      197 |     218M |
    | SimpSwap           |        6 |      523 |      587 |     218M |
    | SimpSwapSmart(mk)  |      578 |      769 |     1557 |     218M |
    | SimpSwapSmart      |       60 |     4346 |     4901 |     218M |
    | Swap(mk)           |       15 |       18 |       36 |     218M |
    | Swap               |        2 |      102 |      112 |     218M |
    | SwapSmart(mk)      |      507 |      844 |     1554 |     218M |
    | SwapSmart          |       54 |     3841 |     4293 |     218M |
    | Switch(mk)         |     3684 |      18M |      18M |     199M |
    | Switch             |      205 |      23M |      23M |     195M |
    | SwitchSmart(mk)    |      30K |      31K |      71K |     218M |
    | SwitchSmart        |     1459 |     359K |     381K |     217M |
    | TaskMove(mk)       |      690 |     1205 |     2152 |     218M |
    | TaskMove           |       41 |     4137 |     4592 |     218M |
    | TaskMoveSmart(mk)  |     6049 |      11K |      19K |     218M |
    | TaskMoveSmart      |      398 |      37K |      41K |     218M |
    | 2-Shift(mk)        |     3947 |    1721K |    1726K |     216M |
    | 2-Shift            |      175 |    2873K |    2876K |     215M |
    | 2-ShiftSmart(mk)   |      24K |    2356K |    2385K |     215M |
    | 2-ShiftSmart       |     1207 |    5407K |    5422K |     212M |
    \----------------------------------------------------------------/

Best makespan.....: 122
N. of Iterations..: 5224328828
Total runtime.....: 3600.02s
Using Python-MIP package version 1.6.8
Começo a execução
    /-------------------------------------------------------------------------------\
    |    Makespan   |   Free Jobs   | Free Machines |    It. Time   |   Total Time  | 
    |-------------------------------------------------------------------------------|
    \-------------------------------------------------------------------------------/
Terminou a execução
