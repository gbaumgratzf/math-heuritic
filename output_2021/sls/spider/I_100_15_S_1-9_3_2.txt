Instance....: instances/I_100_15_S_1-9_3.txt
Algorithm...: Simulated Annealing (alpha=0.990, saMax=10M, t0=1)
Other params: maxIters=100M, seed=-1, timeLimit=3600.00s

    /--------------------------------------------------------\
    |     Iter |   RDP(%) |       S* |       S' |       Time | 
    |----------|----------|----------|----------|------------|
    |        0 |        - |      596 |      596 |       0.00 | s0
    |        2 |        - |      584 |      584 |       0.02 | *
    |        6 |        - |      572 |      572 |       0.02 | *
    |        7 |        - |      569 |      569 |       0.02 | *
    |        9 |        - |      563 |      563 |       0.02 | *
    |       22 |        - |      560 |      560 |       0.02 | *
    |       24 |        - |      556 |      556 |       0.03 | *
    |       25 |        - |      555 |      555 |       0.03 | *
    |       27 |        - |      551 |      551 |       0.03 | *
    |       28 |        - |      548 |      548 |       0.03 | *
    |       30 |        - |      527 |      527 |       0.03 | *
    |       31 |        - |      503 |      503 |       0.03 | *
    |       40 |        - |      498 |      498 |       0.03 | *
    |       44 |        - |      492 |      492 |       0.03 | *
    |       45 |        - |      488 |      488 |       0.03 | *
    |       47 |        - |      487 |      487 |       0.03 | *
    |       48 |        - |      476 |      476 |       0.04 | *
    |       49 |        - |      471 |      471 |       0.04 | *
    |       54 |        - |      461 |      461 |       0.04 | *
    |       67 |        - |      453 |      453 |       0.04 | *
    |       68 |        - |      447 |      447 |       0.04 | *
    |       72 |        - |      442 |      442 |       0.04 | *
    |       78 |        - |      441 |      441 |       0.04 | *
    |       90 |        - |      438 |      438 |       0.04 | *
    |       91 |        - |      432 |      432 |       0.04 | *
    |       98 |        - |      417 |      417 |       0.04 | *
    |      103 |        - |      406 |      406 |       0.04 | *
    |      112 |        - |      400 |      400 |       0.04 | *
    |      115 |        - |      385 |      385 |       0.05 | *
    |      117 |        - |      383 |      383 |       0.05 | *
    |      123 |        - |      382 |      382 |       0.05 | *
    |      127 |        - |      374 |      374 |       0.05 | *
    |      132 |        - |      373 |      373 |       0.05 | *
    |      134 |        - |      368 |      368 |       0.05 | *
    |      140 |        - |      367 |      367 |       0.05 | *
    |      145 |        - |      352 |      352 |       0.05 | *
    |      146 |        - |      348 |      348 |       0.05 | *
    |      150 |        - |      340 |      340 |       0.05 | *
    |      156 |        - |      339 |      339 |       0.05 | *
    |      160 |        - |      335 |      335 |       0.05 | *
    |      165 |        - |      331 |      331 |       0.05 | *
    |      171 |        - |      325 |      325 |       0.05 | *
    |      174 |        - |      324 |      324 |       0.06 | *
    |      175 |        - |      321 |      321 |       0.06 | *
    |      195 |        - |      320 |      320 |       0.06 | *
    |      207 |        - |      316 |      316 |       0.06 | *
    |      212 |        - |      311 |      311 |       0.06 | *
    |      216 |        - |      310 |      310 |       0.06 | *
    |      218 |        - |      309 |      309 |       0.06 | *
    |      223 |        - |      308 |      308 |       0.06 | *
    |      232 |        - |      305 |      305 |       0.06 | *
    |      242 |        - |      304 |      304 |       0.06 | *
    |      249 |        - |      296 |      296 |       0.06 | *
    |      265 |        - |      293 |      293 |       0.06 | *
    |      267 |        - |      289 |      289 |       0.06 | *
    |      271 |        - |      287 |      287 |       0.06 | *
    |      277 |        - |      285 |      285 |       0.06 | *
    |      285 |        - |      284 |      284 |       0.06 | *
    |      290 |        - |      279 |      279 |       0.06 | *
    |      291 |        - |      266 |      266 |       0.06 | *
    |      293 |        - |      264 |      264 |       0.07 | *
    |      306 |        - |      253 |      253 |       0.07 | *
    |      308 |        - |      252 |      252 |       0.07 | *
    |      311 |        - |      246 |      246 |       0.07 | *
    |      333 |        - |      244 |      244 |       0.07 | *
    |      334 |        - |      243 |      243 |       0.07 | *
    |      347 |        - |      241 |      241 |       0.07 | *
    |      366 |        - |      238 |      238 |       0.07 | *
    |      380 |        - |      237 |      237 |       0.07 | *
    |      387 |        - |      233 |      233 |       0.07 | *
    |      418 |        - |      232 |      232 |       0.07 | *
    |      436 |        - |      231 |      231 |       0.07 | *
    |      453 |        - |      230 |      230 |       0.07 | *
    |      467 |        - |      229 |      229 |       0.07 | *
    |      475 |        - |      228 |      228 |       0.07 | *
    |      488 |        - |      221 |      221 |       0.07 | *
    |      498 |        - |      220 |      220 |       0.07 | *
    |      513 |        - |      219 |      219 |       0.07 | *
    |      514 |        - |      218 |      218 |       0.08 | *
    |      518 |        - |      215 |      215 |       0.08 | *
    |      538 |        - |      214 |      214 |       0.08 | *
    |      541 |        - |      213 |      213 |       0.08 | *
    |      545 |        - |      211 |      211 |       0.08 | *
    |      554 |        - |      210 |      210 |       0.08 | *
    |      557 |        - |      209 |      209 |       0.08 | *
    |      560 |        - |      206 |      206 |       0.08 | *
    |      561 |        - |      204 |      204 |       0.08 | *
    |      563 |        - |      203 |      203 |       0.08 | *
    |      565 |        - |      202 |      202 |       0.08 | *
    |      570 |        - |      200 |      200 |       0.08 | *
    |      605 |        - |      199 |      199 |       0.08 | *
    |      632 |        - |      196 |      196 |       0.08 | *
    |      654 |        - |      195 |      195 |       0.08 | *
    |      656 |        - |      194 |      194 |       0.08 | *
    |      712 |        - |      193 |      193 |       0.08 | *
    |      713 |        - |      191 |      191 |       0.08 | *
    |      718 |        - |      190 |      190 |       0.09 | *
    |      725 |        - |      188 |      188 |       0.09 | *
    |      746 |        - |      186 |      186 |       0.09 | *
    |      750 |        - |      185 |      185 |       0.09 | *
    |      809 |        - |      182 |      182 |       0.09 | *
    |      839 |        - |      176 |      176 |       0.09 | *
    |      884 |        - |      175 |      175 |       0.09 | *
    |      894 |        - |      174 |      174 |       0.09 | *
    |      901 |        - |      172 |      172 |       0.09 | *
    |      914 |        - |      171 |      171 |       0.09 | *
    |      919 |        - |      170 |      170 |       0.09 | *
    |      920 |        - |      167 |      167 |       0.09 | *
    |      926 |        - |      166 |      166 |       0.10 | *
    |      954 |        - |      165 |      165 |       0.10 | *
    |      973 |        - |      164 |      164 |       0.10 | *
    |     1044 |        - |      163 |      163 |       0.10 | *
    |     1046 |        - |      161 |      161 |       0.10 | *
    |     1147 |        - |      158 |      158 |       0.10 | *
    |     1218 |        - |      157 |      157 |       0.10 | *
    |     1223 |        - |      156 |      156 |       0.10 | *
    |     1235 |        - |      154 |      154 |       0.10 | *
    |     1242 |        - |      153 |      153 |       0.10 | *
    |     1248 |        - |      152 |      152 |       0.10 | *
    |     1253 |        - |      150 |      150 |       0.10 | *
    |     1278 |        - |      149 |      149 |       0.10 | *
    |     1308 |        - |      148 |      148 |       0.10 | *
    |     1312 |        - |      146 |      146 |       0.10 | *
    |     1430 |        - |      145 |      145 |       0.10 | *
    |     1480 |        - |      144 |      144 |       0.11 | *
    |     1497 |        - |      143 |      143 |       0.11 | *
    |     1498 |        - |      142 |      142 |       0.11 | *
    |     1500 |        - |      141 |      141 |       0.11 | *
    |     1517 |        - |      140 |      140 |       0.11 | *
    |     1518 |        - |      139 |      139 |       0.11 | *
    |     1524 |        - |      138 |      138 |       0.11 | *
    |     1571 |        - |      137 |      137 |       0.11 | *
    |     1575 |        - |      135 |      135 |       0.11 | *
    |     1584 |        - |      134 |      134 |       0.11 | *
    |     1630 |        - |      133 |      133 |       0.11 | *
    |     1665 |        - |      132 |      132 |       0.11 | *
    |     1689 |        - |      131 |      131 |       0.11 | *
    |     1736 |        - |      130 |      130 |       0.12 | *
    |     1885 |        - |      129 |      129 |       0.12 | *
    |     1945 |        - |      128 |      128 |       0.12 | *
    |     2126 |        - |      127 |      127 |       0.12 | *
    |     2391 |        - |      126 |      126 |       0.12 | *
    |     2423 |        - |      125 |      125 |       0.12 | *
    |     2425 |        - |      124 |      124 |       0.12 | *
    |     2477 |        - |      123 |      123 |       0.12 | *
    |     2484 |        - |      122 |      122 |       0.12 | *
    |     2648 |        - |      121 |      121 |       0.12 | *
    |     2972 |        - |      120 |      120 |       0.13 | *
    |     2984 |        - |      119 |      119 |       0.13 | *
    |     3062 |        - |      118 |      118 |       0.13 | *
    |     3192 |        - |      117 |      117 |       0.13 | *
    |     3254 |        - |      116 |      116 |       0.13 | *
    |     3399 |        - |      115 |      115 |       0.13 | *
    |     3414 |        - |      114 |      114 |       0.13 | *
    |     3421 |        - |      113 |      113 |       0.13 | *
    |     3530 |        - |      112 |      112 |       0.13 | *
    |     3793 |        - |      111 |      111 |       0.14 | *
    |     3944 |        - |      110 |      110 |       0.14 | *
    |     4240 |        - |      108 |      108 |       0.14 | *
    |     4323 |        - |      107 |      107 |       0.14 | *
    |     4417 |        - |      106 |      106 |       0.14 | *
    |     4598 |        - |      105 |      105 |       0.14 | *
    |     4757 |        - |      104 |      104 |       0.14 | *
    |     5067 |        - |      103 |      103 |       0.14 | *
    |     5092 |        - |      102 |      102 |       0.14 | *
    |     5241 |        - |      101 |      101 |       0.14 | *
    |     6117 |        - |      100 |      100 |       0.15 | *
    |     6249 |        - |       99 |       99 |       0.15 | *
    |     6468 |        - |       98 |       98 |       0.15 | *
    |     6542 |        - |       97 |       97 |       0.15 | *
    |     6637 |        - |       96 |       96 |       0.15 | *
    |     6824 |        - |       95 |       95 |       0.15 | *
    |     7365 |        - |       94 |       94 |       0.15 | *
    |     7820 |        - |       93 |       93 |       0.16 | *
    |     7846 |        - |       92 |       92 |       0.16 | *
    |     8053 |        - |       91 |       91 |       0.16 | *
    |     8100 |        - |       90 |       90 |       0.16 | *
    |      10K |        - |       89 |       89 |       0.16 | *
    |      11K |        - |       88 |       88 |       0.16 | *
    |      12K |        - |       87 |       87 |       0.17 | *
    |      12K |        - |       86 |       86 |       0.17 | *
    |      16K |        - |       85 |       85 |       0.18 | *
    |      16K |        - |       84 |       84 |       0.18 | *
    |      16K |        - |       83 |       83 |       0.18 | *
    |      17K |        - |       82 |       82 |       0.18 | *
    |      17K |        - |       81 |       81 |       0.18 | *
    |      18K |        - |       80 |       80 |       0.19 | *
    |      20K |        - |       79 |       79 |       0.19 | *
    |      28K |        - |       78 |       78 |       0.21 | *
    |      29K |        - |       77 |       77 |       0.21 | *
    |      32K |        - |       76 |       76 |       0.21 | *
    |      32K |        - |       75 |       75 |       0.22 | *
    |      40K |        - |       74 |       74 |       0.23 | *
    |      43K |        - |       73 |       73 |       0.24 | *
    |      49K |        - |       72 |       72 |       0.25 | *
    |      52K |        - |       71 |       71 |       0.26 | *
    |      60K |        - |       70 |       70 |       0.27 | *
    |     359K |        - |       69 |       69 |       0.97 | *
    |     361K |        - |       68 |       68 |       0.97 | *
    |    6935K |        - |       67 |       67 |       5.92 | *
    |      40M |        - |       66 |       66 |      29.31 | *
    |      60M |        - |       65 |       65 |      42.20 | *
    |     181M |        - |       64 |       64 |     117.85 | *
    |     383M |        - |       63 |       63 |     243.75 | *
    |     832M |        - |       62 |       62 |     525.58 | *
    \--------------------------------------------------------/

Neighborhoods statistics (values in %):

    /----------------------------------------------------------------\
    | Move               | Improvs. | Sideways |  Accepts |  Rejects |
    |--------------------|----------|----------|----------|----------|
    | Shift(mk)          |     842K |    3262K |    4648K |     235M |
    | Shift              |      56K |      24M |      24M |     215M |
    | ShiftSmart(mk)     |    2940K |      12M |      17M |     223M |
    | ShiftSmart         |     196K |      64M |      65M |     175M |
    | SimpSwap(mk)       |     3815 |     8156 |      14K |     239M |
    | SimpSwap           |      500 |      27K |      31K |     240M |
    | SimpSwapSmart(mk)  |      21K |      45K |      78K |     240M |
    | SimpSwapSmart      |     2809 |     142K |     167K |     239M |
    | Swap(mk)           |     2582 |     5139 |     9281 |     239M |
    | Swap               |      346 |      19K |      22K |     240M |
    | SwapSmart(mk)      |      12K |      26K |      44K |     240M |
    | SwapSmart          |     1609 |      79K |      90K |     239M |
    | Switch(mk)         |     526K |      35M |      36M |     204M |
    | Switch             |      35K |      49M |      50M |     190M |
    | SwitchSmart(mk)    |    1504K |      30M |      33M |     207M |
    | SwitchSmart        |     101K |      31M |      32M |     208M |
    | TaskMove(mk)       |      22K |      41K |      73K |     239M |
    | TaskMove           |     1428 |      65K |      77K |     240M |
    | TaskMoveSmart(mk)  |      66K |     141K |     238K |     239M |
    | TaskMoveSmart      |     4413 |     216K |     252K |     239M |
    | 2-Shift(mk)        |     539K |    6104K |    6937K |     233M |
    | 2-Shift            |      36K |      22M |      22M |     218M |
    | 2-ShiftSmart(mk)   |    2451K |      19M |      22M |     218M |
    | 2-ShiftSmart       |     163K |      50M |      51M |     188M |
    \----------------------------------------------------------------/

Best makespan.....: 62
N. of Iterations..: 5750637972
Total runtime.....: 3600.02s
Using Python-MIP package version 1.6.8
Começo a execução
    /-------------------------------------------------------------------------------\
    |    Makespan   |   Free Jobs   | Free Machines |    It. Time   |   Total Time  | 
    |-------------------------------------------------------------------------------|
    \-------------------------------------------------------------------------------/
Terminou a execução
