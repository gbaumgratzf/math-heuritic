Instance....: instances/I_250_20_S_1-9_8.txt
Algorithm...: Simulated Annealing (alpha=0.990, saMax=10M, t0=1)
Other params: maxIters=100M, seed=-1, timeLimit=3600.00s

    /--------------------------------------------------------\
    |     Iter |   RDP(%) |       S* |       S' |       Time | 
    |----------|----------|----------|----------|------------|
    |        0 |        - |     1042 |     1042 |       0.00 | s0
    |       10 |        - |     1017 |     1017 |       0.03 | *
    |       12 |        - |     1012 |     1012 |       0.03 | *
    |       17 |        - |     1011 |     1011 |       0.03 | *
    |       20 |        - |      977 |      977 |       0.03 | *
    |       27 |        - |      951 |      951 |       0.03 | *
    |       28 |        - |      944 |      944 |       0.03 | *
    |       29 |        - |      940 |      940 |       0.03 | *
    |       32 |        - |      933 |      933 |       0.03 | *
    |       36 |        - |      931 |      931 |       0.04 | *
    |       37 |        - |      927 |      927 |       0.04 | *
    |       43 |        - |      912 |      912 |       0.04 | *
    |       44 |        - |      907 |      907 |       0.04 | *
    |       45 |        - |      903 |      903 |       0.04 | *
    |       46 |        - |      890 |      890 |       0.04 | *
    |       49 |        - |      889 |      889 |       0.04 | *
    |       52 |        - |      888 |      888 |       0.05 | *
    |       57 |        - |      884 |      884 |       0.05 | *
    |       58 |        - |      879 |      879 |       0.05 | *
    |       59 |        - |      876 |      876 |       0.05 | *
    |       61 |        - |      874 |      874 |       0.05 | *
    |       62 |        - |      869 |      869 |       0.05 | *
    |       63 |        - |      864 |      864 |       0.05 | *
    |       65 |        - |      863 |      863 |       0.05 | *
    |       66 |        - |      859 |      859 |       0.05 | *
    |       68 |        - |      855 |      855 |       0.05 | *
    |       70 |        - |      854 |      854 |       0.05 | *
    |       75 |        - |      851 |      851 |       0.06 | *
    |       78 |        - |      850 |      850 |       0.06 | *
    |       94 |        - |      848 |      848 |       0.06 | *
    |       96 |        - |      844 |      844 |       0.06 | *
    |      105 |        - |      842 |      842 |       0.06 | *
    |      108 |        - |      840 |      840 |       0.06 | *
    |      111 |        - |      833 |      833 |       0.06 | *
    |      113 |        - |      823 |      823 |       0.06 | *
    |      115 |        - |      820 |      820 |       0.06 | *
    |      117 |        - |      807 |      807 |       0.06 | *
    |      119 |        - |      800 |      800 |       0.07 | *
    |      121 |        - |      794 |      794 |       0.07 | *
    |      122 |        - |      790 |      790 |       0.07 | *
    |      124 |        - |      787 |      787 |       0.07 | *
    |      127 |        - |      781 |      781 |       0.07 | *
    |      134 |        - |      779 |      779 |       0.07 | *
    |      135 |        - |      774 |      774 |       0.07 | *
    |      136 |        - |      771 |      771 |       0.07 | *
    |      149 |        - |      761 |      761 |       0.07 | *
    |      151 |        - |      760 |      760 |       0.07 | *
    |      154 |        - |      755 |      755 |       0.08 | *
    |      174 |        - |      753 |      753 |       0.08 | *
    |      177 |        - |      751 |      751 |       0.08 | *
    |      178 |        - |      748 |      748 |       0.08 | *
    |      181 |        - |      747 |      747 |       0.08 | *
    |      185 |        - |      744 |      744 |       0.08 | *
    |      188 |        - |      738 |      738 |       0.08 | *
    |      192 |        - |      737 |      737 |       0.08 | *
    |      197 |        - |      727 |      727 |       0.08 | *
    |      209 |        - |      726 |      726 |       0.08 | *
    |      218 |        - |      720 |      720 |       0.08 | *
    |      225 |        - |      717 |      717 |       0.09 | *
    |      230 |        - |      715 |      715 |       0.09 | *
    |      242 |        - |      709 |      709 |       0.09 | *
    |      244 |        - |      707 |      707 |       0.09 | *
    |      248 |        - |      699 |      699 |       0.09 | *
    |      249 |        - |      698 |      698 |       0.09 | *
    |      253 |        - |      697 |      697 |       0.09 | *
    |      256 |        - |      673 |      673 |       0.09 | *
    |      263 |        - |      669 |      669 |       0.09 | *
    |      265 |        - |      668 |      668 |       0.09 | *
    |      286 |        - |      666 |      666 |       0.10 | *
    |      290 |        - |      662 |      662 |       0.10 | *
    |      291 |        - |      659 |      659 |       0.10 | *
    |      300 |        - |      656 |      656 |       0.10 | *
    |      316 |        - |      652 |      652 |       0.10 | *
    |      321 |        - |      651 |      651 |       0.10 | *
    |      322 |        - |      648 |      648 |       0.10 | *
    |      323 |        - |      646 |      646 |       0.10 | *
    |      328 |        - |      645 |      645 |       0.10 | *
    |      329 |        - |      644 |      644 |       0.10 | *
    |      330 |        - |      640 |      640 |       0.10 | *
    |      338 |        - |      637 |      637 |       0.10 | *
    |      346 |        - |      636 |      636 |       0.11 | *
    |      352 |        - |      632 |      632 |       0.11 | *
    |      355 |        - |      629 |      629 |       0.11 | *
    |      356 |        - |      628 |      628 |       0.11 | *
    |      357 |        - |      626 |      626 |       0.11 | *
    |      362 |        - |      621 |      621 |       0.11 | *
    |      363 |        - |      619 |      619 |       0.11 | *
    |      368 |        - |      617 |      617 |       0.11 | *
    |      376 |        - |      616 |      616 |       0.11 | *
    |      389 |        - |      613 |      613 |       0.11 | *
    |      390 |        - |      612 |      612 |       0.11 | *
    |      395 |        - |      610 |      610 |       0.12 | *
    |      399 |        - |      609 |      609 |       0.12 | *
    |      410 |        - |      608 |      608 |       0.12 | *
    |      417 |        - |      606 |      606 |       0.12 | *
    |      420 |        - |      604 |      604 |       0.12 | *
    |      422 |        - |      603 |      603 |       0.12 | *
    |      431 |        - |      601 |      601 |       0.12 | *
    |      444 |        - |      600 |      600 |       0.12 | *
    |      457 |        - |      599 |      599 |       0.12 | *
    |      459 |        - |      595 |      595 |       0.12 | *
    |      465 |        - |      592 |      592 |       0.12 | *
    |      467 |        - |      591 |      591 |       0.12 | *
    |      475 |        - |      588 |      588 |       0.12 | *
    |      480 |        - |      587 |      587 |       0.12 | *
    |      491 |        - |      586 |      586 |       0.12 | *
    |      501 |        - |      585 |      585 |       0.12 | *
    |      510 |        - |      582 |      582 |       0.12 | *
    |      519 |        - |      581 |      581 |       0.12 | *
    |      523 |        - |      580 |      580 |       0.13 | *
    |      528 |        - |      578 |      578 |       0.13 | *
    |      530 |        - |      577 |      577 |       0.13 | *
    |      532 |        - |      576 |      576 |       0.13 | *
    |      535 |        - |      574 |      574 |       0.13 | *
    |      545 |        - |      572 |      572 |       0.13 | *
    |      552 |        - |      571 |      571 |       0.13 | *
    |      571 |        - |      570 |      570 |       0.13 | *
    |      574 |        - |      569 |      569 |       0.13 | *
    |      575 |        - |      568 |      568 |       0.13 | *
    |      593 |        - |      566 |      566 |       0.13 | *
    |      598 |        - |      565 |      565 |       0.13 | *
    |      612 |        - |      562 |      562 |       0.13 | *
    |      614 |        - |      558 |      558 |       0.13 | *
    |      619 |        - |      556 |      556 |       0.13 | *
    |      623 |        - |      552 |      552 |       0.13 | *
    |      625 |        - |      551 |      551 |       0.13 | *
    |      626 |        - |      548 |      548 |       0.14 | *
    |      627 |        - |      547 |      547 |       0.14 | *
    |      629 |        - |      546 |      546 |       0.14 | *
    |      630 |        - |      544 |      544 |       0.14 | *
    |      649 |        - |      543 |      543 |       0.14 | *
    |      650 |        - |      539 |      539 |       0.14 | *
    |      659 |        - |      537 |      537 |       0.14 | *
    |      678 |        - |      535 |      535 |       0.14 | *
    |      679 |        - |      531 |      531 |       0.14 | *
    |      700 |        - |      530 |      530 |       0.14 | *
    |      702 |        - |      529 |      529 |       0.14 | *
    |      703 |        - |      528 |      528 |       0.14 | *
    |      715 |        - |      527 |      527 |       0.14 | *
    |      717 |        - |      526 |      526 |       0.14 | *
    |      721 |        - |      525 |      525 |       0.14 | *
    |      733 |        - |      523 |      523 |       0.14 | *
    |      740 |        - |      522 |      522 |       0.14 | *
    |      759 |        - |      521 |      521 |       0.14 | *
    |      761 |        - |      520 |      520 |       0.14 | *
    |      763 |        - |      519 |      519 |       0.15 | *
    |      764 |        - |      518 |      518 |       0.15 | *
    |      776 |        - |      515 |      515 |       0.15 | *
    |      784 |        - |      511 |      511 |       0.15 | *
    |      805 |        - |      505 |      505 |       0.15 | *
    |      807 |        - |      501 |      501 |       0.15 | *
    |      808 |        - |      500 |      500 |       0.15 | *
    |      821 |        - |      499 |      499 |       0.15 | *
    |      826 |        - |      497 |      497 |       0.15 | *
    |      832 |        - |      496 |      496 |       0.15 | *
    |      849 |        - |      495 |      495 |       0.15 | *
    |      855 |        - |      492 |      492 |       0.15 | *
    |      857 |        - |      491 |      491 |       0.15 | *
    |      872 |        - |      490 |      490 |       0.15 | *
    |      874 |        - |      488 |      488 |       0.16 | *
    |      883 |        - |      487 |      487 |       0.16 | *
    |      885 |        - |      485 |      485 |       0.16 | *
    |      887 |        - |      483 |      483 |       0.16 | *
    |      891 |        - |      478 |      478 |       0.16 | *
    |      895 |        - |      475 |      475 |       0.16 | *
    |      898 |        - |      472 |      472 |       0.16 | *
    |      901 |        - |      471 |      471 |       0.16 | *
    |      904 |        - |      470 |      470 |       0.16 | *
    |      920 |        - |      469 |      469 |       0.16 | *
    |      925 |        - |      468 |      468 |       0.16 | *
    |      954 |        - |      467 |      467 |       0.16 | *
    |      955 |        - |      466 |      466 |       0.16 | *
    |      970 |        - |      465 |      465 |       0.16 | *
    |      972 |        - |      464 |      464 |       0.17 | *
    |      975 |        - |      463 |      463 |       0.17 | *
    |      987 |        - |      462 |      462 |       0.17 | *
    |      990 |        - |      456 |      456 |       0.17 | *
    |      999 |        - |      455 |      455 |       0.17 | *
    |     1012 |        - |      454 |      454 |       0.17 | *
    |     1032 |        - |      451 |      451 |       0.17 | *
    |     1042 |        - |      450 |      450 |       0.17 | *
    |     1044 |        - |      444 |      444 |       0.17 | *
    |     1045 |        - |      440 |      440 |       0.17 | *
    |     1080 |        - |      439 |      439 |       0.17 | *
    |     1120 |        - |      438 |      438 |       0.17 | *
    |     1138 |        - |      437 |      437 |       0.17 | *
    |     1143 |        - |      436 |      436 |       0.17 | *
    |     1147 |        - |      432 |      432 |       0.17 | *
    |     1174 |        - |      431 |      431 |       0.17 | *
    |     1178 |        - |      430 |      430 |       0.18 | *
    |     1180 |        - |      429 |      429 |       0.18 | *
    |     1187 |        - |      427 |      427 |       0.18 | *
    |     1201 |        - |      423 |      423 |       0.18 | *
    |     1214 |        - |      422 |      422 |       0.18 | *
    |     1224 |        - |      419 |      419 |       0.18 | *
    |     1230 |        - |      418 |      418 |       0.18 | *
    |     1231 |        - |      417 |      417 |       0.18 | *
    |     1246 |        - |      416 |      416 |       0.18 | *
    |     1260 |        - |      413 |      413 |       0.18 | *
    |     1262 |        - |      411 |      411 |       0.18 | *
    |     1266 |        - |      410 |      410 |       0.18 | *
    |     1308 |        - |      409 |      409 |       0.18 | *
    |     1315 |        - |      408 |      408 |       0.18 | *
    |     1384 |        - |      407 |      407 |       0.18 | *
    |     1419 |        - |      406 |      406 |       0.19 | *
    |     1434 |        - |      405 |      405 |       0.19 | *
    |     1455 |        - |      404 |      404 |       0.19 | *
    |     1484 |        - |      403 |      403 |       0.19 | *
    |     1501 |        - |      401 |      401 |       0.19 | *
    |     1504 |        - |      399 |      399 |       0.19 | *
    |     1506 |        - |      396 |      396 |       0.19 | *
    |     1526 |        - |      395 |      395 |       0.19 | *
    |     1530 |        - |      393 |      393 |       0.19 | *
    |     1544 |        - |      392 |      392 |       0.19 | *
    |     1549 |        - |      389 |      389 |       0.19 | *
    |     1573 |        - |      388 |      388 |       0.19 | *
    |     1593 |        - |      387 |      387 |       0.19 | *
    |     1600 |        - |      384 |      384 |       0.19 | *
    |     1610 |        - |      382 |      382 |       0.19 | *
    |     1675 |        - |      381 |      381 |       0.20 | *
    |     1676 |        - |      380 |      380 |       0.20 | *
    |     1713 |        - |      379 |      379 |       0.20 | *
    |     1714 |        - |      378 |      378 |       0.20 | *
    |     1752 |        - |      377 |      377 |       0.20 | *
    |     1768 |        - |      376 |      376 |       0.20 | *
    |     1783 |        - |      373 |      373 |       0.20 | *
    |     1806 |        - |      372 |      372 |       0.20 | *
    |     1823 |        - |      371 |      371 |       0.20 | *
    |     1839 |        - |      370 |      370 |       0.20 | *
    |     1885 |        - |      369 |      369 |       0.20 | *
    |     1890 |        - |      368 |      368 |       0.20 | *
    |     1903 |        - |      367 |      367 |       0.20 | *
    |     1925 |        - |      365 |      365 |       0.20 | *
    |     1928 |        - |      357 |      357 |       0.20 | *
    |     1941 |        - |      356 |      356 |       0.21 | *
    |     1953 |        - |      355 |      355 |       0.21 | *
    |     1962 |        - |      354 |      354 |       0.21 | *
    |     1966 |        - |      351 |      351 |       0.21 | *
    |     1967 |        - |      349 |      349 |       0.21 | *
    |     1973 |        - |      347 |      347 |       0.21 | *
    |     1979 |        - |      346 |      346 |       0.21 | *
    |     1985 |        - |      345 |      345 |       0.21 | *
    |     2014 |        - |      342 |      342 |       0.21 | *
    |     2050 |        - |      340 |      340 |       0.21 | *
    |     2052 |        - |      339 |      339 |       0.21 | *
    |     2055 |        - |      338 |      338 |       0.21 | *
    |     2064 |        - |      336 |      336 |       0.21 | *
    |     2065 |        - |      334 |      334 |       0.21 | *
    |     2110 |        - |      332 |      332 |       0.21 | *
    |     2118 |        - |      331 |      331 |       0.22 | *
    |     2169 |        - |      330 |      330 |       0.22 | *
    |     2193 |        - |      329 |      329 |       0.22 | *
    |     2197 |        - |      328 |      328 |       0.22 | *
    |     2203 |        - |      325 |      325 |       0.22 | *
    |     2246 |        - |      323 |      323 |       0.22 | *
    |     2253 |        - |      322 |      322 |       0.22 | *
    |     2260 |        - |      321 |      321 |       0.22 | *
    |     2261 |        - |      320 |      320 |       0.22 | *
    |     2279 |        - |      318 |      318 |       0.22 | *
    |     2299 |        - |      317 |      317 |       0.22 | *
    |     2325 |        - |      312 |      312 |       0.22 | *
    |     2338 |        - |      310 |      310 |       0.22 | *
    |     2404 |        - |      309 |      309 |       0.22 | *
    |     2406 |        - |      306 |      306 |       0.22 | *
    |     2427 |        - |      305 |      305 |       0.22 | *
    |     2436 |        - |      304 |      304 |       0.23 | *
    |     2451 |        - |      302 |      302 |       0.23 | *
    |     2471 |        - |      301 |      301 |       0.23 | *
    |     2482 |        - |      300 |      300 |       0.23 | *
    |     2493 |        - |      299 |      299 |       0.23 | *
    |     2521 |        - |      298 |      298 |       0.23 | *
    |     2562 |        - |      297 |      297 |       0.23 | *
    |     2677 |        - |      295 |      295 |       0.23 | *
    |     2694 |        - |      294 |      294 |       0.23 | *
    |     2700 |        - |      293 |      293 |       0.23 | *
    |     2702 |        - |      292 |      292 |       0.23 | *
    |     2712 |        - |      291 |      291 |       0.23 | *
    |     2727 |        - |      290 |      290 |       0.23 | *
    |     2745 |        - |      288 |      288 |       0.23 | *
    |     2747 |        - |      287 |      287 |       0.23 | *
    |     2748 |        - |      286 |      286 |       0.23 | *
    |     2755 |        - |      284 |      284 |       0.24 | *
    |     2777 |        - |      283 |      283 |       0.24 | *
    |     2793 |        - |      282 |      282 |       0.24 | *
    |     2813 |        - |      279 |      279 |       0.24 | *
    |     2814 |        - |      275 |      275 |       0.24 | *
    |     2848 |        - |      274 |      274 |       0.24 | *
    |     2856 |        - |      273 |      273 |       0.24 | *
    |     2873 |        - |      272 |      272 |       0.24 | *
    |     2881 |        - |      270 |      270 |       0.24 | *
    |     2896 |        - |      269 |      269 |       0.24 | *
    |     2902 |        - |      268 |      268 |       0.24 | *
    |     2919 |        - |      267 |      267 |       0.24 | *
    |     2963 |        - |      266 |      266 |       0.24 | *
    |     3020 |        - |      265 |      265 |       0.24 | *
    |     3117 |        - |      264 |      264 |       0.24 | *
    |     3163 |        - |      263 |      263 |       0.24 | *
    |     3182 |        - |      262 |      262 |       0.25 | *
    |     3246 |        - |      260 |      260 |       0.25 | *
    |     3300 |        - |      259 |      259 |       0.25 | *
    |     3339 |        - |      258 |      258 |       0.25 | *
    |     3341 |        - |      257 |      257 |       0.25 | *
    |     3378 |        - |      256 |      256 |       0.25 | *
    |     3394 |        - |      255 |      255 |       0.25 | *
    |     3412 |        - |      254 |      254 |       0.25 | *
    |     3430 |        - |      253 |      253 |       0.25 | *
    |     3452 |        - |      252 |      252 |       0.25 | *
    |     3494 |        - |      251 |      251 |       0.25 | *
    |     3511 |        - |      250 |      250 |       0.25 | *
    |     3579 |        - |      248 |      248 |       0.25 | *
    |     3646 |        - |      247 |      247 |       0.25 | *
    |     3648 |        - |      246 |      246 |       0.25 | *
    |     3672 |        - |      245 |      245 |       0.26 | *
    |     3684 |        - |      244 |      244 |       0.26 | *
    |     3692 |        - |      243 |      243 |       0.26 | *
    |     3727 |        - |      241 |      241 |       0.26 | *
    |     3982 |        - |      240 |      240 |       0.26 | *
    |     4016 |        - |      239 |      239 |       0.26 | *
    |     4037 |        - |      238 |      238 |       0.26 | *
    |     4044 |        - |      236 |      236 |       0.26 | *
    |     4052 |        - |      235 |      235 |       0.26 | *
    |     4061 |        - |      234 |      234 |       0.26 | *
    |     4070 |        - |      233 |      233 |       0.26 | *
    |     4115 |        - |      232 |      232 |       0.26 | *
    |     4183 |        - |      231 |      231 |       0.26 | *
    |     4194 |        - |      230 |      230 |       0.26 | *
    |     4208 |        - |      229 |      229 |       0.26 | *
    |     4223 |        - |      228 |      228 |       0.26 | *
    |     4275 |        - |      226 |      226 |       0.27 | *
    |     4282 |        - |      225 |      225 |       0.27 | *
    |     4301 |        - |      224 |      224 |       0.27 | *
    |     4308 |        - |      223 |      223 |       0.27 | *
    |     4400 |        - |      222 |      222 |       0.27 | *
    |     4469 |        - |      221 |      221 |       0.27 | *
    |     4471 |        - |      220 |      220 |       0.27 | *
    |     4498 |        - |      219 |      219 |       0.27 | *
    |     4515 |        - |      218 |      218 |       0.27 | *
    |     4525 |        - |      217 |      217 |       0.27 | *
    |     4566 |        - |      216 |      216 |       0.27 | *
    |     4659 |        - |      215 |      215 |       0.27 | *
    |     4686 |        - |      214 |      214 |       0.27 | *
    |     4697 |        - |      213 |      213 |       0.27 | *
    |     4717 |        - |      212 |      212 |       0.27 | *
    |     4720 |        - |      211 |      211 |       0.27 | *
    |     4740 |        - |      210 |      210 |       0.27 | *
    |     4756 |        - |      209 |      209 |       0.27 | *
    |     5462 |        - |      208 |      208 |       0.28 | *
    |     5490 |        - |      207 |      207 |       0.28 | *
    |     5609 |        - |      206 |      206 |       0.28 | *
    |     5675 |        - |      205 |      205 |       0.28 | *
    |     5692 |        - |      204 |      204 |       0.28 | *
    |     5702 |        - |      203 |      203 |       0.28 | *
    |     5740 |        - |      201 |      201 |       0.28 | *
    |     5795 |        - |      200 |      200 |       0.28 | *
    |     5802 |        - |      199 |      199 |       0.28 | *
    |     5843 |        - |      198 |      198 |       0.28 | *
    |     5938 |        - |      197 |      197 |       0.28 | *
    |     6105 |        - |      196 |      196 |       0.29 | *
    |     6130 |        - |      195 |      195 |       0.29 | *
    |     6194 |        - |      194 |      194 |       0.29 | *
    |     6239 |        - |      193 |      193 |       0.29 | *
    |     6248 |        - |      192 |      192 |       0.29 | *
    |     6254 |        - |      191 |      191 |       0.29 | *
    |     6747 |        - |      190 |      190 |       0.29 | *
    |     6752 |        - |      189 |      189 |       0.29 | *
    |     6846 |        - |      188 |      188 |       0.29 | *
    |     6893 |        - |      187 |      187 |       0.29 | *
    |     7061 |        - |      186 |      186 |       0.29 | *
    |     7159 |        - |      185 |      185 |       0.29 | *
    |     7559 |        - |      184 |      184 |       0.30 | *
    |     7667 |        - |      183 |      183 |       0.30 | *
    |     7692 |        - |      182 |      182 |       0.30 | *
    |     7708 |        - |      181 |      181 |       0.30 | *
    |     7862 |        - |      180 |      180 |       0.30 | *
    |     7964 |        - |      179 |      179 |       0.30 | *
    |     8033 |        - |      178 |      178 |       0.30 | *
    |     8034 |        - |      177 |      177 |       0.30 | *
    |     8047 |        - |      176 |      176 |       0.30 | *
    |     8109 |        - |      175 |      175 |       0.30 | *
    |     8202 |        - |      174 |      174 |       0.30 | *
    |     8203 |        - |      173 |      173 |       0.30 | *
    |     8209 |        - |      172 |      172 |       0.31 | *
    |     8212 |        - |      171 |      171 |       0.31 | *
    |     8256 |        - |      170 |      170 |       0.31 | *
    |     8439 |        - |      169 |      169 |       0.31 | *
    |     8995 |        - |      168 |      168 |       0.31 | *
    |     9043 |        - |      167 |      167 |       0.31 | *
    |     9502 |        - |      166 |      166 |       0.31 | *
    |     9714 |        - |      165 |      165 |       0.31 | *
    |     9840 |        - |      164 |      164 |       0.31 | *
    |     9844 |        - |      163 |      163 |       0.32 | *
    |      10K |        - |      162 |      162 |       0.32 | *
    |      10K |        - |      161 |      161 |       0.32 | *
    |      10K |        - |      160 |      160 |       0.32 | *
    |      11K |        - |      159 |      159 |       0.32 | *
    |      11K |        - |      158 |      158 |       0.32 | *
    |      12K |        - |      157 |      157 |       0.33 | *
    |      12K |        - |      156 |      156 |       0.33 | *
    |      12K |        - |      154 |      154 |       0.33 | *
    |      12K |        - |      153 |      153 |       0.33 | *
    |      12K |        - |      152 |      152 |       0.33 | *
    |      12K |        - |      151 |      151 |       0.33 | *
    |      12K |        - |      150 |      150 |       0.33 | *
    |      13K |        - |      149 |      149 |       0.33 | *
    |      13K |        - |      148 |      148 |       0.33 | *
    |      13K |        - |      147 |      147 |       0.33 | *
    |      13K |        - |      146 |      146 |       0.33 | *
    |      13K |        - |      145 |      145 |       0.33 | *
    |      13K |        - |      144 |      144 |       0.34 | *
    |      14K |        - |      143 |      143 |       0.34 | *
    |      14K |        - |      142 |      142 |       0.34 | *
    |      14K |        - |      141 |      141 |       0.34 | *
    |      14K |        - |      140 |      140 |       0.34 | *
    |      16K |        - |      139 |      139 |       0.34 | *
    |      16K |        - |      138 |      138 |       0.35 | *
    |      16K |        - |      137 |      137 |       0.35 | *
    |      16K |        - |      136 |      136 |       0.35 | *
    |      16K |        - |      135 |      135 |       0.35 | *
    |      16K |        - |      134 |      134 |       0.35 | *
    |      17K |        - |      133 |      133 |       0.35 | *
    |      17K |        - |      132 |      132 |       0.35 | *
    |      17K |        - |      131 |      131 |       0.35 | *
    |      18K |        - |      130 |      130 |       0.35 | *
    |      19K |        - |      129 |      129 |       0.36 | *
    |      19K |        - |      128 |      128 |       0.36 | *
    |      20K |        - |      127 |      127 |       0.36 | *
    |      20K |        - |      126 |      126 |       0.36 | *
    |      21K |        - |      125 |      125 |       0.37 | *
    |      21K |        - |      124 |      124 |       0.37 | *
    |      21K |        - |      123 |      123 |       0.37 | *
    |      25K |        - |      122 |      122 |       0.38 | *
    |      26K |        - |      121 |      121 |       0.38 | *
    |      26K |        - |      120 |      120 |       0.38 | *
    |      27K |        - |      119 |      119 |       0.39 | *
    |      28K |        - |      118 |      118 |       0.39 | *
    |      30K |        - |      117 |      117 |       0.40 | *
    |      30K |        - |      116 |      116 |       0.40 | *
    |      34K |        - |      115 |      115 |       0.41 | *
    |      34K |        - |      114 |      114 |       0.41 | *
    |      41K |        - |      113 |      113 |       0.43 | *
    |      41K |        - |      112 |      112 |       0.44 | *
    |      41K |        - |      111 |      111 |       0.44 | *
    |      42K |        - |      110 |      110 |       0.44 | *
    |      42K |        - |      109 |      109 |       0.44 | *
    |      48K |        - |      108 |      108 |       0.46 | *
    |      56K |        - |      107 |      107 |       0.49 | *
    |      58K |        - |      106 |      106 |       0.50 | *
    |      59K |        - |      105 |      105 |       0.50 | *
    |      59K |        - |      104 |      104 |       0.50 | *
    |      73K |        - |      103 |      103 |       0.54 | *
    |      73K |        - |      102 |      102 |       0.54 | *
    |      96K |        - |      101 |      101 |       0.63 | *
    |     182K |        - |      100 |      100 |       0.95 | *
    |     259K |        - |       99 |       99 |       1.17 | *
    |     366K |        - |       98 |       98 |       1.38 | *
    |     618K |        - |       97 |       97 |       1.70 | *
    |     672K |        - |       96 |       96 |       1.76 | *
    |     679K |        - |       95 |       95 |       1.77 | *
    |     715K |        - |       94 |       94 |       1.80 | *
    |     716K |        - |       93 |       93 |       1.80 | *
    |      11M |        - |       92 |       92 |       9.97 | *
    |      20M |        - |       91 |       91 |      16.85 | *
    |     113M |        - |       90 |       90 |      88.29 | *
    |     153M |        - |       89 |       89 |     119.46 | *
    |     202M |        - |       88 |       88 |     156.91 | *
    |     231M |        - |       87 |       87 |     179.17 | *
    |     295M |        - |       86 |       86 |     228.11 | *
    |     382M |        - |       85 |       85 |     294.97 | *
    |     446M |        - |       84 |       84 |     343.90 | *
    |     511M |        - |       83 |       83 |     393.06 | *
    |     517M |        - |       82 |       82 |     397.96 | *
    |     642M |        - |       81 |       81 |     492.98 | *
    |     752M |        - |       80 |       80 |     576.67 | *
    |     783M |        - |       79 |       79 |     599.67 | *
    |     980M |        - |       78 |       78 |     749.16 | *
    |    1205M |        - |       77 |       77 |     918.80 | *
    |    1821M |        - |       76 |       76 |    1380.86 | *
    \--------------------------------------------------------/

Neighborhoods statistics (values in %):

    /----------------------------------------------------------------\
    | Move               | Improvs. | Sideways |  Accepts |  Rejects |
    |--------------------|----------|----------|----------|----------|
    | Shift(mk)          |     525K |    2891K |    3815K |     197M |
    | Shift              |      26K |      13M |      13M |     188M |
    | ShiftSmart(mk)     |    2989K |      21M |      25M |     176M |
    | ShiftSmart         |     150K |      52M |      53M |     148M |
    | SimpSwap(mk)       |     2030 |     5197 |     8822 |     201M |
    | SimpSwap           |      212 |      17K |      21K |     201M |
    | SimpSwapSmart(mk)  |      21K |      53K |      89K |     201M |
    | SimpSwapSmart      |     2130 |     170K |     199K |     201M |
    | Swap(mk)           |     1142 |     2597 |     4667 |     201M |
    | Swap               |      128 |     9868 |      12K |     201M |
    | SwapSmart(mk)      |     9228 |      24K |      39K |     201M |
    | SwapSmart          |      932 |      66K |      75K |     201M |
    | Switch(mk)         |     316K |      17M |      17M |     184M |
    | Switch             |      16K |      23M |      24M |     178M |
    | SwitchSmart(mk)    |    1601K |    7361K |    9743K |     192M |
    | SwitchSmart        |      80K |      25M |      26M |     175M |
    | TaskMove(mk)       |      25K |      61K |     102K |     201M |
    | TaskMove           |     1235 |      92K |     108K |     201M |
    | TaskMoveSmart(mk)  |     107K |     299K |     461K |     201M |
    | TaskMoveSmart      |     5746 |     437K |     496K |     201M |
    | 2-Shift(mk)        |     237K |    2206K |    2589K |     199M |
    | 2-Shift            |      12K |    6508K |    6845K |     194M |
    | 2-ShiftSmart(mk)   |    2568K |      12M |      16M |     186M |
    | 2-ShiftSmart       |     128K |      33M |      34M |     168M |
    \----------------------------------------------------------------/

Best makespan.....: 76
N. of Iterations..: 4830155488
Total runtime.....: 3600.02s
Using Python-MIP package version 1.6.8
Começo a execução
    /-------------------------------------------------------------------------------\
    |    Makespan   |   Free Jobs   | Free Machines |    It. Time   |   Total Time  | 
    |-------------------------------------------------------------------------------|
    \-------------------------------------------------------------------------------/
Terminou a execução
