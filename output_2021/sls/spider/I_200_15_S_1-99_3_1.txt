Instance....: instances/I_200_15_S_1-99_3.txt
Algorithm...: Simulated Annealing (alpha=0.990, saMax=10M, t0=1)
Other params: maxIters=100M, seed=-1, timeLimit=3600.00s

    /--------------------------------------------------------\
    |     Iter |   RDP(%) |       S* |       S' |       Time | 
    |----------|----------|----------|----------|------------|
    |        0 |        - |     2082 |     2082 |       0.00 | s0
    |        1 |        - |     1915 |     1915 |       0.00 | *
    |        2 |        - |     1889 |     1889 |       0.01 | *
    |        3 |        - |     1858 |     1858 |       0.01 | *
    |        4 |        - |     1855 |     1855 |       0.01 | *
    |        7 |        - |     1752 |     1752 |       0.01 | *
    |        9 |        - |     1728 |     1728 |       0.01 | *
    |       10 |        - |     1680 |     1680 |       0.01 | *
    |       11 |        - |     1679 |     1679 |       0.01 | *
    |       15 |        - |     1643 |     1643 |       0.03 | *
    |       18 |        - |     1632 |     1632 |       0.03 | *
    |       19 |        - |     1621 |     1621 |       0.03 | *
    |       29 |        - |     1614 |     1614 |       0.03 | *
    |       30 |        - |     1599 |     1599 |       0.03 | *
    |       39 |        - |     1541 |     1541 |       0.04 | *
    |       40 |        - |     1516 |     1516 |       0.04 | *
    |       43 |        - |     1498 |     1498 |       0.04 | *
    |       48 |        - |     1468 |     1468 |       0.04 | *
    |       51 |        - |     1462 |     1462 |       0.04 | *
    |       52 |        - |     1453 |     1453 |       0.04 | *
    |       53 |        - |     1420 |     1420 |       0.04 | *
    |       54 |        - |     1410 |     1410 |       0.04 | *
    |       55 |        - |     1384 |     1384 |       0.04 | *
    |       57 |        - |     1355 |     1355 |       0.04 | *
    |       62 |        - |     1348 |     1348 |       0.04 | *
    |       67 |        - |     1340 |     1340 |       0.04 | *
    |       68 |        - |     1338 |     1338 |       0.04 | *
    |       69 |        - |     1336 |     1336 |       0.04 | *
    |       70 |        - |     1308 |     1308 |       0.04 | *
    |       82 |        - |     1288 |     1288 |       0.04 | *
    |       92 |        - |     1265 |     1265 |       0.05 | *
    |       94 |        - |     1255 |     1255 |       0.05 | *
    |       95 |        - |     1249 |     1249 |       0.05 | *
    |       96 |        - |     1245 |     1245 |       0.05 | *
    |      103 |        - |     1234 |     1234 |       0.05 | *
    |      118 |        - |     1221 |     1221 |       0.05 | *
    |      119 |        - |     1214 |     1214 |       0.05 | *
    |      120 |        - |     1213 |     1213 |       0.05 | *
    |      122 |        - |     1210 |     1210 |       0.05 | *
    |      124 |        - |     1181 |     1181 |       0.05 | *
    |      125 |        - |     1175 |     1175 |       0.05 | *
    |      129 |        - |     1165 |     1165 |       0.05 | *
    |      132 |        - |     1159 |     1159 |       0.05 | *
    |      140 |        - |     1143 |     1143 |       0.05 | *
    |      143 |        - |     1141 |     1141 |       0.05 | *
    |      151 |        - |     1138 |     1138 |       0.06 | *
    |      155 |        - |     1106 |     1106 |       0.06 | *
    |      165 |        - |     1105 |     1105 |       0.06 | *
    |      166 |        - |     1098 |     1098 |       0.06 | *
    |      176 |        - |     1095 |     1095 |       0.06 | *
    |      178 |        - |     1090 |     1090 |       0.06 | *
    |      200 |        - |     1080 |     1080 |       0.06 | *
    |      207 |        - |     1079 |     1079 |       0.06 | *
    |      228 |        - |     1070 |     1070 |       0.06 | *
    |      263 |        - |     1063 |     1063 |       0.06 | *
    |      267 |        - |     1062 |     1062 |       0.06 | *
    |      283 |        - |     1041 |     1041 |       0.06 | *
    |      285 |        - |     1039 |     1039 |       0.06 | *
    |      299 |        - |     1033 |     1033 |       0.06 | *
    |      304 |        - |     1030 |     1030 |       0.06 | *
    |      309 |        - |     1029 |     1029 |       0.07 | *
    |      312 |        - |     1028 |     1028 |       0.07 | *
    |      318 |        - |     1018 |     1018 |       0.07 | *
    |      321 |        - |     1007 |     1007 |       0.07 | *
    |      329 |        - |     1006 |     1006 |       0.07 | *
    |      332 |        - |      999 |      999 |       0.07 | *
    |      374 |        - |      991 |      991 |       0.07 | *
    |      382 |        - |      984 |      984 |       0.07 | *
    |      391 |        - |      972 |      972 |       0.07 | *
    |      419 |        - |      955 |      955 |       0.07 | *
    |      441 |        - |      949 |      949 |       0.07 | *
    |      452 |        - |      948 |      948 |       0.07 | *
    |      461 |        - |      946 |      946 |       0.07 | *
    |      464 |        - |      939 |      939 |       0.07 | *
    |      471 |        - |      933 |      933 |       0.07 | *
    |      472 |        - |      931 |      931 |       0.07 | *
    |      484 |        - |      930 |      930 |       0.08 | *
    |      508 |        - |      922 |      922 |       0.08 | *
    |      511 |        - |      919 |      919 |       0.08 | *
    |      513 |        - |      907 |      907 |       0.08 | *
    |      525 |        - |      902 |      902 |       0.08 | *
    |      530 |        - |      887 |      887 |       0.08 | *
    |      539 |        - |      881 |      881 |       0.08 | *
    |      543 |        - |      880 |      880 |       0.08 | *
    |      544 |        - |      878 |      878 |       0.08 | *
    |      547 |        - |      873 |      873 |       0.08 | *
    |      551 |        - |      868 |      868 |       0.08 | *
    |      573 |        - |      866 |      866 |       0.08 | *
    |      582 |        - |      861 |      861 |       0.08 | *
    |      599 |        - |      859 |      859 |       0.08 | *
    |      606 |        - |      856 |      856 |       0.08 | *
    |      611 |        - |      853 |      853 |       0.08 | *
    |      613 |        - |      850 |      850 |       0.09 | *
    |      620 |        - |      849 |      849 |       0.09 | *
    |      624 |        - |      848 |      848 |       0.09 | *
    |      628 |        - |      847 |      847 |       0.09 | *
    |      705 |        - |      846 |      846 |       0.09 | *
    |      721 |        - |      843 |      843 |       0.09 | *
    |      733 |        - |      837 |      837 |       0.09 | *
    |      736 |        - |      836 |      836 |       0.09 | *
    |      742 |        - |      833 |      833 |       0.09 | *
    |      749 |        - |      823 |      823 |       0.09 | *
    |      795 |        - |      817 |      817 |       0.09 | *
    |      818 |        - |      813 |      813 |       0.09 | *
    |      821 |        - |      811 |      811 |       0.09 | *
    |      861 |        - |      804 |      804 |       0.09 | *
    |      905 |        - |      803 |      803 |       0.09 | *
    |      906 |        - |      794 |      794 |       0.09 | *
    |      908 |        - |      784 |      784 |       0.09 | *
    |      914 |        - |      783 |      783 |       0.09 | *
    |      924 |        - |      777 |      777 |       0.10 | *
    |      930 |        - |      776 |      776 |       0.10 | *
    |      976 |        - |      772 |      772 |       0.10 | *
    |      979 |        - |      770 |      770 |       0.10 | *
    |      987 |        - |      768 |      768 |       0.10 | *
    |      990 |        - |      767 |      767 |       0.10 | *
    |      998 |        - |      762 |      762 |       0.10 | *
    |     1089 |        - |      758 |      758 |       0.10 | *
    |     1108 |        - |      754 |      754 |       0.10 | *
    |     1129 |        - |      752 |      752 |       0.10 | *
    |     1140 |        - |      749 |      749 |       0.10 | *
    |     1150 |        - |      747 |      747 |       0.10 | *
    |     1172 |        - |      743 |      743 |       0.10 | *
    |     1219 |        - |      735 |      735 |       0.10 | *
    |     1220 |        - |      733 |      733 |       0.10 | *
    |     1224 |        - |      732 |      732 |       0.10 | *
    |     1239 |        - |      726 |      726 |       0.11 | *
    |     1250 |        - |      724 |      724 |       0.11 | *
    |     1312 |        - |      719 |      719 |       0.11 | *
    |     1345 |        - |      718 |      718 |       0.11 | *
    |     1404 |        - |      717 |      717 |       0.11 | *
    |     1426 |        - |      716 |      716 |       0.11 | *
    |     1440 |        - |      712 |      712 |       0.11 | *
    |     1446 |        - |      711 |      711 |       0.11 | *
    |     1456 |        - |      705 |      705 |       0.11 | *
    |     1500 |        - |      704 |      704 |       0.11 | *
    |     1504 |        - |      702 |      702 |       0.11 | *
    |     1542 |        - |      701 |      701 |       0.11 | *
    |     1592 |        - |      700 |      700 |       0.11 | *
    |     1691 |        - |      696 |      696 |       0.11 | *
    |     1692 |        - |      695 |      695 |       0.11 | *
    |     1696 |        - |      694 |      694 |       0.11 | *
    |     1724 |        - |      692 |      692 |       0.12 | *
    |     1736 |        - |      691 |      691 |       0.12 | *
    |     1739 |        - |      688 |      688 |       0.12 | *
    |     1749 |        - |      687 |      687 |       0.12 | *
    |     1757 |        - |      685 |      685 |       0.12 | *
    |     1793 |        - |      683 |      683 |       0.12 | *
    |     1800 |        - |      681 |      681 |       0.12 | *
    |     1866 |        - |      677 |      677 |       0.12 | *
    |     1896 |        - |      672 |      672 |       0.12 | *
    |     1916 |        - |      671 |      671 |       0.12 | *
    |     1923 |        - |      667 |      667 |       0.12 | *
    |     1945 |        - |      664 |      664 |       0.12 | *
    |     1972 |        - |      660 |      660 |       0.12 | *
    |     1996 |        - |      659 |      659 |       0.12 | *
    |     2005 |        - |      657 |      657 |       0.12 | *
    |     2009 |        - |      655 |      655 |       0.12 | *
    |     2055 |        - |      652 |      652 |       0.12 | *
    |     2060 |        - |      648 |      648 |       0.12 | *
    |     2063 |        - |      643 |      643 |       0.12 | *
    |     2109 |        - |      642 |      642 |       0.12 | *
    |     2112 |        - |      637 |      637 |       0.13 | *
    |     2151 |        - |      634 |      634 |       0.13 | *
    |     2153 |        - |      632 |      632 |       0.13 | *
    |     2228 |        - |      631 |      631 |       0.13 | *
    |     2266 |        - |      630 |      630 |       0.13 | *
    |     2286 |        - |      624 |      624 |       0.13 | *
    |     2308 |        - |      621 |      621 |       0.13 | *
    |     2389 |        - |      620 |      620 |       0.13 | *
    |     2393 |        - |      614 |      614 |       0.13 | *
    |     2457 |        - |      612 |      612 |       0.13 | *
    |     2478 |        - |      609 |      609 |       0.13 | *
    |     2502 |        - |      608 |      608 |       0.13 | *
    |     2508 |        - |      606 |      606 |       0.13 | *
    |     2522 |        - |      603 |      603 |       0.13 | *
    |     2610 |        - |      601 |      601 |       0.13 | *
    |     2622 |        - |      600 |      600 |       0.13 | *
    |     2639 |        - |      599 |      599 |       0.14 | *
    |     2729 |        - |      598 |      598 |       0.14 | *
    |     2832 |        - |      597 |      597 |       0.14 | *
    |     2925 |        - |      592 |      592 |       0.14 | *
    |     2936 |        - |      590 |      590 |       0.14 | *
    |     2944 |        - |      589 |      589 |       0.14 | *
    |     3301 |        - |      587 |      587 |       0.14 | *
    |     3426 |        - |      585 |      585 |       0.14 | *
    |     3500 |        - |      583 |      583 |       0.14 | *
    |     3549 |        - |      582 |      582 |       0.14 | *
    |     3556 |        - |      581 |      581 |       0.14 | *
    |     3606 |        - |      580 |      580 |       0.14 | *
    |     3686 |        - |      578 |      578 |       0.14 | *
    |     3688 |        - |      574 |      574 |       0.14 | *
    |     3719 |        - |      569 |      569 |       0.14 | *
    |     3749 |        - |      565 |      565 |       0.14 | *
    |     3817 |        - |      563 |      563 |       0.15 | *
    |     3837 |        - |      562 |      562 |       0.15 | *
    |     3840 |        - |      558 |      558 |       0.15 | *
    |     3975 |        - |      557 |      557 |       0.15 | *
    |     3986 |        - |      556 |      556 |       0.15 | *
    |     4154 |        - |      552 |      552 |       0.15 | *
    |     4255 |        - |      550 |      550 |       0.15 | *
    |     4258 |        - |      548 |      548 |       0.15 | *
    |     4264 |        - |      546 |      546 |       0.15 | *
    |     4320 |        - |      545 |      545 |       0.15 | *
    |     4485 |        - |      543 |      543 |       0.15 | *
    |     4531 |        - |      542 |      542 |       0.15 | *
    |     4582 |        - |      540 |      540 |       0.15 | *
    |     4600 |        - |      539 |      539 |       0.15 | *
    |     4727 |        - |      538 |      538 |       0.15 | *
    |     4822 |        - |      534 |      534 |       0.15 | *
    |     5077 |        - |      533 |      533 |       0.15 | *
    |     5135 |        - |      527 |      527 |       0.15 | *
    |     5186 |        - |      526 |      526 |       0.15 | *
    |     5229 |        - |      525 |      525 |       0.16 | *
    |     5233 |        - |      521 |      521 |       0.16 | *
    |     5341 |        - |      520 |      520 |       0.16 | *
    |     5351 |        - |      518 |      518 |       0.16 | *
    |     5529 |        - |      517 |      517 |       0.16 | *
    |     6029 |        - |      507 |      507 |       0.16 | *
    |     6154 |        - |      506 |      506 |       0.16 | *
    |     6183 |        - |      503 |      503 |       0.16 | *
    |     6270 |        - |      499 |      499 |       0.16 | *
    |     6440 |        - |      498 |      498 |       0.16 | *
    |     6482 |        - |      497 |      497 |       0.16 | *
    |     6491 |        - |      495 |      495 |       0.16 | *
    |     6643 |        - |      494 |      494 |       0.16 | *
    |     6656 |        - |      492 |      492 |       0.16 | *
    |     6669 |        - |      491 |      491 |       0.16 | *
    |     6771 |        - |      489 |      489 |       0.16 | *
    |     6941 |        - |      488 |      488 |       0.16 | *
    |     6974 |        - |      486 |      486 |       0.16 | *
    |     7232 |        - |      485 |      485 |       0.16 | *
    |     7286 |        - |      483 |      483 |       0.16 | *
    |     7369 |        - |      481 |      481 |       0.17 | *
    |     7387 |        - |      480 |      480 |       0.17 | *
    |     7397 |        - |      478 |      478 |       0.17 | *
    |     7418 |        - |      477 |      477 |       0.17 | *
    |     7539 |        - |      476 |      476 |       0.17 | *
    |     7585 |        - |      474 |      474 |       0.17 | *
    |     7625 |        - |      473 |      473 |       0.17 | *
    |     7661 |        - |      472 |      472 |       0.17 | *
    |     7884 |        - |      471 |      471 |       0.17 | *
    |     8003 |        - |      470 |      470 |       0.17 | *
    |     8102 |        - |      469 |      469 |       0.17 | *
    |     8175 |        - |      467 |      467 |       0.17 | *
    |     8177 |        - |      466 |      466 |       0.17 | *
    |     8205 |        - |      465 |      465 |       0.17 | *
    |     8214 |        - |      462 |      462 |       0.17 | *
    |     8240 |        - |      461 |      461 |       0.17 | *
    |     8299 |        - |      460 |      460 |       0.17 | *
    |     8452 |        - |      458 |      458 |       0.17 | *
    |     8465 |        - |      457 |      457 |       0.17 | *
    |     8715 |        - |      454 |      454 |       0.18 | *
    |     8749 |        - |      452 |      452 |       0.18 | *
    |     8789 |        - |      451 |      451 |       0.18 | *
    |     8790 |        - |      450 |      450 |       0.18 | *
    |     8896 |        - |      448 |      448 |       0.18 | *
    |     9639 |        - |      447 |      447 |       0.18 | *
    |     9711 |        - |      446 |      446 |       0.18 | *
    |      10K |        - |      445 |      445 |       0.18 | *
    |      10K |        - |      443 |      443 |       0.18 | *
    |      11K |        - |      442 |      442 |       0.18 | *
    |      11K |        - |      439 |      439 |       0.19 | *
    |      11K |        - |      438 |      438 |       0.19 | *
    |      11K |        - |      436 |      436 |       0.19 | *
    |      11K |        - |      434 |      434 |       0.19 | *
    |      11K |        - |      433 |      433 |       0.19 | *
    |      11K |        - |      432 |      432 |       0.19 | *
    |      11K |        - |      431 |      431 |       0.19 | *
    |      11K |        - |      429 |      429 |       0.19 | *
    |      12K |        - |      427 |      427 |       0.19 | *
    |      12K |        - |      426 |      426 |       0.19 | *
    |      12K |        - |      425 |      425 |       0.19 | *
    |      12K |        - |      424 |      424 |       0.19 | *
    |      12K |        - |      421 |      421 |       0.19 | *
    |      12K |        - |      416 |      416 |       0.19 | *
    |      13K |        - |      414 |      414 |       0.19 | *
    |      13K |        - |      410 |      410 |       0.19 | *
    |      13K |        - |      409 |      409 |       0.20 | *
    |      13K |        - |      408 |      408 |       0.20 | *
    |      13K |        - |      406 |      406 |       0.20 | *
    |      13K |        - |      405 |      405 |       0.20 | *
    |      14K |        - |      404 |      404 |       0.20 | *
    |      14K |        - |      402 |      402 |       0.20 | *
    |      14K |        - |      399 |      399 |       0.20 | *
    |      14K |        - |      398 |      398 |       0.20 | *
    |      14K |        - |      396 |      396 |       0.20 | *
    |      14K |        - |      393 |      393 |       0.20 | *
    |      14K |        - |      391 |      391 |       0.20 | *
    |      14K |        - |      390 |      390 |       0.20 | *
    |      14K |        - |      389 |      389 |       0.20 | *
    |      15K |        - |      388 |      388 |       0.20 | *
    |      15K |        - |      387 |      387 |       0.20 | *
    |      15K |        - |      386 |      386 |       0.20 | *
    |      15K |        - |      385 |      385 |       0.20 | *
    |      15K |        - |      382 |      382 |       0.20 | *
    |      16K |        - |      379 |      379 |       0.21 | *
    |      16K |        - |      378 |      378 |       0.21 | *
    |      17K |        - |      377 |      377 |       0.21 | *
    |      17K |        - |      376 |      376 |       0.21 | *
    |      17K |        - |      375 |      375 |       0.21 | *
    |      17K |        - |      374 |      374 |       0.21 | *
    |      18K |        - |      373 |      373 |       0.21 | *
    |      18K |        - |      370 |      370 |       0.21 | *
    |      18K |        - |      367 |      367 |       0.21 | *
    |      19K |        - |      363 |      363 |       0.21 | *
    |      19K |        - |      362 |      362 |       0.21 | *
    |      19K |        - |      361 |      361 |       0.21 | *
    |      20K |        - |      360 |      360 |       0.22 | *
    |      20K |        - |      358 |      358 |       0.22 | *
    |      20K |        - |      357 |      357 |       0.22 | *
    |      22K |        - |      356 |      356 |       0.22 | *
    |      22K |        - |      354 |      354 |       0.22 | *
    |      22K |        - |      352 |      352 |       0.22 | *
    |      23K |        - |      351 |      351 |       0.22 | *
    |      26K |        - |      350 |      350 |       0.23 | *
    |      26K |        - |      349 |      349 |       0.23 | *
    |      26K |        - |      348 |      348 |       0.23 | *
    |      26K |        - |      347 |      347 |       0.23 | *
    |      32K |        - |      344 |      344 |       0.23 | *
    |      32K |        - |      343 |      343 |       0.24 | *
    |      33K |        - |      341 |      341 |       0.24 | *
    |      33K |        - |      340 |      340 |       0.24 | *
    |      34K |        - |      339 |      339 |       0.24 | *
    |      38K |        - |      338 |      338 |       0.25 | *
    |      38K |        - |      337 |      337 |       0.25 | *
    |      39K |        - |      336 |      336 |       0.25 | *
    |      40K |        - |      335 |      335 |       0.25 | *
    |      40K |        - |      334 |      334 |       0.25 | *
    |      40K |        - |      332 |      332 |       0.26 | *
    |      45K |        - |      330 |      330 |       0.26 | *
    |      49K |        - |      329 |      329 |       0.27 | *
    |      50K |        - |      327 |      327 |       0.28 | *
    |      53K |        - |      325 |      325 |       0.28 | *
    |      54K |        - |      324 |      324 |       0.28 | *
    |      54K |        - |      323 |      323 |       0.29 | *
    |      54K |        - |      322 |      322 |       0.29 | *
    |      61K |        - |      321 |      321 |       0.29 | *
    |      62K |        - |      318 |      318 |       0.30 | *
    |      62K |        - |      317 |      317 |       0.30 | *
    |      66K |        - |      315 |      315 |       0.30 | *
    |      70K |        - |      314 |      314 |       0.31 | *
    |      70K |        - |      312 |      312 |       0.31 | *
    |      70K |        - |      310 |      310 |       0.31 | *
    |      72K |        - |      309 |      309 |       0.31 | *
    |      72K |        - |      308 |      308 |       0.31 | *
    |      72K |        - |      307 |      307 |       0.31 | *
    |      72K |        - |      305 |      305 |       0.31 | *
    |      73K |        - |      304 |      304 |       0.31 | *
    |      74K |        - |      303 |      303 |       0.31 | *
    |      77K |        - |      302 |      302 |       0.32 | *
    |      80K |        - |      300 |      300 |       0.32 | *
    |      83K |        - |      299 |      299 |       0.32 | *
    |      86K |        - |      298 |      298 |       0.33 | *
    |      86K |        - |      297 |      297 |       0.33 | *
    |      86K |        - |      294 |      294 |       0.33 | *
    |      86K |        - |      293 |      293 |       0.33 | *
    |      86K |        - |      292 |      292 |       0.33 | *
    |      87K |        - |      291 |      291 |       0.33 | *
    |      89K |        - |      289 |      289 |       0.33 | *
    |      89K |        - |      288 |      288 |       0.33 | *
    |      89K |        - |      287 |      287 |       0.33 | *
    |      90K |        - |      286 |      286 |       0.34 | *
    |      90K |        - |      285 |      285 |       0.34 | *
    |      91K |        - |      284 |      284 |       0.34 | *
    |      91K |        - |      283 |      283 |       0.34 | *
    |      91K |        - |      282 |      282 |       0.34 | *
    |      96K |        - |      281 |      281 |       0.35 | *
    |      96K |        - |      280 |      280 |       0.35 | *
    |      96K |        - |      277 |      277 |       0.35 | *
    |      96K |        - |      276 |      276 |       0.35 | *
    |      97K |        - |      275 |      275 |       0.35 | *
    |     138K |        - |      274 |      274 |       0.50 | *
    |     138K |        - |      273 |      273 |       0.50 | *
    |     140K |        - |      272 |      272 |       0.51 | *
    |     140K |        - |      271 |      271 |       0.51 | *
    |     143K |        - |      270 |      270 |       0.52 | *
    |     143K |        - |      269 |      269 |       0.53 | *
    |     145K |        - |      268 |      268 |       0.53 | *
    |     149K |        - |      267 |      267 |       0.55 | *
    |     159K |        - |      266 |      266 |       0.59 | *
    |     176K |        - |      264 |      264 |       0.66 | *
    |     176K |        - |      263 |      263 |       0.67 | *
    |     177K |        - |      262 |      262 |       0.67 | *
    |     177K |        - |      261 |      261 |       0.67 | *
    |     178K |        - |      260 |      260 |       0.67 | *
    |     249K |        - |      258 |      258 |       0.94 | *
    |     249K |        - |      257 |      257 |       0.94 | *
    |     302K |        - |      256 |      256 |       1.05 | *
    |     320K |        - |      255 |      255 |       1.08 | *
    |     321K |        - |      254 |      254 |       1.08 | *
    |     325K |        - |      253 |      253 |       1.08 | *
    |     333K |        - |      252 |      252 |       1.09 | *
    |     334K |        - |      251 |      251 |       1.10 | *
    |     335K |        - |      250 |      250 |       1.10 | *
    |     338K |        - |      249 |      249 |       1.10 | *
    |     349K |        - |      248 |      248 |       1.12 | *
    |     349K |        - |      247 |      247 |       1.12 | *
    |     439K |        - |      246 |      246 |       1.27 | *
    |     448K |        - |      245 |      245 |       1.29 | *
    |     448K |        - |      244 |      244 |       1.29 | *
    |     449K |        - |      241 |      241 |       1.29 | *
    |     475K |        - |      240 |      240 |       1.34 | *
    |     529K |        - |      239 |      239 |       1.45 | *
    |     617K |        - |      237 |      237 |       1.59 | *
    |     682K |        - |      236 |      236 |       1.66 | *
    |     686K |        - |      235 |      235 |       1.66 | *
    |     686K |        - |      234 |      234 |       1.66 | *
    |     686K |        - |      233 |      233 |       1.66 | *
    |     704K |        - |      232 |      232 |       1.68 | *
    |     704K |        - |      231 |      231 |       1.68 | *
    |     711K |        - |      230 |      230 |       1.69 | *
    |     731K |        - |      229 |      229 |       1.71 | *
    |     832K |        - |      228 |      228 |       1.80 | *
    |     838K |        - |      227 |      227 |       1.81 | *
    |     839K |        - |      226 |      226 |       1.81 | *
    |     843K |        - |      225 |      225 |       1.81 | *
    |     860K |        - |      224 |      224 |       1.83 | *
    |     867K |        - |      222 |      222 |       1.83 | *
    |     890K |        - |      221 |      221 |       1.86 | *
    |     920K |        - |      220 |      220 |       1.89 | *
    |     931K |        - |      219 |      219 |       1.90 | *
    |     977K |        - |      218 |      218 |       1.94 | *
    |    1337K |        - |      216 |      216 |       2.21 | *
    |    1648K |        - |      215 |      215 |       2.44 | *
    |    1972K |        - |      214 |      214 |       2.69 | *
    |    1973K |        - |      213 |      213 |       2.69 | *
    |    3400K |        - |      212 |      212 |       4.23 | *
    |    4024K |        - |      211 |      211 |       4.69 | *
    |    5218K |        - |      210 |      210 |       5.58 | *
    |    5365K |        - |      209 |      209 |       5.69 | *
    |    5701K |        - |      208 |      208 |       6.03 | *
    |    5773K |        - |      207 |      207 |       6.08 | *
    |    8515K |        - |      206 |      206 |       8.09 | *
    |    8933K |        - |      205 |      205 |       8.40 | *
    |    8935K |        - |      204 |      204 |       8.41 | *
    |    9357K |        - |      203 |      203 |       8.71 | *
    |    9420K |        - |      202 |      202 |       8.76 | *
    |    9420K |        - |      200 |      200 |       8.76 | *
    |      10M |        - |      199 |      199 |       9.41 | *
    |      11M |        - |      198 |      198 |       9.57 | *
    |      11M |        - |      197 |      197 |       9.97 | *
    |      13M |        - |      196 |      196 |      11.42 | *
    |      13M |        - |      195 |      195 |      11.42 | *
    |      18M |        - |      194 |      194 |      15.16 | *
    |      20M |        - |      193 |      193 |      16.52 | *
    |      21M |        - |      192 |      192 |      17.62 | *
    |      22M |        - |      191 |      191 |      17.96 | *
    |      22M |        - |      190 |      190 |      18.25 | *
    |      42M |        - |      189 |      189 |      33.21 | *
    |      43M |        - |      188 |      188 |      33.68 | *
    |      53M |        - |      187 |      187 |      41.10 | *
    |      53M |        - |      186 |      186 |      41.14 | *
    |      53M |        - |      185 |      185 |      41.30 | *
    |     118M |        - |      184 |      184 |      89.15 | *
    |     119M |        - |      183 |      183 |      90.29 | *
    |     121M |        - |      182 |      182 |      91.68 | *
    |     219M |        - |      181 |      181 |     164.11 | *
    |     219M |        - |      180 |      180 |     164.14 | *
    |     219M |        - |      179 |      179 |     164.21 | *
    |    1020M |        - |      178 |      178 |     755.99 | *
    |    1058M |        - |      177 |      177 |     783.98 | *
    |    1125M |        - |      176 |      176 |     833.99 | *
    |    1161M |        - |      175 |      175 |     860.71 | *
    |    1172M |        - |      174 |      174 |     869.05 | *
    |    1493M |        - |      173 |      173 |    1106.60 | *
    \--------------------------------------------------------/

Neighborhoods statistics (values in %):

    /----------------------------------------------------------------\
    | Move               | Improvs. | Sideways |  Accepts |  Rejects |
    |--------------------|----------|----------|----------|----------|
    | Shift(mk)          |     7590 |      16K |      26K |     207M |
    | Shift              |      551 |     464K |     471K |     206M |
    | ShiftSmart(mk)     |      85K |     211K |     323K |     206M |
    | ShiftSmart         |     5670 |    4976K |    5043K |     202M |
    | SimpSwap(mk)       |       36 |       55 |       96 |     207M |
    | SimpSwap           |        1 |      280 |      299 |     207M |
    | SimpSwapSmart(mk)  |      277 |      836 |     1226 |     207M |
    | SimpSwapSmart      |       52 |     3301 |     3628 |     207M |
    | Swap(mk)           |        9 |        3 |       14 |     207M |
    | Swap               |        0 |       38 |       41 |     207M |
    | SwapSmart(mk)      |      340 |      717 |     1162 |     207M |
    | SwapSmart          |       47 |     3104 |     3395 |     207M |
    | Switch(mk)         |     1312 |      17M |      17M |     190M |
    | Switch             |       94 |      16M |      16M |     191M |
    | SwitchSmart(mk)    |      13K |      29K |      45K |     207M |
    | SwitchSmart        |      887 |    1952K |    1964K |     205M |
    | TaskMove(mk)       |      446 |      760 |     1382 |     207M |
    | TaskMove           |       25 |     2839 |     3141 |     207M |
    | TaskMoveSmart(mk)  |     5327 |     9451 |      17K |     207M |
    | TaskMoveSmart      |      489 |      34K |      38K |     207M |
    | 2-Shift(mk)        |     1541 |    1500K |    1501K |     205M |
    | 2-Shift            |       95 |    1491K |    1493K |     205M |
    | 2-ShiftSmart(mk)   |      15K |    2758K |    2776K |     204M |
    | 2-ShiftSmart       |     1026 |    3129K |    3140K |     204M |
    \----------------------------------------------------------------/

Best makespan.....: 173
N. of Iterations..: 4961587736
Total runtime.....: 3600.01s
Using Python-MIP package version 1.6.8
Começo a execução
    /-------------------------------------------------------------------------------\
    |    Makespan   |   Free Jobs   | Free Machines |    It. Time   |   Total Time  | 
    |-------------------------------------------------------------------------------|
    \-------------------------------------------------------------------------------/
Terminou a execução
