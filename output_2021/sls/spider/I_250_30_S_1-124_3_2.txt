Instance....: instances/I_250_30_S_1-124_3.txt
Algorithm...: Simulated Annealing (alpha=0.990, saMax=10M, t0=1)
Other params: maxIters=100M, seed=-1, timeLimit=3600.00s

    /--------------------------------------------------------\
    |     Iter |   RDP(%) |       S* |       S' |       Time | 
    |----------|----------|----------|----------|------------|
    |        0 |        - |     1616 |     1616 |       0.00 | s0
    |        3 |        - |     1423 |     1423 |       0.00 | *
    |       10 |        - |     1395 |     1395 |       0.02 | *
    |       17 |        - |     1380 |     1380 |       0.02 | *
    |       20 |        - |     1363 |     1363 |       0.02 | *
    |       28 |        - |     1320 |     1320 |       0.02 | *
    |       29 |        - |     1297 |     1297 |       0.03 | *
    |       41 |        - |     1294 |     1294 |       0.03 | *
    |       42 |        - |     1291 |     1291 |       0.03 | *
    |       43 |        - |     1290 |     1290 |       0.03 | *
    |       48 |        - |     1280 |     1280 |       0.03 | *
    |       49 |        - |     1225 |     1225 |       0.03 | *
    |       50 |        - |     1208 |     1208 |       0.03 | *
    |       51 |        - |     1202 |     1202 |       0.03 | *
    |       53 |        - |     1199 |     1199 |       0.03 | *
    |       54 |        - |     1156 |     1156 |       0.03 | *
    |       55 |        - |     1139 |     1139 |       0.03 | *
    |       57 |        - |     1119 |     1119 |       0.03 | *
    |       58 |        - |     1101 |     1101 |       0.04 | *
    |       59 |        - |     1100 |     1100 |       0.04 | *
    |       66 |        - |     1088 |     1088 |       0.04 | *
    |       70 |        - |     1082 |     1082 |       0.04 | *
    |       72 |        - |     1079 |     1079 |       0.04 | *
    |       73 |        - |     1067 |     1067 |       0.04 | *
    |       75 |        - |     1057 |     1057 |       0.04 | *
    |       76 |        - |     1052 |     1052 |       0.04 | *
    |       78 |        - |     1048 |     1048 |       0.04 | *
    |       97 |        - |     1042 |     1042 |       0.04 | *
    |       98 |        - |     1033 |     1033 |       0.04 | *
    |       99 |        - |     1011 |     1011 |       0.04 | *
    |      106 |        - |     1001 |     1001 |       0.04 | *
    |      107 |        - |      997 |      997 |       0.04 | *
    |      110 |        - |      988 |      988 |       0.04 | *
    |      113 |        - |      982 |      982 |       0.04 | *
    |      115 |        - |      976 |      976 |       0.05 | *
    |      122 |        - |      971 |      971 |       0.05 | *
    |      131 |        - |      962 |      962 |       0.05 | *
    |      139 |        - |      946 |      946 |       0.05 | *
    |      141 |        - |      937 |      937 |       0.05 | *
    |      143 |        - |      934 |      934 |       0.05 | *
    |      145 |        - |      919 |      919 |       0.05 | *
    |      151 |        - |      915 |      915 |       0.05 | *
    |      152 |        - |      913 |      913 |       0.05 | *
    |      158 |        - |      912 |      912 |       0.05 | *
    |      164 |        - |      905 |      905 |       0.05 | *
    |      165 |        - |      903 |      903 |       0.05 | *
    |      166 |        - |      902 |      902 |       0.05 | *
    |      169 |        - |      901 |      901 |       0.05 | *
    |      172 |        - |      899 |      899 |       0.05 | *
    |      176 |        - |      896 |      896 |       0.05 | *
    |      177 |        - |      885 |      885 |       0.06 | *
    |      185 |        - |      877 |      877 |       0.06 | *
    |      187 |        - |      874 |      874 |       0.06 | *
    |      208 |        - |      867 |      867 |       0.06 | *
    |      210 |        - |      861 |      861 |       0.06 | *
    |      216 |        - |      857 |      857 |       0.06 | *
    |      227 |        - |      856 |      856 |       0.06 | *
    |      239 |        - |      855 |      855 |       0.06 | *
    |      244 |        - |      854 |      854 |       0.06 | *
    |      253 |        - |      846 |      846 |       0.06 | *
    |      255 |        - |      840 |      840 |       0.06 | *
    |      266 |        - |      831 |      831 |       0.06 | *
    |      268 |        - |      829 |      829 |       0.06 | *
    |      271 |        - |      828 |      828 |       0.06 | *
    |      278 |        - |      827 |      827 |       0.06 | *
    |      283 |        - |      826 |      826 |       0.07 | *
    |      286 |        - |      825 |      825 |       0.07 | *
    |      289 |        - |      812 |      812 |       0.07 | *
    |      290 |        - |      810 |      810 |       0.07 | *
    |      304 |        - |      809 |      809 |       0.07 | *
    |      307 |        - |      806 |      806 |       0.07 | *
    |      310 |        - |      804 |      804 |       0.07 | *
    |      323 |        - |      799 |      799 |       0.07 | *
    |      327 |        - |      797 |      797 |       0.07 | *
    |      334 |        - |      796 |      796 |       0.07 | *
    |      335 |        - |      795 |      795 |       0.07 | *
    |      339 |        - |      789 |      789 |       0.07 | *
    |      340 |        - |      785 |      785 |       0.07 | *
    |      342 |        - |      784 |      784 |       0.07 | *
    |      343 |        - |      779 |      779 |       0.07 | *
    |      350 |        - |      778 |      778 |       0.07 | *
    |      360 |        - |      777 |      777 |       0.07 | *
    |      366 |        - |      776 |      776 |       0.07 | *
    |      375 |        - |      766 |      766 |       0.07 | *
    |      389 |        - |      765 |      765 |       0.08 | *
    |      390 |        - |      762 |      762 |       0.08 | *
    |      393 |        - |      761 |      761 |       0.08 | *
    |      395 |        - |      760 |      760 |       0.08 | *
    |      404 |        - |      755 |      755 |       0.08 | *
    |      406 |        - |      751 |      751 |       0.08 | *
    |      408 |        - |      745 |      745 |       0.08 | *
    |      411 |        - |      742 |      742 |       0.08 | *
    |      413 |        - |      737 |      737 |       0.08 | *
    |      424 |        - |      736 |      736 |       0.08 | *
    |      426 |        - |      733 |      733 |       0.08 | *
    |      427 |        - |      732 |      732 |       0.08 | *
    |      428 |        - |      725 |      725 |       0.08 | *
    |      444 |        - |      724 |      724 |       0.08 | *
    |      454 |        - |      716 |      716 |       0.08 | *
    |      458 |        - |      711 |      711 |       0.08 | *
    |      465 |        - |      709 |      709 |       0.08 | *
    |      467 |        - |      708 |      708 |       0.08 | *
    |      476 |        - |      702 |      702 |       0.09 | *
    |      493 |        - |      697 |      697 |       0.09 | *
    |      497 |        - |      695 |      695 |       0.09 | *
    |      501 |        - |      693 |      693 |       0.09 | *
    |      519 |        - |      686 |      686 |       0.09 | *
    |      523 |        - |      685 |      685 |       0.09 | *
    |      541 |        - |      678 |      678 |       0.09 | *
    |      543 |        - |      676 |      676 |       0.09 | *
    |      550 |        - |      672 |      672 |       0.09 | *
    |      586 |        - |      671 |      671 |       0.09 | *
    |      589 |        - |      666 |      666 |       0.09 | *
    |      592 |        - |      665 |      665 |       0.09 | *
    |      594 |        - |      654 |      654 |       0.09 | *
    |      595 |        - |      653 |      653 |       0.09 | *
    |      604 |        - |      651 |      651 |       0.09 | *
    |      607 |        - |      650 |      650 |       0.09 | *
    |      613 |        - |      643 |      643 |       0.09 | *
    |      623 |        - |      642 |      642 |       0.09 | *
    |      625 |        - |      640 |      640 |       0.09 | *
    |      637 |        - |      634 |      634 |       0.10 | *
    |      675 |        - |      632 |      632 |       0.10 | *
    |      727 |        - |      631 |      631 |       0.10 | *
    |      729 |        - |      630 |      630 |       0.10 | *
    |      756 |        - |      629 |      629 |       0.10 | *
    |      775 |        - |      628 |      628 |       0.10 | *
    |      793 |        - |      627 |      627 |       0.10 | *
    |      808 |        - |      624 |      624 |       0.10 | *
    |      857 |        - |      623 |      623 |       0.10 | *
    |      863 |        - |      621 |      621 |       0.10 | *
    |      875 |        - |      619 |      619 |       0.10 | *
    |      876 |        - |      617 |      617 |       0.10 | *
    |      879 |        - |      614 |      614 |       0.10 | *
    |      891 |        - |      611 |      611 |       0.10 | *
    |      905 |        - |      610 |      610 |       0.10 | *
    |      912 |        - |      607 |      607 |       0.10 | *
    |      923 |        - |      606 |      606 |       0.11 | *
    |      924 |        - |      605 |      605 |       0.11 | *
    |      937 |        - |      604 |      604 |       0.11 | *
    |      945 |        - |      603 |      603 |       0.11 | *
    |      948 |        - |      602 |      602 |       0.11 | *
    |      955 |        - |      595 |      595 |       0.11 | *
    |      972 |        - |      594 |      594 |       0.11 | *
    |      982 |        - |      593 |      593 |       0.11 | *
    |     1009 |        - |      591 |      591 |       0.11 | *
    |     1015 |        - |      582 |      582 |       0.11 | *
    |     1057 |        - |      581 |      581 |       0.11 | *
    |     1070 |        - |      580 |      580 |       0.11 | *
    |     1096 |        - |      579 |      579 |       0.11 | *
    |     1166 |        - |      577 |      577 |       0.11 | *
    |     1169 |        - |      575 |      575 |       0.12 | *
    |     1170 |        - |      574 |      574 |       0.12 | *
    |     1187 |        - |      571 |      571 |       0.12 | *
    |     1209 |        - |      570 |      570 |       0.12 | *
    |     1211 |        - |      569 |      569 |       0.12 | *
    |     1257 |        - |      567 |      567 |       0.12 | *
    |     1276 |        - |      566 |      566 |       0.12 | *
    |     1284 |        - |      563 |      563 |       0.12 | *
    |     1291 |        - |      562 |      562 |       0.12 | *
    |     1325 |        - |      561 |      561 |       0.12 | *
    |     1340 |        - |      557 |      557 |       0.12 | *
    |     1379 |        - |      556 |      556 |       0.12 | *
    |     1436 |        - |      555 |      555 |       0.13 | *
    |     1450 |        - |      554 |      554 |       0.13 | *
    |     1513 |        - |      551 |      551 |       0.13 | *
    |     1521 |        - |      550 |      550 |       0.13 | *
    |     1540 |        - |      549 |      549 |       0.13 | *
    |     1548 |        - |      548 |      548 |       0.13 | *
    |     1620 |        - |      547 |      547 |       0.13 | *
    |     1644 |        - |      545 |      545 |       0.13 | *
    |     1649 |        - |      542 |      542 |       0.13 | *
    |     1688 |        - |      538 |      538 |       0.13 | *
    |     1705 |        - |      537 |      537 |       0.13 | *
    |     1718 |        - |      536 |      536 |       0.13 | *
    |     1723 |        - |      534 |      534 |       0.14 | *
    |     1775 |        - |      532 |      532 |       0.14 | *
    |     1801 |        - |      530 |      530 |       0.14 | *
    |     1821 |        - |      529 |      529 |       0.14 | *
    |     1839 |        - |      526 |      526 |       0.14 | *
    |     1849 |        - |      524 |      524 |       0.14 | *
    |     1891 |        - |      522 |      522 |       0.14 | *
    |     1920 |        - |      520 |      520 |       0.14 | *
    |     1923 |        - |      518 |      518 |       0.14 | *
    |     1970 |        - |      516 |      516 |       0.14 | *
    |     1971 |        - |      512 |      512 |       0.14 | *
    |     1974 |        - |      511 |      511 |       0.14 | *
    |     2029 |        - |      510 |      510 |       0.14 | *
    |     2058 |        - |      508 |      508 |       0.14 | *
    |     2082 |        - |      506 |      506 |       0.15 | *
    |     2104 |        - |      504 |      504 |       0.15 | *
    |     2109 |        - |      503 |      503 |       0.15 | *
    |     2270 |        - |      502 |      502 |       0.15 | *
    |     2290 |        - |      501 |      501 |       0.15 | *
    |     2303 |        - |      500 |      500 |       0.15 | *
    |     2307 |        - |      498 |      498 |       0.15 | *
    |     2370 |        - |      496 |      496 |       0.15 | *
    |     2393 |        - |      493 |      493 |       0.15 | *
    |     2410 |        - |      491 |      491 |       0.15 | *
    |     2454 |        - |      489 |      489 |       0.15 | *
    |     2479 |        - |      486 |      486 |       0.15 | *
    |     2541 |        - |      485 |      485 |       0.15 | *
    |     2595 |        - |      484 |      484 |       0.16 | *
    |     2619 |        - |      483 |      483 |       0.16 | *
    |     2638 |        - |      481 |      481 |       0.16 | *
    |     2648 |        - |      480 |      480 |       0.16 | *
    |     2667 |        - |      478 |      478 |       0.16 | *
    |     2796 |        - |      476 |      476 |       0.16 | *
    |     2808 |        - |      474 |      474 |       0.16 | *
    |     2813 |        - |      473 |      473 |       0.16 | *
    |     2814 |        - |      472 |      472 |       0.16 | *
    |     2847 |        - |      467 |      467 |       0.16 | *
    |     2852 |        - |      465 |      465 |       0.16 | *
    |     2864 |        - |      464 |      464 |       0.16 | *
    |     2891 |        - |      463 |      463 |       0.16 | *
    |     2892 |        - |      457 |      457 |       0.16 | *
    |     2964 |        - |      456 |      456 |       0.16 | *
    |     2996 |        - |      455 |      455 |       0.17 | *
    |     3018 |        - |      454 |      454 |       0.17 | *
    |     3021 |        - |      453 |      453 |       0.17 | *
    |     3040 |        - |      452 |      452 |       0.17 | *
    |     3053 |        - |      451 |      451 |       0.17 | *
    |     3229 |        - |      448 |      448 |       0.17 | *
    |     3263 |        - |      444 |      444 |       0.17 | *
    |     3337 |        - |      443 |      443 |       0.17 | *
    |     3459 |        - |      442 |      442 |       0.17 | *
    |     3501 |        - |      440 |      440 |       0.17 | *
    |     3504 |        - |      438 |      438 |       0.17 | *
    |     3549 |        - |      437 |      437 |       0.17 | *
    |     3650 |        - |      435 |      435 |       0.18 | *
    |     3854 |        - |      432 |      432 |       0.18 | *
    |     3867 |        - |      426 |      426 |       0.18 | *
    |     3904 |        - |      423 |      423 |       0.18 | *
    |     4013 |        - |      422 |      422 |       0.18 | *
    |     4143 |        - |      421 |      421 |       0.18 | *
    |     4145 |        - |      420 |      420 |       0.18 | *
    |     4146 |        - |      419 |      419 |       0.18 | *
    |     4339 |        - |      418 |      418 |       0.18 | *
    |     4462 |        - |      415 |      415 |       0.18 | *
    |     4593 |        - |      414 |      414 |       0.18 | *
    |     4778 |        - |      413 |      413 |       0.18 | *
    |     4806 |        - |      412 |      412 |       0.19 | *
    |     4814 |        - |      411 |      411 |       0.19 | *
    |     4816 |        - |      409 |      409 |       0.19 | *
    |     4852 |        - |      408 |      408 |       0.19 | *
    |     4885 |        - |      406 |      406 |       0.19 | *
    |     4951 |        - |      405 |      405 |       0.19 | *
    |     4952 |        - |      404 |      404 |       0.19 | *
    |     4990 |        - |      403 |      403 |       0.19 | *
    |     5561 |        - |      398 |      398 |       0.19 | *
    |     5638 |        - |      397 |      397 |       0.19 | *
    |     5651 |        - |      395 |      395 |       0.19 | *
    |     5967 |        - |      394 |      394 |       0.20 | *
    |     6043 |        - |      393 |      393 |       0.20 | *
    |     6093 |        - |      392 |      392 |       0.20 | *
    |     6163 |        - |      390 |      390 |       0.20 | *
    |     6177 |        - |      389 |      389 |       0.20 | *
    |     6309 |        - |      388 |      388 |       0.20 | *
    |     6349 |        - |      387 |      387 |       0.20 | *
    |     6355 |        - |      386 |      386 |       0.20 | *
    |     6389 |        - |      385 |      385 |       0.20 | *
    |     6836 |        - |      383 |      383 |       0.20 | *
    |     6954 |        - |      382 |      382 |       0.20 | *
    |     6964 |        - |      381 |      381 |       0.21 | *
    |     6978 |        - |      380 |      380 |       0.21 | *
    |     7234 |        - |      379 |      379 |       0.21 | *
    |     7243 |        - |      378 |      378 |       0.21 | *
    |     7319 |        - |      377 |      377 |       0.21 | *
    |     7826 |        - |      375 |      375 |       0.21 | *
    |     7952 |        - |      374 |      374 |       0.21 | *
    |     7962 |        - |      373 |      373 |       0.21 | *
    |     7992 |        - |      372 |      372 |       0.21 | *
    |     8036 |        - |      371 |      371 |       0.21 | *
    |     8081 |        - |      367 |      367 |       0.21 | *
    |     8162 |        - |      365 |      365 |       0.21 | *
    |     8244 |        - |      362 |      362 |       0.22 | *
    |     8295 |        - |      361 |      361 |       0.22 | *
    |     8362 |        - |      359 |      359 |       0.22 | *
    |     8446 |        - |      358 |      358 |       0.22 | *
    |     8570 |        - |      357 |      357 |       0.22 | *
    |     8592 |        - |      355 |      355 |       0.22 | *
    |     8693 |        - |      353 |      353 |       0.22 | *
    |     8753 |        - |      352 |      352 |       0.22 | *
    |     8963 |        - |      350 |      350 |       0.22 | *
    |     8996 |        - |      349 |      349 |       0.22 | *
    |     9179 |        - |      348 |      348 |       0.22 | *
    |     9264 |        - |      347 |      347 |       0.22 | *
    |     9299 |        - |      345 |      345 |       0.22 | *
    |     9332 |        - |      344 |      344 |       0.22 | *
    |     9476 |        - |      341 |      341 |       0.23 | *
    |     9738 |        - |      339 |      339 |       0.23 | *
    |     9794 |        - |      338 |      338 |       0.23 | *
    |      11K |        - |      337 |      337 |       0.23 | *
    |      11K |        - |      336 |      336 |       0.23 | *
    |      11K |        - |      334 |      334 |       0.23 | *
    |      11K |        - |      332 |      332 |       0.23 | *
    |      11K |        - |      331 |      331 |       0.23 | *
    |      11K |        - |      330 |      330 |       0.23 | *
    |      11K |        - |      329 |      329 |       0.23 | *
    |      11K |        - |      328 |      328 |       0.24 | *
    |      11K |        - |      327 |      327 |       0.24 | *
    |      11K |        - |      326 |      326 |       0.24 | *
    |      13K |        - |      325 |      325 |       0.24 | *
    |      14K |        - |      322 |      322 |       0.24 | *
    |      14K |        - |      321 |      321 |       0.24 | *
    |      14K |        - |      320 |      320 |       0.25 | *
    |      14K |        - |      319 |      319 |       0.25 | *
    |      14K |        - |      318 |      318 |       0.25 | *
    |      15K |        - |      317 |      317 |       0.25 | *
    |      15K |        - |      316 |      316 |       0.25 | *
    |      15K |        - |      315 |      315 |       0.25 | *
    |      15K |        - |      314 |      314 |       0.25 | *
    |      16K |        - |      313 |      313 |       0.26 | *
    |      17K |        - |      312 |      312 |       0.26 | *
    |      17K |        - |      310 |      310 |       0.26 | *
    |      18K |        - |      309 |      309 |       0.26 | *
    |      18K |        - |      308 |      308 |       0.26 | *
    |      18K |        - |      307 |      307 |       0.26 | *
    |      18K |        - |      305 |      305 |       0.26 | *
    |      18K |        - |      303 |      303 |       0.26 | *
    |      18K |        - |      302 |      302 |       0.27 | *
    |      20K |        - |      301 |      301 |       0.27 | *
    |      20K |        - |      300 |      300 |       0.27 | *
    |      20K |        - |      299 |      299 |       0.27 | *
    |      20K |        - |      296 |      296 |       0.27 | *
    |      21K |        - |      295 |      295 |       0.28 | *
    |      21K |        - |      294 |      294 |       0.28 | *
    |      22K |        - |      293 |      293 |       0.28 | *
    |      27K |        - |      290 |      290 |       0.29 | *
    |      27K |        - |      289 |      289 |       0.29 | *
    |      27K |        - |      288 |      288 |       0.29 | *
    |      27K |        - |      287 |      287 |       0.29 | *
    |      27K |        - |      285 |      285 |       0.30 | *
    |      28K |        - |      283 |      283 |       0.30 | *
    |      29K |        - |      281 |      281 |       0.30 | *
    |      29K |        - |      280 |      280 |       0.30 | *
    |      29K |        - |      279 |      279 |       0.30 | *
    |      31K |        - |      278 |      278 |       0.31 | *
    |      32K |        - |      277 |      277 |       0.31 | *
    |      34K |        - |      276 |      276 |       0.32 | *
    |      34K |        - |      275 |      275 |       0.32 | *
    |      36K |        - |      273 |      273 |       0.32 | *
    |      36K |        - |      272 |      272 |       0.33 | *
    |      38K |        - |      271 |      271 |       0.33 | *
    |      38K |        - |      270 |      270 |       0.33 | *
    |      39K |        - |      269 |      269 |       0.33 | *
    |      39K |        - |      268 |      268 |       0.33 | *
    |      40K |        - |      267 |      267 |       0.34 | *
    |      40K |        - |      266 |      266 |       0.34 | *
    |      41K |        - |      265 |      265 |       0.34 | *
    |      42K |        - |      264 |      264 |       0.34 | *
    |      43K |        - |      263 |      263 |       0.35 | *
    |      43K |        - |      262 |      262 |       0.35 | *
    |      44K |        - |      261 |      261 |       0.35 | *
    |      44K |        - |      260 |      260 |       0.35 | *
    |      45K |        - |      259 |      259 |       0.35 | *
    |      46K |        - |      258 |      258 |       0.36 | *
    |      48K |        - |      257 |      257 |       0.36 | *
    |      49K |        - |      256 |      256 |       0.37 | *
    |      49K |        - |      255 |      255 |       0.37 | *
    |      51K |        - |      252 |      252 |       0.37 | *
    |      52K |        - |      251 |      251 |       0.37 | *
    |      52K |        - |      247 |      247 |       0.38 | *
    |      52K |        - |      246 |      246 |       0.38 | *
    |      52K |        - |      245 |      245 |       0.38 | *
    |      54K |        - |      244 |      244 |       0.38 | *
    |      54K |        - |      243 |      243 |       0.38 | *
    |      55K |        - |      242 |      242 |       0.39 | *
    |      55K |        - |      240 |      240 |       0.39 | *
    |      55K |        - |      239 |      239 |       0.39 | *
    |      57K |        - |      238 |      238 |       0.39 | *
    |      59K |        - |      237 |      237 |       0.40 | *
    |      59K |        - |      236 |      236 |       0.40 | *
    |      62K |        - |      235 |      235 |       0.41 | *
    |      63K |        - |      234 |      234 |       0.41 | *
    |      63K |        - |      233 |      233 |       0.41 | *
    |      63K |        - |      232 |      232 |       0.41 | *
    |      63K |        - |      231 |      231 |       0.41 | *
    |      64K |        - |      230 |      230 |       0.41 | *
    |      64K |        - |      229 |      229 |       0.41 | *
    |      65K |        - |      228 |      228 |       0.41 | *
    |      65K |        - |      227 |      227 |       0.42 | *
    |      72K |        - |      226 |      226 |       0.43 | *
    |      79K |        - |      225 |      225 |       0.44 | *
    |      84K |        - |      224 |      224 |       0.45 | *
    |      86K |        - |      223 |      223 |       0.45 | *
    |      94K |        - |      222 |      222 |       0.46 | *
    |     100K |        - |      220 |      220 |       0.46 | *
    |     100K |        - |      217 |      217 |       0.46 | *
    |     106K |        - |      216 |      216 |       0.47 | *
    |     108K |        - |      215 |      215 |       0.47 | *
    |     108K |        - |      214 |      214 |       0.47 | *
    |     108K |        - |      213 |      213 |       0.47 | *
    |     109K |        - |      212 |      212 |       0.47 | *
    |     114K |        - |      211 |      211 |       0.48 | *
    |     114K |        - |      210 |      210 |       0.48 | *
    |     117K |        - |      209 |      209 |       0.49 | *
    |     120K |        - |      208 |      208 |       0.49 | *
    |     123K |        - |      207 |      207 |       0.50 | *
    |     124K |        - |      206 |      206 |       0.50 | *
    |     130K |        - |      205 |      205 |       0.51 | *
    |     130K |        - |      204 |      204 |       0.51 | *
    |     132K |        - |      203 |      203 |       0.51 | *
    |     132K |        - |      202 |      202 |       0.51 | *
    |     139K |        - |      201 |      201 |       0.52 | *
    |     144K |        - |      200 |      200 |       0.53 | *
    |     144K |        - |      199 |      199 |       0.53 | *
    |     145K |        - |      198 |      198 |       0.54 | *
    |     152K |        - |      197 |      197 |       0.55 | *
    |     155K |        - |      196 |      196 |       0.55 | *
    |     155K |        - |      195 |      195 |       0.55 | *
    |     156K |        - |      193 |      193 |       0.56 | *
    |     195K |        - |      192 |      192 |       0.66 | *
    |     195K |        - |      191 |      191 |       0.66 | *
    |     201K |        - |      190 |      190 |       0.68 | *
    |     210K |        - |      189 |      189 |       0.71 | *
    |     211K |        - |      188 |      188 |       0.72 | *
    |     213K |        - |      187 |      187 |       0.72 | *
    |     215K |        - |      186 |      186 |       0.73 | *
    |     216K |        - |      185 |      185 |       0.74 | *
    |     216K |        - |      184 |      184 |       0.74 | *
    |     227K |        - |      183 |      183 |       0.78 | *
    |     249K |        - |      182 |      182 |       0.86 | *
    |     250K |        - |      181 |      181 |       0.86 | *
    |     256K |        - |      180 |      180 |       0.88 | *
    |     266K |        - |      179 |      179 |       0.91 | *
    |     266K |        - |      178 |      178 |       0.91 | *
    |     275K |        - |      177 |      177 |       0.95 | *
    |     298K |        - |      176 |      176 |       1.02 | *
    |     312K |        - |      175 |      175 |       1.05 | *
    |     317K |        - |      174 |      174 |       1.07 | *
    |     342K |        - |      173 |      173 |       1.13 | *
    |     348K |        - |      172 |      172 |       1.15 | *
    |     359K |        - |      171 |      171 |       1.17 | *
    |     362K |        - |      170 |      170 |       1.17 | *
    |     426K |        - |      169 |      169 |       1.26 | *
    |     426K |        - |      168 |      168 |       1.26 | *
    |     440K |        - |      167 |      167 |       1.28 | *
    |     440K |        - |      166 |      166 |       1.28 | *
    |     444K |        - |      165 |      165 |       1.28 | *
    |     448K |        - |      164 |      164 |       1.29 | *
    |     475K |        - |      163 |      163 |       1.31 | *
    |     488K |        - |      162 |      162 |       1.33 | *
    |     490K |        - |      161 |      161 |       1.33 | *
    |     530K |        - |      160 |      160 |       1.37 | *
    |     585K |        - |      159 |      159 |       1.42 | *
    |     673K |        - |      158 |      158 |       1.51 | *
    |     675K |        - |      157 |      157 |       1.51 | *
    |     822K |        - |      156 |      156 |       1.67 | *
    |     890K |        - |      155 |      155 |       1.75 | *
    |    1078K |        - |      154 |      154 |       1.91 | *
    |    1117K |        - |      153 |      153 |       1.94 | *
    |    1159K |        - |      152 |      152 |       1.97 | *
    |    1161K |        - |      151 |      151 |       1.98 | *
    |    1161K |        - |      150 |      150 |       1.98 | *
    |    1175K |        - |      149 |      149 |       1.99 | *
    |    1196K |        - |      148 |      148 |       2.00 | *
    |    1196K |        - |      147 |      147 |       2.00 | *
    |    1208K |        - |      146 |      146 |       2.01 | *
    |    1287K |        - |      145 |      145 |       2.07 | *
    |    1288K |        - |      144 |      144 |       2.07 | *
    |    1317K |        - |      143 |      143 |       2.10 | *
    |    1322K |        - |      142 |      142 |       2.10 | *
    |    1322K |        - |      141 |      141 |       2.10 | *
    |    1322K |        - |      140 |      140 |       2.10 | *
    |    1380K |        - |      139 |      139 |       2.15 | *
    |    1417K |        - |      138 |      138 |       2.17 | *
    |    1752K |        - |      137 |      137 |       2.42 | *
    |    1823K |        - |      136 |      136 |       2.48 | *
    |    1824K |        - |      134 |      134 |       2.48 | *
    |    1870K |        - |      133 |      133 |       2.52 | *
    |    1893K |        - |      132 |      132 |       2.54 | *
    |    1968K |        - |      131 |      131 |       2.59 | *
    |    2290K |        - |      130 |      130 |       2.84 | *
    |    3089K |        - |      129 |      129 |       3.51 | *
    |    4220K |        - |      128 |      128 |       4.89 | *
    |    4573K |        - |      127 |      127 |       5.20 | *
    |    7085K |        - |      126 |      126 |       7.13 | *
    |    7158K |        - |      125 |      125 |       7.19 | *
    |    7164K |        - |      124 |      124 |       7.19 | *
    |    7322K |        - |      123 |      123 |       7.32 | *
    |    7367K |        - |      122 |      122 |       7.35 | *
    |    8425K |        - |      121 |      121 |       8.22 | *
    |    8460K |        - |      120 |      120 |       8.27 | *
    |      11M |        - |      119 |      119 |      10.71 | *
    |      11M |        - |      118 |      118 |      10.71 | *
    |      11M |        - |      117 |      117 |      10.78 | *
    |      12M |        - |      116 |      116 |      11.85 | *
    |      13M |        - |      115 |      115 |      12.15 | *
    |      13M |        - |      114 |      114 |      12.62 | *
    |      20M |        - |      113 |      113 |      18.84 | *
    |      24M |        - |      112 |      112 |      23.07 | *
    |      24M |        - |      111 |      111 |      23.10 | *
    |      24M |        - |      110 |      110 |      23.14 | *
    |      24M |        - |      109 |      109 |      23.16 | *
    |      24M |        - |      108 |      108 |      23.16 | *
    |      51M |        - |      107 |      107 |      46.16 | *
    |      51M |        - |      106 |      106 |      46.65 | *
    |      52M |        - |      105 |      105 |      47.05 | *
    |      90M |        - |      104 |      104 |      77.04 | *
    |      91M |        - |      103 |      103 |      77.99 | *
    |     245M |        - |      102 |      102 |     196.04 | *
    |     249M |        - |      101 |      101 |     199.33 | *
    |     258M |        - |      100 |      100 |     206.08 | *
    |     503M |        - |       99 |       99 |     392.58 | *
    |     525M |        - |       98 |       98 |     408.97 | *
    |     701M |        - |       97 |       97 |     541.93 | *
    |     723M |        - |       96 |       96 |     558.82 | *
    |     746M |        - |       95 |       95 |     576.65 | *
    |     764M |        - |       94 |       94 |     590.34 | *
    \--------------------------------------------------------/

Neighborhoods statistics (values in %):

    /----------------------------------------------------------------\
    | Move               | Improvs. | Sideways |  Accepts |  Rejects |
    |--------------------|----------|----------|----------|----------|
    | Shift(mk)          |      13K |      26K |      44K |     203M |
    | Shift              |      447 |     492K |     503K |     203M |
    | ShiftSmart(mk)     |      88K |     187K |     302K |     203M |
    | ShiftSmart         |     2907 |    3167K |    3234K |     200M |
    | SimpSwap(mk)       |       83 |      175 |      287 |     204M |
    | SimpSwap           |        5 |      893 |      971 |     204M |
    | SimpSwapSmart(mk)  |      580 |     1176 |     1972 |     204M |
    | SimpSwapSmart      |       32 |     5623 |     6084 |     204M |
    | Swap(mk)           |       30 |       31 |       64 |     204M |
    | Swap               |        0 |      173 |      186 |     204M |
    | SwapSmart(mk)      |      479 |     1093 |     1746 |     204M |
    | SwapSmart          |       26 |     5188 |     5563 |     204M |
    | Switch(mk)         |     2705 |      24M |      24M |     179M |
    | Switch             |      100 |      26M |      26M |     178M |
    | SwitchSmart(mk)    |      17K |      21K |      43K |     204M |
    | SwitchSmart        |      619 |     333K |     347K |     203M |
    | TaskMove(mk)       |      895 |     1968 |     3180 |     204M |
    | TaskMove           |       36 |      12K |      13K |     204M |
    | TaskMoveSmart(mk)  |     6549 |      14K |      23K |     204M |
    | TaskMoveSmart      |      267 |      96K |     100K |     203M |
    | 2-Shift(mk)        |     4745 |    3388K |    3394K |     200M |
    | 2-Shift            |      178 |    4159K |    4163K |     200M |
    | 2-ShiftSmart(mk)   |      22K |    1146K |    1172K |     203M |
    | 2-ShiftSmart       |      754 |    7244K |    7258K |     196M |
    \----------------------------------------------------------------/

Best makespan.....: 94
N. of Iterations..: 4887800870
Total runtime.....: 3600.01s
Using Python-MIP package version 1.6.8
Começo a execução
    /-------------------------------------------------------------------------------\
    |    Makespan   |   Free Jobs   | Free Machines |    It. Time   |   Total Time  | 
    |-------------------------------------------------------------------------------|
    \-------------------------------------------------------------------------------/
Terminou a execução
