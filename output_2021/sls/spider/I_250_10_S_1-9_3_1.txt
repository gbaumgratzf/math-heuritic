Instance....: instances/I_250_10_S_1-9_3.txt
Algorithm...: Simulated Annealing (alpha=0.990, saMax=10M, t0=1)
Other params: maxIters=100M, seed=-1, timeLimit=3600.00s

    /--------------------------------------------------------\
    |     Iter |   RDP(%) |       S* |       S' |       Time | 
    |----------|----------|----------|----------|------------|
    |        0 |        - |     1763 |     1763 |       0.00 | s0
    |        3 |        - |     1747 |     1747 |       0.01 | *
    |       10 |        - |     1659 |     1659 |       0.02 | *
    |       14 |        - |     1627 |     1627 |       0.03 | *
    |       16 |        - |     1625 |     1625 |       0.03 | *
    |       17 |        - |     1601 |     1601 |       0.03 | *
    |       21 |        - |     1578 |     1578 |       0.03 | *
    |       22 |        - |     1511 |     1511 |       0.03 | *
    |       29 |        - |     1499 |     1499 |       0.03 | *
    |       30 |        - |     1493 |     1493 |       0.03 | *
    |       33 |        - |     1484 |     1484 |       0.03 | *
    |       36 |        - |     1464 |     1464 |       0.04 | *
    |       53 |        - |     1460 |     1460 |       0.04 | *
    |       54 |        - |     1445 |     1445 |       0.04 | *
    |       55 |        - |     1418 |     1418 |       0.04 | *
    |       57 |        - |     1409 |     1409 |       0.04 | *
    |       58 |        - |     1398 |     1398 |       0.04 | *
    |       61 |        - |     1394 |     1394 |       0.04 | *
    |       63 |        - |     1392 |     1392 |       0.04 | *
    |       67 |        - |     1387 |     1387 |       0.04 | *
    |       71 |        - |     1383 |     1383 |       0.04 | *
    |       76 |        - |     1380 |     1380 |       0.04 | *
    |       77 |        - |     1379 |     1379 |       0.04 | *
    |       90 |        - |     1377 |     1377 |       0.04 | *
    |       91 |        - |     1373 |     1373 |       0.04 | *
    |      102 |        - |     1368 |     1368 |       0.05 | *
    |      104 |        - |     1365 |     1365 |       0.05 | *
    |      111 |        - |     1356 |     1356 |       0.05 | *
    |      113 |        - |     1347 |     1347 |       0.05 | *
    |      120 |        - |     1335 |     1335 |       0.05 | *
    |      127 |        - |     1324 |     1324 |       0.05 | *
    |      128 |        - |     1323 |     1323 |       0.05 | *
    |      134 |        - |     1320 |     1320 |       0.05 | *
    |      135 |        - |     1318 |     1318 |       0.05 | *
    |      137 |        - |     1317 |     1317 |       0.05 | *
    |      139 |        - |     1311 |     1311 |       0.05 | *
    |      148 |        - |     1299 |     1299 |       0.05 | *
    |      152 |        - |     1295 |     1295 |       0.05 | *
    |      153 |        - |     1290 |     1290 |       0.05 | *
    |      155 |        - |     1286 |     1286 |       0.05 | *
    |      157 |        - |     1280 |     1280 |       0.06 | *
    |      163 |        - |     1277 |     1277 |       0.06 | *
    |      170 |        - |     1271 |     1271 |       0.06 | *
    |      173 |        - |     1266 |     1266 |       0.06 | *
    |      174 |        - |     1263 |     1263 |       0.06 | *
    |      177 |        - |     1260 |     1260 |       0.06 | *
    |      181 |        - |     1256 |     1256 |       0.06 | *
    |      184 |        - |     1250 |     1250 |       0.06 | *
    |      185 |        - |     1245 |     1245 |       0.06 | *
    |      191 |        - |     1239 |     1239 |       0.06 | *
    |      193 |        - |     1230 |     1230 |       0.06 | *
    |      195 |        - |     1218 |     1218 |       0.06 | *
    |      202 |        - |     1212 |     1212 |       0.06 | *
    |      210 |        - |     1210 |     1210 |       0.06 | *
    |      212 |        - |     1209 |     1209 |       0.06 | *
    |      215 |        - |     1208 |     1208 |       0.07 | *
    |      218 |        - |     1207 |     1207 |       0.07 | *
    |      234 |        - |     1206 |     1206 |       0.07 | *
    |      243 |        - |     1200 |     1200 |       0.07 | *
    |      244 |        - |     1197 |     1197 |       0.07 | *
    |      263 |        - |     1192 |     1192 |       0.07 | *
    |      276 |        - |     1189 |     1189 |       0.07 | *
    |      279 |        - |     1187 |     1187 |       0.07 | *
    |      287 |        - |     1185 |     1185 |       0.07 | *
    |      299 |        - |     1183 |     1183 |       0.07 | *
    |      303 |        - |     1180 |     1180 |       0.07 | *
    |      308 |        - |     1179 |     1179 |       0.07 | *
    |      313 |        - |     1178 |     1178 |       0.07 | *
    |      315 |        - |     1170 |     1170 |       0.07 | *
    |      319 |        - |     1163 |     1163 |       0.07 | *
    |      323 |        - |     1151 |     1151 |       0.07 | *
    |      328 |        - |     1143 |     1143 |       0.08 | *
    |      330 |        - |     1139 |     1139 |       0.08 | *
    |      333 |        - |     1137 |     1137 |       0.08 | *
    |      340 |        - |     1123 |     1123 |       0.08 | *
    |      341 |        - |     1122 |     1122 |       0.08 | *
    |      344 |        - |     1119 |     1119 |       0.08 | *
    |      345 |        - |     1118 |     1118 |       0.08 | *
    |      355 |        - |     1116 |     1116 |       0.08 | *
    |      356 |        - |     1112 |     1112 |       0.08 | *
    |      357 |        - |     1108 |     1108 |       0.08 | *
    |      365 |        - |     1107 |     1107 |       0.08 | *
    |      370 |        - |     1106 |     1106 |       0.08 | *
    |      380 |        - |     1098 |     1098 |       0.08 | *
    |      386 |        - |     1097 |     1097 |       0.08 | *
    |      393 |        - |     1096 |     1096 |       0.08 | *
    |      398 |        - |     1095 |     1095 |       0.09 | *
    |      399 |        - |     1077 |     1077 |       0.09 | *
    |      402 |        - |     1072 |     1072 |       0.09 | *
    |      414 |        - |     1071 |     1071 |       0.09 | *
    |      424 |        - |     1069 |     1069 |       0.09 | *
    |      425 |        - |     1066 |     1066 |       0.09 | *
    |      459 |        - |     1065 |     1065 |       0.09 | *
    |      466 |        - |     1064 |     1064 |       0.09 | *
    |      475 |        - |     1063 |     1063 |       0.09 | *
    |      489 |        - |     1060 |     1060 |       0.09 | *
    |      491 |        - |     1059 |     1059 |       0.09 | *
    |      495 |        - |     1056 |     1056 |       0.09 | *
    |      499 |        - |     1052 |     1052 |       0.09 | *
    |      507 |        - |     1049 |     1049 |       0.09 | *
    |      515 |        - |     1044 |     1044 |       0.09 | *
    |      518 |        - |     1040 |     1040 |       0.09 | *
    |      523 |        - |     1038 |     1038 |       0.09 | *
    |      526 |        - |     1027 |     1027 |       0.09 | *
    |      545 |        - |     1021 |     1021 |       0.09 | *
    |      549 |        - |     1015 |     1015 |       0.10 | *
    |      553 |        - |     1011 |     1011 |       0.10 | *
    |      554 |        - |     1010 |     1010 |       0.10 | *
    |      558 |        - |     1006 |     1006 |       0.10 | *
    |      559 |        - |      994 |      994 |       0.10 | *
    |      564 |        - |      991 |      991 |       0.10 | *
    |      565 |        - |      990 |      990 |       0.10 | *
    |      571 |        - |      983 |      983 |       0.10 | *
    |      575 |        - |      980 |      980 |       0.10 | *
    |      615 |        - |      979 |      979 |       0.10 | *
    |      630 |        - |      958 |      958 |       0.10 | *
    |      637 |        - |      957 |      957 |       0.10 | *
    |      643 |        - |      954 |      954 |       0.10 | *
    |      644 |        - |      952 |      952 |       0.10 | *
    |      661 |        - |      951 |      951 |       0.10 | *
    |      662 |        - |      950 |      950 |       0.10 | *
    |      666 |        - |      940 |      940 |       0.10 | *
    |      678 |        - |      938 |      938 |       0.11 | *
    |      684 |        - |      931 |      931 |       0.11 | *
    |      712 |        - |      929 |      929 |       0.11 | *
    |      727 |        - |      928 |      928 |       0.11 | *
    |      749 |        - |      925 |      925 |       0.11 | *
    |      761 |        - |      924 |      924 |       0.11 | *
    |      767 |        - |      923 |      923 |       0.11 | *
    |      771 |        - |      922 |      922 |       0.11 | *
    |      775 |        - |      921 |      921 |       0.11 | *
    |      780 |        - |      917 |      917 |       0.11 | *
    |      787 |        - |      914 |      914 |       0.11 | *
    |      794 |        - |      910 |      910 |       0.11 | *
    |      795 |        - |      908 |      908 |       0.11 | *
    |      797 |        - |      907 |      907 |       0.12 | *
    |      798 |        - |      905 |      905 |       0.12 | *
    |      801 |        - |      901 |      901 |       0.12 | *
    |      802 |        - |      899 |      899 |       0.12 | *
    |      812 |        - |      897 |      897 |       0.12 | *
    |      818 |        - |      894 |      894 |       0.12 | *
    |      825 |        - |      893 |      893 |       0.12 | *
    |      830 |        - |      892 |      892 |       0.12 | *
    |      860 |        - |      890 |      890 |       0.12 | *
    |      870 |        - |      887 |      887 |       0.12 | *
    |      881 |        - |      884 |      884 |       0.12 | *
    |      891 |        - |      876 |      876 |       0.12 | *
    |      904 |        - |      872 |      872 |       0.12 | *
    |      918 |        - |      858 |      858 |       0.12 | *
    |      919 |        - |      857 |      857 |       0.13 | *
    |      922 |        - |      855 |      855 |       0.13 | *
    |      924 |        - |      847 |      847 |       0.13 | *
    |      941 |        - |      844 |      844 |       0.13 | *
    |      944 |        - |      843 |      843 |       0.13 | *
    |      978 |        - |      839 |      839 |       0.13 | *
    |      985 |        - |      831 |      831 |       0.13 | *
    |      991 |        - |      829 |      829 |       0.13 | *
    |     1008 |        - |      828 |      828 |       0.13 | *
    |     1036 |        - |      826 |      826 |       0.13 | *
    |     1042 |        - |      824 |      824 |       0.13 | *
    |     1048 |        - |      822 |      822 |       0.13 | *
    |     1061 |        - |      820 |      820 |       0.13 | *
    |     1066 |        - |      817 |      817 |       0.13 | *
    |     1085 |        - |      814 |      814 |       0.13 | *
    |     1091 |        - |      813 |      813 |       0.13 | *
    |     1093 |        - |      812 |      812 |       0.13 | *
    |     1106 |        - |      805 |      805 |       0.14 | *
    |     1114 |        - |      804 |      804 |       0.14 | *
    |     1129 |        - |      803 |      803 |       0.14 | *
    |     1134 |        - |      802 |      802 |       0.14 | *
    |     1139 |        - |      801 |      801 |       0.14 | *
    |     1144 |        - |      800 |      800 |       0.14 | *
    |     1154 |        - |      798 |      798 |       0.14 | *
    |     1167 |        - |      797 |      797 |       0.14 | *
    |     1174 |        - |      796 |      796 |       0.14 | *
    |     1176 |        - |      795 |      795 |       0.14 | *
    |     1177 |        - |      794 |      794 |       0.14 | *
    |     1179 |        - |      785 |      785 |       0.14 | *
    |     1198 |        - |      779 |      779 |       0.14 | *
    |     1214 |        - |      777 |      777 |       0.14 | *
    |     1235 |        - |      768 |      768 |       0.14 | *
    |     1236 |        - |      767 |      767 |       0.14 | *
    |     1237 |        - |      763 |      763 |       0.15 | *
    |     1243 |        - |      760 |      760 |       0.15 | *
    |     1246 |        - |      759 |      759 |       0.15 | *
    |     1248 |        - |      758 |      758 |       0.15 | *
    |     1249 |        - |      757 |      757 |       0.15 | *
    |     1250 |        - |      755 |      755 |       0.15 | *
    |     1257 |        - |      754 |      754 |       0.15 | *
    |     1262 |        - |      753 |      753 |       0.15 | *
    |     1279 |        - |      751 |      751 |       0.15 | *
    |     1280 |        - |      749 |      749 |       0.15 | *
    |     1284 |        - |      746 |      746 |       0.15 | *
    |     1286 |        - |      745 |      745 |       0.15 | *
    |     1293 |        - |      743 |      743 |       0.15 | *
    |     1303 |        - |      739 |      739 |       0.15 | *
    |     1304 |        - |      733 |      733 |       0.15 | *
    |     1307 |        - |      730 |      730 |       0.15 | *
    |     1316 |        - |      727 |      727 |       0.15 | *
    |     1322 |        - |      726 |      726 |       0.15 | *
    |     1331 |        - |      723 |      723 |       0.15 | *
    |     1336 |        - |      722 |      722 |       0.16 | *
    |     1342 |        - |      721 |      721 |       0.16 | *
    |     1344 |        - |      720 |      720 |       0.16 | *
    |     1347 |        - |      717 |      717 |       0.16 | *
    |     1353 |        - |      714 |      714 |       0.16 | *
    |     1356 |        - |      710 |      710 |       0.16 | *
    |     1359 |        - |      706 |      706 |       0.16 | *
    |     1371 |        - |      704 |      704 |       0.16 | *
    |     1374 |        - |      703 |      703 |       0.16 | *
    |     1379 |        - |      702 |      702 |       0.16 | *
    |     1382 |        - |      701 |      701 |       0.16 | *
    |     1387 |        - |      699 |      699 |       0.16 | *
    |     1396 |        - |      698 |      698 |       0.16 | *
    |     1408 |        - |      696 |      696 |       0.16 | *
    |     1412 |        - |      694 |      694 |       0.16 | *
    |     1437 |        - |      693 |      693 |       0.16 | *
    |     1447 |        - |      689 |      689 |       0.16 | *
    |     1473 |        - |      687 |      687 |       0.16 | *
    |     1484 |        - |      683 |      683 |       0.17 | *
    |     1486 |        - |      682 |      682 |       0.17 | *
    |     1490 |        - |      681 |      681 |       0.17 | *
    |     1510 |        - |      675 |      675 |       0.17 | *
    |     1522 |        - |      674 |      674 |       0.17 | *
    |     1542 |        - |      669 |      669 |       0.17 | *
    |     1572 |        - |      664 |      664 |       0.17 | *
    |     1573 |        - |      660 |      660 |       0.17 | *
    |     1586 |        - |      657 |      657 |       0.17 | *
    |     1605 |        - |      656 |      656 |       0.17 | *
    |     1608 |        - |      654 |      654 |       0.17 | *
    |     1631 |        - |      653 |      653 |       0.17 | *
    |     1633 |        - |      652 |      652 |       0.17 | *
    |     1644 |        - |      650 |      650 |       0.17 | *
    |     1647 |        - |      649 |      649 |       0.17 | *
    |     1654 |        - |      645 |      645 |       0.17 | *
    |     1670 |        - |      644 |      644 |       0.17 | *
    |     1673 |        - |      643 |      643 |       0.17 | *
    |     1687 |        - |      642 |      642 |       0.18 | *
    |     1693 |        - |      641 |      641 |       0.18 | *
    |     1694 |        - |      640 |      640 |       0.18 | *
    |     1714 |        - |      639 |      639 |       0.18 | *
    |     1721 |        - |      633 |      633 |       0.18 | *
    |     1724 |        - |      632 |      632 |       0.18 | *
    |     1779 |        - |      630 |      630 |       0.18 | *
    |     1788 |        - |      629 |      629 |       0.18 | *
    |     1806 |        - |      628 |      628 |       0.18 | *
    |     1843 |        - |      627 |      627 |       0.18 | *
    |     1845 |        - |      626 |      626 |       0.18 | *
    |     1860 |        - |      624 |      624 |       0.18 | *
    |     1862 |        - |      623 |      623 |       0.18 | *
    |     1878 |        - |      622 |      622 |       0.18 | *
    |     1891 |        - |      619 |      619 |       0.18 | *
    |     1908 |        - |      618 |      618 |       0.18 | *
    |     1919 |        - |      617 |      617 |       0.19 | *
    |     1923 |        - |      616 |      616 |       0.19 | *
    |     1929 |        - |      615 |      615 |       0.19 | *
    |     1973 |        - |      611 |      611 |       0.19 | *
    |     1980 |        - |      609 |      609 |       0.19 | *
    |     1989 |        - |      608 |      608 |       0.19 | *
    |     2056 |        - |      607 |      607 |       0.19 | *
    |     2060 |        - |      604 |      604 |       0.19 | *
    |     2093 |        - |      603 |      603 |       0.19 | *
    |     2094 |        - |      600 |      600 |       0.19 | *
    |     2095 |        - |      598 |      598 |       0.19 | *
    |     2119 |        - |      597 |      597 |       0.19 | *
    |     2126 |        - |      596 |      596 |       0.19 | *
    |     2155 |        - |      594 |      594 |       0.19 | *
    |     2164 |        - |      593 |      593 |       0.19 | *
    |     2195 |        - |      592 |      592 |       0.19 | *
    |     2242 |        - |      591 |      591 |       0.20 | *
    |     2354 |        - |      590 |      590 |       0.20 | *
    |     2366 |        - |      589 |      589 |       0.20 | *
    |     2387 |        - |      587 |      587 |       0.20 | *
    |     2402 |        - |      585 |      585 |       0.20 | *
    |     2405 |        - |      584 |      584 |       0.20 | *
    |     2431 |        - |      583 |      583 |       0.20 | *
    |     2483 |        - |      579 |      579 |       0.20 | *
    |     2515 |        - |      578 |      578 |       0.20 | *
    |     2533 |        - |      571 |      571 |       0.20 | *
    |     2553 |        - |      567 |      567 |       0.21 | *
    |     2554 |        - |      565 |      565 |       0.21 | *
    |     2558 |        - |      564 |      564 |       0.21 | *
    |     2559 |        - |      562 |      562 |       0.21 | *
    |     2560 |        - |      561 |      561 |       0.21 | *
    |     2561 |        - |      560 |      560 |       0.21 | *
    |     2563 |        - |      559 |      559 |       0.21 | *
    |     2581 |        - |      558 |      558 |       0.21 | *
    |     2585 |        - |      557 |      557 |       0.21 | *
    |     2591 |        - |      556 |      556 |       0.21 | *
    |     2637 |        - |      554 |      554 |       0.21 | *
    |     2642 |        - |      552 |      552 |       0.21 | *
    |     2644 |        - |      551 |      551 |       0.21 | *
    |     2648 |        - |      547 |      547 |       0.21 | *
    |     2687 |        - |      545 |      545 |       0.21 | *
    |     2695 |        - |      544 |      544 |       0.21 | *
    |     2719 |        - |      543 |      543 |       0.21 | *
    |     2732 |        - |      542 |      542 |       0.22 | *
    |     2735 |        - |      540 |      540 |       0.22 | *
    |     2747 |        - |      539 |      539 |       0.22 | *
    |     2754 |        - |      536 |      536 |       0.22 | *
    |     2774 |        - |      535 |      535 |       0.22 | *
    |     2777 |        - |      532 |      532 |       0.22 | *
    |     2784 |        - |      531 |      531 |       0.22 | *
    |     2956 |        - |      529 |      529 |       0.22 | *
    |     2958 |        - |      528 |      528 |       0.22 | *
    |     3015 |        - |      527 |      527 |       0.22 | *
    |     3035 |        - |      526 |      526 |       0.22 | *
    |     3058 |        - |      525 |      525 |       0.22 | *
    |     3061 |        - |      524 |      524 |       0.22 | *
    |     3076 |        - |      523 |      523 |       0.22 | *
    |     3095 |        - |      519 |      519 |       0.22 | *
    |     3139 |        - |      517 |      517 |       0.22 | *
    |     3157 |        - |      516 |      516 |       0.22 | *
    |     3207 |        - |      515 |      515 |       0.23 | *
    |     3213 |        - |      514 |      514 |       0.23 | *
    |     3214 |        - |      512 |      512 |       0.23 | *
    |     3231 |        - |      510 |      510 |       0.23 | *
    |     3245 |        - |      509 |      509 |       0.23 | *
    |     3250 |        - |      506 |      506 |       0.23 | *
    |     3272 |        - |      501 |      501 |       0.23 | *
    |     3331 |        - |      499 |      499 |       0.23 | *
    |     3414 |        - |      496 |      496 |       0.23 | *
    |     3472 |        - |      495 |      495 |       0.23 | *
    |     3476 |        - |      494 |      494 |       0.23 | *
    |     3541 |        - |      492 |      492 |       0.23 | *
    |     3567 |        - |      491 |      491 |       0.23 | *
    |     3629 |        - |      489 |      489 |       0.23 | *
    |     3631 |        - |      488 |      488 |       0.23 | *
    |     3648 |        - |      487 |      487 |       0.23 | *
    |     3659 |        - |      486 |      486 |       0.24 | *
    |     3805 |        - |      484 |      484 |       0.24 | *
    |     3844 |        - |      481 |      481 |       0.24 | *
    |     3859 |        - |      480 |      480 |       0.24 | *
    |     3900 |        - |      479 |      479 |       0.24 | *
    |     3908 |        - |      478 |      478 |       0.24 | *
    |     3926 |        - |      477 |      477 |       0.24 | *
    |     3939 |        - |      476 |      476 |       0.24 | *
    |     3962 |        - |      475 |      475 |       0.24 | *
    |     3978 |        - |      474 |      474 |       0.24 | *
    |     3981 |        - |      473 |      473 |       0.24 | *
    |     3991 |        - |      472 |      472 |       0.24 | *
    |     4246 |        - |      468 |      468 |       0.24 | *
    |     4293 |        - |      467 |      467 |       0.24 | *
    |     4297 |        - |      466 |      466 |       0.24 | *
    |     4302 |        - |      465 |      465 |       0.25 | *
    |     4303 |        - |      463 |      463 |       0.25 | *
    |     4306 |        - |      460 |      460 |       0.25 | *
    |     4362 |        - |      459 |      459 |       0.25 | *
    |     4407 |        - |      455 |      455 |       0.25 | *
    |     4416 |        - |      453 |      453 |       0.25 | *
    |     4419 |        - |      452 |      452 |       0.25 | *
    |     4461 |        - |      451 |      451 |       0.25 | *
    |     4528 |        - |      450 |      450 |       0.25 | *
    |     4571 |        - |      449 |      449 |       0.25 | *
    |     4600 |        - |      448 |      448 |       0.25 | *
    |     4672 |        - |      446 |      446 |       0.25 | *
    |     4689 |        - |      445 |      445 |       0.25 | *
    |     4712 |        - |      444 |      444 |       0.25 | *
    |     4917 |        - |      443 |      443 |       0.26 | *
    |     5134 |        - |      442 |      442 |       0.26 | *
    |     5155 |        - |      441 |      441 |       0.26 | *
    |     5199 |        - |      440 |      440 |       0.26 | *
    |     5273 |        - |      439 |      439 |       0.26 | *
    |     5284 |        - |      438 |      438 |       0.26 | *
    |     5293 |        - |      436 |      436 |       0.26 | *
    |     5312 |        - |      435 |      435 |       0.26 | *
    |     5317 |        - |      434 |      434 |       0.26 | *
    |     5397 |        - |      433 |      433 |       0.26 | *
    |     5401 |        - |      432 |      432 |       0.26 | *
    |     5421 |        - |      431 |      431 |       0.26 | *
    |     5441 |        - |      430 |      430 |       0.27 | *
    |     5483 |        - |      429 |      429 |       0.27 | *
    |     5521 |        - |      428 |      428 |       0.27 | *
    |     5594 |        - |      426 |      426 |       0.27 | *
    |     5649 |        - |      425 |      425 |       0.27 | *
    |     5813 |        - |      424 |      424 |       0.27 | *
    |     5900 |        - |      423 |      423 |       0.27 | *
    |     6006 |        - |      422 |      422 |       0.27 | *
    |     6233 |        - |      421 |      421 |       0.27 | *
    |     6261 |        - |      420 |      420 |       0.27 | *
    |     6398 |        - |      419 |      419 |       0.28 | *
    |     6420 |        - |      418 |      418 |       0.28 | *
    |     6432 |        - |      417 |      417 |       0.28 | *
    |     6543 |        - |      416 |      416 |       0.28 | *
    |     6611 |        - |      415 |      415 |       0.28 | *
    |     6768 |        - |      414 |      414 |       0.28 | *
    |     6859 |        - |      413 |      413 |       0.28 | *
    |     6886 |        - |      412 |      412 |       0.28 | *
    |     6949 |        - |      411 |      411 |       0.28 | *
    |     6986 |        - |      410 |      410 |       0.28 | *
    |     6998 |        - |      409 |      409 |       0.29 | *
    |     7044 |        - |      407 |      407 |       0.29 | *
    |     7060 |        - |      405 |      405 |       0.29 | *
    |     7411 |        - |      404 |      404 |       0.29 | *
    |     7433 |        - |      403 |      403 |       0.29 | *
    |     7461 |        - |      402 |      402 |       0.29 | *
    |     7526 |        - |      401 |      401 |       0.29 | *
    |     7589 |        - |      400 |      400 |       0.29 | *
    |     7693 |        - |      399 |      399 |       0.29 | *
    |     7782 |        - |      398 |      398 |       0.29 | *
    |     7839 |        - |      397 |      397 |       0.29 | *
    |     7885 |        - |      396 |      396 |       0.30 | *
    |     7932 |        - |      394 |      394 |       0.30 | *
    |     8381 |        - |      393 |      393 |       0.30 | *
    |     8662 |        - |      391 |      391 |       0.30 | *
    |     8699 |        - |      390 |      390 |       0.30 | *
    |     8745 |        - |      389 |      389 |       0.30 | *
    |     8781 |        - |      388 |      388 |       0.30 | *
    |     8984 |        - |      387 |      387 |       0.30 | *
    |     9046 |        - |      386 |      386 |       0.30 | *
    |     9094 |        - |      385 |      385 |       0.31 | *
    |     9273 |        - |      383 |      383 |       0.31 | *
    |     9738 |        - |      382 |      382 |       0.31 | *
    |     9739 |        - |      381 |      381 |       0.31 | *
    |     9772 |        - |      380 |      380 |       0.31 | *
    |     9840 |        - |      379 |      379 |       0.31 | *
    |     9955 |        - |      378 |      378 |       0.31 | *
    |      10K |        - |      377 |      377 |       0.31 | *
    |      10K |        - |      376 |      376 |       0.31 | *
    |      10K |        - |      375 |      375 |       0.32 | *
    |      11K |        - |      374 |      374 |       0.32 | *
    |      11K |        - |      373 |      373 |       0.32 | *
    |      11K |        - |      372 |      372 |       0.32 | *
    |      11K |        - |      371 |      371 |       0.32 | *
    |      11K |        - |      370 |      370 |       0.33 | *
    |      12K |        - |      369 |      369 |       0.33 | *
    |      12K |        - |      368 |      368 |       0.33 | *
    |      12K |        - |      367 |      367 |       0.33 | *
    |      12K |        - |      366 |      366 |       0.33 | *
    |      13K |        - |      365 |      365 |       0.33 | *
    |      13K |        - |      364 |      364 |       0.33 | *
    |      13K |        - |      363 |      363 |       0.33 | *
    |      13K |        - |      362 |      362 |       0.34 | *
    |      13K |        - |      361 |      361 |       0.34 | *
    |      13K |        - |      360 |      360 |       0.34 | *
    |      13K |        - |      359 |      359 |       0.34 | *
    |      14K |        - |      358 |      358 |       0.34 | *
    |      14K |        - |      357 |      357 |       0.35 | *
    |      15K |        - |      356 |      356 |       0.35 | *
    |      15K |        - |      355 |      355 |       0.35 | *
    |      15K |        - |      354 |      354 |       0.35 | *
    |      16K |        - |      353 |      353 |       0.36 | *
    |      16K |        - |      352 |      352 |       0.36 | *
    |      17K |        - |      350 |      350 |       0.36 | *
    |      17K |        - |      349 |      349 |       0.36 | *
    |      17K |        - |      348 |      348 |       0.36 | *
    |      17K |        - |      347 |      347 |       0.36 | *
    |      17K |        - |      346 |      346 |       0.36 | *
    |      17K |        - |      345 |      345 |       0.36 | *
    |      17K |        - |      343 |      343 |       0.36 | *
    |      17K |        - |      342 |      342 |       0.37 | *
    |      18K |        - |      341 |      341 |       0.37 | *
    |      18K |        - |      340 |      340 |       0.37 | *
    |      18K |        - |      339 |      339 |       0.37 | *
    |      18K |        - |      338 |      338 |       0.37 | *
    |      18K |        - |      337 |      337 |       0.38 | *
    |      20K |        - |      336 |      336 |       0.38 | *
    |      21K |        - |      335 |      335 |       0.39 | *
    |      21K |        - |      334 |      334 |       0.39 | *
    |      22K |        - |      333 |      333 |       0.39 | *
    |      23K |        - |      332 |      332 |       0.40 | *
    |      23K |        - |      331 |      331 |       0.40 | *
    |      23K |        - |      330 |      330 |       0.40 | *
    |      23K |        - |      329 |      329 |       0.40 | *
    |      24K |        - |      328 |      328 |       0.40 | *
    |      25K |        - |      327 |      327 |       0.41 | *
    |      25K |        - |      326 |      326 |       0.41 | *
    |      31K |        - |      325 |      325 |       0.43 | *
    |      31K |        - |      324 |      324 |       0.43 | *
    |      31K |        - |      323 |      323 |       0.43 | *
    |      32K |        - |      322 |      322 |       0.44 | *
    |      32K |        - |      321 |      321 |       0.44 | *
    |      32K |        - |      320 |      320 |       0.44 | *
    |      32K |        - |      319 |      319 |       0.44 | *
    |      32K |        - |      318 |      318 |       0.44 | *
    |      36K |        - |      317 |      317 |       0.46 | *
    |      47K |        - |      316 |      316 |       0.50 | *
    |      48K |        - |      315 |      315 |       0.50 | *
    |      52K |        - |      314 |      314 |       0.53 | *
    |      53K |        - |      313 |      313 |       0.53 | *
    |      53K |        - |      312 |      312 |       0.53 | *
    |      53K |        - |      311 |      311 |       0.53 | *
    |      69K |        - |      310 |      310 |       0.62 | *
    |      71K |        - |      309 |      309 |       0.64 | *
    |      71K |        - |      308 |      308 |       0.64 | *
    |      88K |        - |      307 |      307 |       0.75 | *
    |     105K |        - |      306 |      306 |       0.86 | *
    |     106K |        - |      305 |      305 |       0.86 | *
    |     107K |        - |      304 |      304 |       0.88 | *
    |     107K |        - |      303 |      303 |       0.88 | *
    |     117K |        - |      302 |      302 |       0.94 | *
    |     117K |        - |      301 |      301 |       0.94 | *
    |     122K |        - |      300 |      300 |       0.96 | *
    |     123K |        - |      299 |      299 |       0.97 | *
    |     167K |        - |      298 |      298 |       1.19 | *
    |     180K |        - |      297 |      297 |       1.24 | *
    |     237K |        - |      296 |      296 |       1.36 | *
    |     256K |        - |      295 |      295 |       1.40 | *
    |     369K |        - |      294 |      294 |       1.57 | *
    |     375K |        - |      293 |      293 |       1.58 | *
    |     375K |        - |      292 |      292 |       1.58 | *
    |     429K |        - |      291 |      291 |       1.66 | *
    |     479K |        - |      290 |      290 |       1.73 | *
    |    1025K |        - |      289 |      289 |       2.21 | *
    |    1857K |        - |      288 |      288 |       2.93 | *
    |    1857K |        - |      287 |      287 |       2.93 | *
    |      16M |        - |      286 |      286 |      18.61 | *
    |      32M |        - |      285 |      285 |      32.63 | *
    |      55M |        - |      284 |      284 |      53.21 | *
    |     171M |        - |      283 |      283 |     154.63 | *
    |     181M |        - |      282 |      282 |     162.85 | *
    |     201M |        - |      281 |      281 |     180.97 | *
    |     245M |        - |      280 |      280 |     219.42 | *
    |     245M |        - |      279 |      279 |     219.42 | *
    |     330M |        - |      278 |      278 |     294.37 | *
    |     456M |        - |      277 |      277 |     403.92 | *
    |     470M |        - |      276 |      276 |     416.66 | *
    |     580M |        - |      275 |      275 |     512.36 | *
    |     603M |        - |      274 |      274 |     532.86 | *
    |     681M |        - |      273 |      273 |     601.53 | *
    |     720M |        - |      272 |      272 |     635.35 | *
    |     927M |        - |      271 |      271 |     816.29 | *
    |     970M |        - |      270 |      270 |     854.11 | *
    |    1003M |        - |      269 |      269 |     883.55 | *
    |    1188M |        - |      268 |      268 |    1045.02 | *
    |    1476M |        - |      267 |      267 |    1296.75 | *
    |    1856M |        - |      266 |      266 |    1626.81 | *
    \--------------------------------------------------------/

Neighborhoods statistics (values in %):

    /----------------------------------------------------------------\
    | Move               | Improvs. | Sideways |  Accepts |  Rejects |
    |--------------------|----------|----------|----------|----------|
    | Shift(mk)          |     274K |    1528K |    2075K |     171M |
    | Shift              |      27K |    6199K |    6728K |     167M |
    | ShiftSmart(mk)     |    3009K |      18M |      23M |     150M |
    | ShiftSmart         |     301K |      41M |      42M |     131M |
    | SimpSwap(mk)       |      551 |     1166 |     2271 |     173M |
    | SimpSwap           |      107 |     3950 |     5090 |     173M |
    | SimpSwapSmart(mk)  |      10K |      23K |      44K |     173M |
    | SimpSwapSmart      |     2091 |      73K |      91K |     173M |
    | Swap(mk)           |      201 |      433 |      867 |     173M |
    | Swap               |       45 |     1728 |     2268 |     173M |
    | SwapSmart(mk)      |     3982 |     8580 |      15K |     173M |
    | SwapSmart          |      761 |      23K |      28K |     173M |
    | Switch(mk)         |     138K |    7687K |    7944K |     165M |
    | Switch             |      14K |    9739K |      10M |     163M |
    | SwitchSmart(mk)    |    1625K |    6593K |    9066K |     164M |
    | SwitchSmart        |     162K |      20M |      21M |     152M |
    | TaskMove(mk)       |     8389 |      17K |      31K |     173M |
    | TaskMove           |      827 |      27K |      34K |     173M |
    | TaskMoveSmart(mk)  |      58K |     126K |     217K |     173M |
    | TaskMoveSmart      |     6097 |     202K |     240K |     173M |
    | 2-Shift(mk)        |      65K |     561K |     682K |     172M |
    | 2-Shift            |     6555 |    1647K |    1798K |     171M |
    | 2-ShiftSmart(mk)   |    2351K |    8188K |      11M |     162M |
    | 2-ShiftSmart       |     234K |      22M |      23M |     150M |
    \----------------------------------------------------------------/

Best makespan.....: 266
N. of Iterations..: 4155023665
Total runtime.....: 3600.01s
Using Python-MIP package version 1.6.8
Começo a execução
    /-------------------------------------------------------------------------------\
    |    Makespan   |   Free Jobs   | Free Machines |    It. Time   |   Total Time  | 
    |-------------------------------------------------------------------------------|
    \-------------------------------------------------------------------------------/
Terminou a execução
