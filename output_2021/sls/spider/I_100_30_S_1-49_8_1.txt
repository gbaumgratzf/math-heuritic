Instance....: instances/I_100_30_S_1-49_8.txt
Algorithm...: Simulated Annealing (alpha=0.990, saMax=10M, t0=1)
Other params: maxIters=100M, seed=-1, timeLimit=3600.00s

    /--------------------------------------------------------\
    |     Iter |   RDP(%) |       S* |       S' |       Time | 
    |----------|----------|----------|----------|------------|
    |        0 |        - |      601 |      601 |       0.00 | s0
    |        0 |        - |      557 |      557 |       0.00 | *
    |        2 |        - |      554 |      554 |       0.01 | *
    |        4 |        - |      538 |      538 |       0.02 | *
    |        5 |        - |      514 |      514 |       0.03 | *
    |        9 |        - |      492 |      492 |       0.03 | *
    |       14 |        - |      484 |      484 |       0.03 | *
    |       17 |        - |      479 |      479 |       0.03 | *
    |       18 |        - |      477 |      477 |       0.03 | *
    |       26 |        - |      474 |      474 |       0.03 | *
    |       35 |        - |      473 |      473 |       0.03 | *
    |       37 |        - |      461 |      461 |       0.03 | *
    |       38 |        - |      447 |      447 |       0.04 | *
    |       42 |        - |      445 |      445 |       0.04 | *
    |       43 |        - |      436 |      436 |       0.04 | *
    |       47 |        - |      435 |      435 |       0.04 | *
    |       48 |        - |      432 |      432 |       0.04 | *
    |       57 |        - |      431 |      431 |       0.04 | *
    |       68 |        - |      424 |      424 |       0.04 | *
    |       70 |        - |      416 |      416 |       0.04 | *
    |       72 |        - |      413 |      413 |       0.04 | *
    |       81 |        - |      411 |      411 |       0.04 | *
    |       82 |        - |      405 |      405 |       0.04 | *
    |       87 |        - |      402 |      402 |       0.04 | *
    |       89 |        - |      365 |      365 |       0.04 | *
    |       91 |        - |      363 |      363 |       0.05 | *
    |       96 |        - |      361 |      361 |       0.05 | *
    |      100 |        - |      354 |      354 |       0.05 | *
    |      101 |        - |      335 |      335 |       0.05 | *
    |      103 |        - |      334 |      334 |       0.05 | *
    |      104 |        - |      329 |      329 |       0.05 | *
    |      107 |        - |      322 |      322 |       0.05 | *
    |      117 |        - |      320 |      320 |       0.05 | *
    |      118 |        - |      317 |      317 |       0.05 | *
    |      123 |        - |      314 |      314 |       0.05 | *
    |      130 |        - |      310 |      310 |       0.05 | *
    |      142 |        - |      306 |      306 |       0.05 | *
    |      144 |        - |      300 |      300 |       0.05 | *
    |      153 |        - |      299 |      299 |       0.05 | *
    |      173 |        - |      298 |      298 |       0.05 | *
    |      182 |        - |      296 |      296 |       0.06 | *
    |      185 |        - |      294 |      294 |       0.06 | *
    |      195 |        - |      291 |      291 |       0.06 | *
    |      196 |        - |      287 |      287 |       0.06 | *
    |      198 |        - |      286 |      286 |       0.06 | *
    |      199 |        - |      282 |      282 |       0.06 | *
    |      214 |        - |      281 |      281 |       0.06 | *
    |      215 |        - |      279 |      279 |       0.06 | *
    |      228 |        - |      275 |      275 |       0.06 | *
    |      232 |        - |      274 |      274 |       0.06 | *
    |      235 |        - |      272 |      272 |       0.06 | *
    |      239 |        - |      271 |      271 |       0.06 | *
    |      240 |        - |      270 |      270 |       0.06 | *
    |      243 |        - |      268 |      268 |       0.06 | *
    |      248 |        - |      266 |      266 |       0.06 | *
    |      293 |        - |      260 |      260 |       0.07 | *
    |      297 |        - |      259 |      259 |       0.07 | *
    |      323 |        - |      258 |      258 |       0.07 | *
    |      376 |        - |      257 |      257 |       0.07 | *
    |      383 |        - |      256 |      256 |       0.07 | *
    |      384 |        - |      254 |      254 |       0.07 | *
    |      397 |        - |      253 |      253 |       0.07 | *
    |      430 |        - |      250 |      250 |       0.07 | *
    |      442 |        - |      249 |      249 |       0.07 | *
    |      445 |        - |      248 |      248 |       0.07 | *
    |      447 |        - |      247 |      247 |       0.07 | *
    |      463 |        - |      245 |      245 |       0.07 | *
    |      469 |        - |      244 |      244 |       0.07 | *
    |      479 |        - |      238 |      238 |       0.07 | *
    |      484 |        - |      236 |      236 |       0.08 | *
    |      502 |        - |      235 |      235 |       0.08 | *
    |      508 |        - |      232 |      232 |       0.08 | *
    |      509 |        - |      230 |      230 |       0.08 | *
    |      532 |        - |      226 |      226 |       0.08 | *
    |      569 |        - |      221 |      221 |       0.08 | *
    |      579 |        - |      220 |      220 |       0.08 | *
    |      584 |        - |      219 |      219 |       0.08 | *
    |      590 |        - |      215 |      215 |       0.08 | *
    |      610 |        - |      214 |      214 |       0.08 | *
    |      616 |        - |      213 |      213 |       0.08 | *
    |      625 |        - |      212 |      212 |       0.08 | *
    |      638 |        - |      210 |      210 |       0.08 | *
    |      649 |        - |      201 |      201 |       0.08 | *
    |      656 |        - |      199 |      199 |       0.08 | *
    |      657 |        - |      198 |      198 |       0.08 | *
    |      679 |        - |      193 |      193 |       0.09 | *
    |      682 |        - |      191 |      191 |       0.09 | *
    |      690 |        - |      190 |      190 |       0.09 | *
    |      696 |        - |      184 |      184 |       0.09 | *
    |      707 |        - |      183 |      183 |       0.09 | *
    |      766 |        - |      182 |      182 |       0.09 | *
    |      775 |        - |      180 |      180 |       0.09 | *
    |      795 |        - |      177 |      177 |       0.09 | *
    |      828 |        - |      176 |      176 |       0.09 | *
    |      885 |        - |      175 |      175 |       0.09 | *
    |      889 |        - |      173 |      173 |       0.09 | *
    |      919 |        - |      171 |      171 |       0.10 | *
    |      920 |        - |      169 |      169 |       0.10 | *
    |      931 |        - |      168 |      168 |       0.10 | *
    |      932 |        - |      165 |      165 |       0.10 | *
    |      934 |        - |      163 |      163 |       0.10 | *
    |      946 |        - |      162 |      162 |       0.10 | *
    |      955 |        - |      160 |      160 |       0.10 | *
    |     1015 |        - |      159 |      159 |       0.10 | *
    |     1027 |        - |      158 |      158 |       0.10 | *
    |     1107 |        - |      157 |      157 |       0.10 | *
    |     1126 |        - |      156 |      156 |       0.10 | *
    |     1136 |        - |      154 |      154 |       0.10 | *
    |     1148 |        - |      153 |      153 |       0.10 | *
    |     1193 |        - |      152 |      152 |       0.10 | *
    |     1211 |        - |      151 |      151 |       0.10 | *
    |     1228 |        - |      149 |      149 |       0.11 | *
    |     1259 |        - |      147 |      147 |       0.11 | *
    |     1289 |        - |      146 |      146 |       0.11 | *
    |     1317 |        - |      145 |      145 |       0.11 | *
    |     1321 |        - |      144 |      144 |       0.11 | *
    |     1329 |        - |      142 |      142 |       0.11 | *
    |     1421 |        - |      141 |      141 |       0.11 | *
    |     1507 |        - |      139 |      139 |       0.11 | *
    |     1513 |        - |      138 |      138 |       0.11 | *
    |     1652 |        - |      137 |      137 |       0.11 | *
    |     1691 |        - |      136 |      136 |       0.11 | *
    |     1814 |        - |      135 |      135 |       0.12 | *
    |     1881 |        - |      134 |      134 |       0.12 | *
    |     1883 |        - |      133 |      133 |       0.12 | *
    |     1890 |        - |      131 |      131 |       0.12 | *
    |     1966 |        - |      130 |      130 |       0.12 | *
    |     1987 |        - |      129 |      129 |       0.12 | *
    |     2221 |        - |      127 |      127 |       0.13 | *
    |     2266 |        - |      126 |      126 |       0.13 | *
    |     2283 |        - |      124 |      124 |       0.13 | *
    |     2284 |        - |      123 |      123 |       0.13 | *
    |     2487 |        - |      122 |      122 |       0.13 | *
    |     2521 |        - |      121 |      121 |       0.13 | *
    |     2522 |        - |      120 |      120 |       0.13 | *
    |     2622 |        - |      119 |      119 |       0.13 | *
    |     2631 |        - |      117 |      117 |       0.13 | *
    |     2641 |        - |      116 |      116 |       0.13 | *
    |     2685 |        - |      114 |      114 |       0.13 | *
    |     2779 |        - |      113 |      113 |       0.14 | *
    |     3003 |        - |      112 |      112 |       0.14 | *
    |     3096 |        - |      111 |      111 |       0.14 | *
    |     3269 |        - |      110 |      110 |       0.14 | *
    |     3284 |        - |      109 |      109 |       0.14 | *
    |     3316 |        - |      108 |      108 |       0.14 | *
    |     3498 |        - |      107 |      107 |       0.14 | *
    |     3528 |        - |      106 |      106 |       0.15 | *
    |     3572 |        - |      105 |      105 |       0.15 | *
    |     3662 |        - |      104 |      104 |       0.15 | *
    |     4168 |        - |      102 |      102 |       0.15 | *
    |     4251 |        - |      101 |      101 |       0.15 | *
    |     4272 |        - |      100 |      100 |       0.15 | *
    |     4652 |        - |       99 |       99 |       0.16 | *
    |     4690 |        - |       98 |       98 |       0.16 | *
    |     5183 |        - |       97 |       97 |       0.16 | *
    |     5254 |        - |       96 |       96 |       0.16 | *
    |     5301 |        - |       95 |       95 |       0.16 | *
    |     5322 |        - |       94 |       94 |       0.16 | *
    |     5331 |        - |       93 |       93 |       0.16 | *
    |     5387 |        - |       92 |       92 |       0.16 | *
    |     5430 |        - |       91 |       91 |       0.16 | *
    |     5804 |        - |       90 |       90 |       0.16 | *
    |     5838 |        - |       89 |       89 |       0.16 | *
    |     6050 |        - |       88 |       88 |       0.17 | *
    |     6438 |        - |       87 |       87 |       0.17 | *
    |     6636 |        - |       85 |       85 |       0.17 | *
    |     6883 |        - |       84 |       84 |       0.17 | *
    |     6899 |        - |       83 |       83 |       0.17 | *
    |     6904 |        - |       82 |       82 |       0.17 | *
    |     7113 |        - |       81 |       81 |       0.17 | *
    |     7407 |        - |       80 |       80 |       0.17 | *
    |     8005 |        - |       79 |       79 |       0.18 | *
    |     8647 |        - |       78 |       78 |       0.18 | *
    |     9251 |        - |       77 |       77 |       0.18 | *
    |      11K |        - |       75 |       75 |       0.19 | *
    |      11K |        - |       74 |       74 |       0.19 | *
    |      11K |        - |       72 |       72 |       0.19 | *
    |      11K |        - |       71 |       71 |       0.19 | *
    |      14K |        - |       70 |       70 |       0.19 | *
    |      14K |        - |       69 |       69 |       0.20 | *
    |      17K |        - |       68 |       68 |       0.20 | *
    |      22K |        - |       67 |       67 |       0.21 | *
    |      25K |        - |       66 |       66 |       0.22 | *
    |      27K |        - |       65 |       65 |       0.22 | *
    |      32K |        - |       64 |       64 |       0.23 | *
    |      32K |        - |       63 |       63 |       0.23 | *
    |      32K |        - |       62 |       62 |       0.24 | *
    |      33K |        - |       61 |       61 |       0.24 | *
    |      34K |        - |       60 |       60 |       0.24 | *
    |      35K |        - |       59 |       59 |       0.24 | *
    |      35K |        - |       58 |       58 |       0.24 | *
    |      35K |        - |       57 |       57 |       0.24 | *
    |      35K |        - |       56 |       56 |       0.25 | *
    |      38K |        - |       55 |       55 |       0.25 | *
    |      48K |        - |       54 |       54 |       0.27 | *
    |      49K |        - |       53 |       53 |       0.27 | *
    |      53K |        - |       52 |       52 |       0.28 | *
    |      53K |        - |       51 |       51 |       0.28 | *
    |      55K |        - |       50 |       50 |       0.29 | *
    |      81K |        - |       49 |       49 |       0.33 | *
    |     103K |        - |       48 |       48 |       0.36 | *
    |     115K |        - |       47 |       47 |       0.39 | *
    |     142K |        - |       46 |       46 |       0.43 | *
    |     145K |        - |       45 |       45 |       0.44 | *
    |     149K |        - |       44 |       44 |       0.45 | *
    |     263K |        - |       43 |       43 |       0.70 | *
    |     301K |        - |       42 |       42 |       0.79 | *
    |     746K |        - |       41 |       41 |       1.59 | *
    |     964K |        - |       40 |       40 |       1.78 | *
    |    1055K |        - |       39 |       39 |       1.89 | *
    |    5607K |        - |       38 |       38 |       5.93 | *
    |      23M |        - |       37 |       37 |      19.40 | *
    |      46M |        - |       36 |       36 |      33.98 | *
    |     114M |        - |       35 |       35 |      76.61 | *
    |     289M |        - |       34 |       34 |     185.18 | *
    |     293M |        - |       33 |       33 |     187.92 | *
    |     580M |        - |       32 |       32 |     364.55 | *
    \--------------------------------------------------------/

Neighborhoods statistics (values in %):

    /----------------------------------------------------------------\
    | Move               | Improvs. | Sideways |  Accepts |  Rejects |
    |--------------------|----------|----------|----------|----------|
    | Shift(mk)          |     172K |    1124K |    1361K |     248M |
    | Shift              |     5921 |      17M |      17M |     232M |
    | ShiftSmart(mk)     |     327K |    1360K |    1813K |     247M |
    | ShiftSmart         |      11K |      21M |      22M |     227M |
    | SimpSwap(mk)       |     1636 |     3783 |     6000 |     249M |
    | SimpSwap           |      102 |      24K |      26K |     249M |
    | SimpSwapSmart(mk)  |     3862 |     9985 |      15K |     249M |
    | SimpSwapSmart      |      294 |      60K |      63K |     249M |
    | Swap(mk)           |     1142 |     2559 |     4154 |     249M |
    | Swap               |       91 |      24K |      25K |     249M |
    | SwapSmart(mk)      |     5138 |      12K |      19K |     249M |
    | SwapSmart          |      368 |      95K |      99K |     249M |
    | Switch(mk)         |      69K |      78M |      78M |     170M |
    | Switch             |     2297 |      87M |      87M |     162M |
    | SwitchSmart(mk)    |     150K |    1118K |    1320K |     248M |
    | SwitchSmart        |     4992 |      15M |      15M |     234M |
    | TaskMove(mk)       |     8538 |      28K |      40K |     249M |
    | TaskMove           |      353 |      93K |      98K |     249M |
    | TaskMoveSmart(mk)  |      25K |      84K |     119K |     249M |
    | TaskMoveSmart      |      882 |     277K |     293K |     249M |
    | 2-Shift(mk)        |     120K |      38M |      38M |     211M |
    | 2-Shift            |     4099 |      54M |      54M |     195M |
    | 2-ShiftSmart(mk)   |     178K |      91M |      91M |     158M |
    | 2-ShiftSmart       |     5863 |      81M |      81M |     168M |
    \----------------------------------------------------------------/

Best makespan.....: 32
N. of Iterations..: 5978432566
Total runtime.....: 3600.02s
Using Python-MIP package version 1.6.8
Começo a execução
    /-------------------------------------------------------------------------------\
    |    Makespan   |   Free Jobs   | Free Machines |    It. Time   |   Total Time  | 
    |-------------------------------------------------------------------------------|
    \-------------------------------------------------------------------------------/
Terminou a execução
