Instance....: instances/I_50_20_S_1-9_2.txt
Algorithm...: Simulated Annealing (alpha=0.990, saMax=10M, t0=1)
Other params: maxIters=100M, seed=-1, timeLimit=3600.00s

    /--------------------------------------------------------\
    |     Iter |   RDP(%) |       S* |       S' |       Time | 
    |----------|----------|----------|----------|------------|
    |        0 |        - |      362 |      362 |       0.00 | s0
    |        0 |        - |      359 |      359 |       0.00 | *
    |        1 |        - |      351 |      351 |       0.01 | *
    |        2 |        - |      305 |      305 |       0.01 | *
    |       18 |        - |      244 |      244 |       0.02 | *
    |       19 |        - |      243 |      243 |       0.02 | *
    |       23 |        - |      228 |      228 |       0.03 | *
    |       24 |        - |      218 |      218 |       0.03 | *
    |       52 |        - |      217 |      217 |       0.03 | *
    |       53 |        - |      211 |      211 |       0.03 | *
    |       54 |        - |      204 |      204 |       0.03 | *
    |       57 |        - |      203 |      203 |       0.03 | *
    |       60 |        - |      202 |      202 |       0.04 | *
    |       62 |        - |      192 |      192 |       0.04 | *
    |       63 |        - |      190 |      190 |       0.04 | *
    |       64 |        - |      186 |      186 |       0.04 | *
    |       65 |        - |      185 |      185 |       0.04 | *
    |       70 |        - |      176 |      176 |       0.04 | *
    |       76 |        - |      170 |      170 |       0.04 | *
    |       80 |        - |      162 |      162 |       0.04 | *
    |       82 |        - |      142 |      142 |       0.04 | *
    |      112 |        - |      138 |      138 |       0.04 | *
    |      120 |        - |      137 |      137 |       0.04 | *
    |      127 |        - |      136 |      136 |       0.04 | *
    |      138 |        - |      134 |      134 |       0.04 | *
    |      145 |        - |      132 |      132 |       0.04 | *
    |      150 |        - |      131 |      131 |       0.05 | *
    |      157 |        - |      129 |      129 |       0.05 | *
    |      160 |        - |      127 |      127 |       0.05 | *
    |      200 |        - |      126 |      126 |       0.05 | *
    |      205 |        - |      124 |      124 |       0.05 | *
    |      214 |        - |      122 |      122 |       0.05 | *
    |      241 |        - |      119 |      119 |       0.05 | *
    |      244 |        - |      117 |      117 |       0.05 | *
    |      245 |        - |      116 |      116 |       0.05 | *
    |      250 |        - |      113 |      113 |       0.05 | *
    |      266 |        - |      112 |      112 |       0.05 | *
    |      285 |        - |      110 |      110 |       0.05 | *
    |      296 |        - |      109 |      109 |       0.05 | *
    |      298 |        - |      104 |      104 |       0.06 | *
    |      321 |        - |      103 |      103 |       0.06 | *
    |      347 |        - |      102 |      102 |       0.06 | *
    |      382 |        - |      100 |      100 |       0.06 | *
    |      383 |        - |       99 |       99 |       0.06 | *
    |      385 |        - |       98 |       98 |       0.06 | *
    |      386 |        - |       96 |       96 |       0.06 | *
    |      456 |        - |       95 |       95 |       0.06 | *
    |      471 |        - |       94 |       94 |       0.06 | *
    |      526 |        - |       92 |       92 |       0.06 | *
    |      572 |        - |       91 |       91 |       0.06 | *
    |      586 |        - |       88 |       88 |       0.07 | *
    |      676 |        - |       87 |       87 |       0.07 | *
    |      679 |        - |       83 |       83 |       0.07 | *
    |      689 |        - |       81 |       81 |       0.07 | *
    |      702 |        - |       80 |       80 |       0.07 | *
    |      731 |        - |       79 |       79 |       0.07 | *
    |      757 |        - |       77 |       77 |       0.07 | *
    |      826 |        - |       76 |       76 |       0.07 | *
    |      874 |        - |       74 |       74 |       0.07 | *
    |     1009 |        - |       73 |       73 |       0.07 | *
    |     1092 |        - |       71 |       71 |       0.07 | *
    |     1140 |        - |       70 |       70 |       0.07 | *
    |     1153 |        - |       69 |       69 |       0.07 | *
    |     1183 |        - |       68 |       68 |       0.07 | *
    |     1229 |        - |       66 |       66 |       0.08 | *
    |     1309 |        - |       65 |       65 |       0.08 | *
    |     1395 |        - |       64 |       64 |       0.08 | *
    |     1499 |        - |       62 |       62 |       0.08 | *
    |     1801 |        - |       61 |       61 |       0.08 | *
    |     1806 |        - |       60 |       60 |       0.08 | *
    |     1814 |        - |       57 |       57 |       0.08 | *
    |     1860 |        - |       56 |       56 |       0.08 | *
    |     1913 |        - |       55 |       55 |       0.08 | *
    |     2150 |        - |       53 |       53 |       0.08 | *
    |     2246 |        - |       52 |       52 |       0.08 | *
    |     2763 |        - |       51 |       51 |       0.09 | *
    |     2791 |        - |       50 |       50 |       0.09 | *
    |     3453 |        - |       49 |       49 |       0.09 | *
    |     3457 |        - |       48 |       48 |       0.09 | *
    |     4169 |        - |       47 |       47 |       0.09 | *
    |     4269 |        - |       46 |       46 |       0.09 | *
    |     4272 |        - |       45 |       45 |       0.10 | *
    |     4509 |        - |       44 |       44 |       0.10 | *
    |     4529 |        - |       43 |       43 |       0.10 | *
    |     4550 |        - |       42 |       42 |       0.10 | *
    |     4873 |        - |       41 |       41 |       0.10 | *
    |     5001 |        - |       40 |       40 |       0.10 | *
    |     6964 |        - |       39 |       39 |       0.10 | *
    |     7003 |        - |       38 |       38 |       0.10 | *
    |     9207 |        - |       37 |       37 |       0.11 | *
    |     9218 |        - |       36 |       36 |       0.11 | *
    |      11K |        - |       35 |       35 |       0.11 | *
    |      12K |        - |       34 |       34 |       0.11 | *
    |      13K |        - |       33 |       33 |       0.11 | *
    |      16K |        - |       32 |       32 |       0.12 | *
    |      16K |        - |       31 |       31 |       0.12 | *
    |      16K |        - |       30 |       30 |       0.12 | *
    |      21K |        - |       29 |       29 |       0.12 | *
    |      26K |        - |       28 |       28 |       0.13 | *
    |      41K |        - |       27 |       27 |       0.14 | *
    |      41K |        - |       26 |       26 |       0.14 | *
    |     168K |        - |       25 |       25 |       0.25 | *
    |    1034K |        - |       24 |       24 |       1.14 | *
    \--------------------------------------------------------/

Neighborhoods statistics (values in %):

    /----------------------------------------------------------------\
    | Move               | Improvs. | Sideways |  Accepts |  Rejects |
    |--------------------|----------|----------|----------|----------|
    | Shift(mk)          |    1545K |      21M |      23M |     267M |
    | Shift              |      89K |     116M |     117M |     181M |
    | ShiftSmart(mk)     |    2270K |      24M |      27M |     264M |
    | ShiftSmart         |     131K |     136M |     137M |     161M |
    | SimpSwap(mk)       |      12K |      32K |      52K |     298M |
    | SimpSwap           |     1182 |     231K |     242K |     298M |
    | SimpSwapSmart(mk)  |      22K |      60K |     101K |     298M |
    | SimpSwapSmart      |     2408 |     470K |     494K |     298M |
    | Swap(mk)           |      11K |      30K |      48K |     298M |
    | Swap               |     1053 |     220K |     231K |     298M |
    | SwapSmart(mk)      |      18K |      67K |      94K |     298M |
    | SwapSmart          |     1761 |     383K |     399K |     298M |
    | Switch(mk)         |     862K |     150M |     151M |     139M |
    | Switch             |      50K |     181M |     182M |     117M |
    | SwitchSmart(mk)    |    1573K |      22M |      24M |     267M |
    | SwitchSmart        |      90K |     114M |     115M |     183M |
    | TaskMove(mk)       |      57K |     178K |     253K |     298M |
    | TaskMove           |     2896 |    1166K |    1198K |     297M |
    | TaskMoveSmart(mk)  |      86K |     331K |     448K |     298M |
    | TaskMoveSmart      |     4520 |    1943K |    1996K |     296M |
    | 2-Shift(mk)        |    1039K |     146M |     148M |     143M |
    | 2-Shift            |      60K |     164M |     165M |     134M |
    | 2-ShiftSmart(mk)   |    1166K |     272M |     273M |      17M |
    | 2-ShiftSmart       |      67K |     232M |     232M |      66M |
    \----------------------------------------------------------------/

Best makespan.....: 24
N. of Iterations..: 7112153825
Total runtime.....: 3600.03s
Using Python-MIP package version 1.6.8
Começo a execução
    /-------------------------------------------------------------------------------\
    |    Makespan   |   Free Jobs   | Free Machines |    It. Time   |   Total Time  | 
    |-------------------------------------------------------------------------------|
    \-------------------------------------------------------------------------------/
Terminou a execução
