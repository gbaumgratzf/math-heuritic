Instance....: instances/I_150_25_S_1-124_8.txt
Algorithm...: Simulated Annealing (alpha=0.990, saMax=10M, t0=1)
Other params: maxIters=100M, seed=-1, timeLimit=3600.00s

    /--------------------------------------------------------\
    |     Iter |   RDP(%) |       S* |       S' |       Time | 
    |----------|----------|----------|----------|------------|
    |        0 |        - |     1907 |     1907 |       0.00 | s0
    |        0 |        - |     1860 |     1860 |       0.01 | *
    |        6 |        - |     1767 |     1767 |       0.01 | *
    |        8 |        - |     1574 |     1574 |       0.01 | *
    |       13 |        - |     1288 |     1288 |       0.01 | *
    |       19 |        - |     1286 |     1286 |       0.04 | *
    |       21 |        - |     1242 |     1242 |       0.04 | *
    |       22 |        - |     1158 |     1158 |       0.04 | *
    |       26 |        - |     1090 |     1090 |       0.04 | *
    |       27 |        - |     1057 |     1057 |       0.04 | *
    |       30 |        - |      955 |      955 |       0.04 | *
    |       33 |        - |      952 |      952 |       0.04 | *
    |       35 |        - |      942 |      942 |       0.04 | *
    |       38 |        - |      910 |      910 |       0.04 | *
    |       39 |        - |      899 |      899 |       0.04 | *
    |       60 |        - |      849 |      849 |       0.04 | *
    |       68 |        - |      846 |      846 |       0.04 | *
    |       70 |        - |      823 |      823 |       0.05 | *
    |       72 |        - |      820 |      820 |       0.05 | *
    |       77 |        - |      789 |      789 |       0.05 | *
    |       81 |        - |      786 |      786 |       0.05 | *
    |       82 |        - |      784 |      784 |       0.05 | *
    |       91 |        - |      765 |      765 |       0.05 | *
    |       97 |        - |      728 |      728 |       0.05 | *
    |      104 |        - |      715 |      715 |       0.05 | *
    |      107 |        - |      714 |      714 |       0.05 | *
    |      110 |        - |      699 |      699 |       0.05 | *
    |      120 |        - |      687 |      687 |       0.05 | *
    |      123 |        - |      683 |      683 |       0.05 | *
    |      129 |        - |      655 |      655 |       0.05 | *
    |      134 |        - |      643 |      643 |       0.05 | *
    |      142 |        - |      636 |      636 |       0.05 | *
    |      147 |        - |      628 |      628 |       0.06 | *
    |      150 |        - |      620 |      620 |       0.06 | *
    |      153 |        - |      615 |      615 |       0.06 | *
    |      163 |        - |      613 |      613 |       0.06 | *
    |      198 |        - |      611 |      611 |       0.06 | *
    |      203 |        - |      609 |      609 |       0.06 | *
    |      206 |        - |      587 |      587 |       0.06 | *
    |      221 |        - |      585 |      585 |       0.06 | *
    |      223 |        - |      583 |      583 |       0.06 | *
    |      225 |        - |      574 |      574 |       0.06 | *
    |      239 |        - |      571 |      571 |       0.06 | *
    |      245 |        - |      564 |      564 |       0.06 | *
    |      247 |        - |      563 |      563 |       0.06 | *
    |      253 |        - |      561 |      561 |       0.06 | *
    |      267 |        - |      553 |      553 |       0.06 | *
    |      292 |        - |      550 |      550 |       0.07 | *
    |      295 |        - |      548 |      548 |       0.07 | *
    |      306 |        - |      546 |      546 |       0.07 | *
    |      335 |        - |      543 |      543 |       0.07 | *
    |      337 |        - |      539 |      539 |       0.07 | *
    |      345 |        - |      537 |      537 |       0.07 | *
    |      351 |        - |      535 |      535 |       0.07 | *
    |      352 |        - |      532 |      532 |       0.07 | *
    |      355 |        - |      530 |      530 |       0.07 | *
    |      359 |        - |      524 |      524 |       0.07 | *
    |      373 |        - |      518 |      518 |       0.07 | *
    |      374 |        - |      516 |      516 |       0.07 | *
    |      399 |        - |      515 |      515 |       0.07 | *
    |      400 |        - |      514 |      514 |       0.07 | *
    |      403 |        - |      511 |      511 |       0.07 | *
    |      424 |        - |      510 |      510 |       0.07 | *
    |      439 |        - |      508 |      508 |       0.07 | *
    |      450 |        - |      505 |      505 |       0.07 | *
    |      452 |        - |      497 |      497 |       0.08 | *
    |      454 |        - |      496 |      496 |       0.08 | *
    |      488 |        - |      491 |      491 |       0.08 | *
    |      495 |        - |      490 |      490 |       0.08 | *
    |      502 |        - |      489 |      489 |       0.08 | *
    |      508 |        - |      488 |      488 |       0.08 | *
    |      516 |        - |      480 |      480 |       0.08 | *
    |      534 |        - |      478 |      478 |       0.08 | *
    |      535 |        - |      477 |      477 |       0.08 | *
    |      543 |        - |      469 |      469 |       0.08 | *
    |      554 |        - |      467 |      467 |       0.08 | *
    |      562 |        - |      465 |      465 |       0.08 | *
    |      572 |        - |      464 |      464 |       0.08 | *
    |      582 |        - |      462 |      462 |       0.08 | *
    |      597 |        - |      455 |      455 |       0.08 | *
    |      604 |        - |      453 |      453 |       0.08 | *
    |      610 |        - |      452 |      452 |       0.08 | *
    |      630 |        - |      449 |      449 |       0.08 | *
    |      659 |        - |      448 |      448 |       0.08 | *
    |      704 |        - |      447 |      447 |       0.09 | *
    |      709 |        - |      444 |      444 |       0.09 | *
    |      742 |        - |      443 |      443 |       0.09 | *
    |      747 |        - |      442 |      442 |       0.09 | *
    |      797 |        - |      441 |      441 |       0.09 | *
    |      798 |        - |      436 |      436 |       0.09 | *
    |      813 |        - |      435 |      435 |       0.09 | *
    |      830 |        - |      433 |      433 |       0.09 | *
    |      846 |        - |      429 |      429 |       0.09 | *
    |      854 |        - |      425 |      425 |       0.09 | *
    |      917 |        - |      421 |      421 |       0.09 | *
    |      935 |        - |      416 |      416 |       0.09 | *
    |      945 |        - |      411 |      411 |       0.09 | *
    |      949 |        - |      407 |      407 |       0.09 | *
    |     1075 |        - |      406 |      406 |       0.09 | *
    |     1106 |        - |      405 |      405 |       0.09 | *
    |     1114 |        - |      403 |      403 |       0.09 | *
    |     1120 |        - |      401 |      401 |       0.09 | *
    |     1157 |        - |      399 |      399 |       0.10 | *
    |     1158 |        - |      398 |      398 |       0.10 | *
    |     1171 |        - |      395 |      395 |       0.10 | *
    |     1213 |        - |      394 |      394 |       0.10 | *
    |     1226 |        - |      393 |      393 |       0.10 | *
    |     1257 |        - |      389 |      389 |       0.10 | *
    |     1288 |        - |      387 |      387 |       0.10 | *
    |     1300 |        - |      386 |      386 |       0.10 | *
    |     1313 |        - |      384 |      384 |       0.10 | *
    |     1352 |        - |      383 |      383 |       0.10 | *
    |     1360 |        - |      381 |      381 |       0.10 | *
    |     1374 |        - |      378 |      378 |       0.10 | *
    |     1388 |        - |      377 |      377 |       0.10 | *
    |     1401 |        - |      376 |      376 |       0.10 | *
    |     1416 |        - |      375 |      375 |       0.10 | *
    |     1439 |        - |      374 |      374 |       0.10 | *
    |     1448 |        - |      368 |      368 |       0.10 | *
    |     1452 |        - |      367 |      367 |       0.10 | *
    |     1544 |        - |      366 |      366 |       0.10 | *
    |     1682 |        - |      365 |      365 |       0.11 | *
    |     1683 |        - |      363 |      363 |       0.11 | *
    |     1696 |        - |      362 |      362 |       0.11 | *
    |     1723 |        - |      361 |      361 |       0.11 | *
    |     1806 |        - |      360 |      360 |       0.11 | *
    |     1823 |        - |      359 |      359 |       0.11 | *
    |     1828 |        - |      358 |      358 |       0.11 | *
    |     1893 |        - |      345 |      345 |       0.11 | *
    |     1903 |        - |      344 |      344 |       0.11 | *
    |     1922 |        - |      343 |      343 |       0.11 | *
    |     1960 |        - |      341 |      341 |       0.11 | *
    |     1999 |        - |      340 |      340 |       0.11 | *
    |     2021 |        - |      339 |      339 |       0.11 | *
    |     2023 |        - |      338 |      338 |       0.11 | *
    |     2042 |        - |      335 |      335 |       0.11 | *
    |     2069 |        - |      334 |      334 |       0.11 | *
    |     2104 |        - |      333 |      333 |       0.12 | *
    |     2115 |        - |      329 |      329 |       0.12 | *
    |     2151 |        - |      328 |      328 |       0.12 | *
    |     2209 |        - |      327 |      327 |       0.12 | *
    |     2214 |        - |      324 |      324 |       0.12 | *
    |     2272 |        - |      323 |      323 |       0.12 | *
    |     2274 |        - |      322 |      322 |       0.12 | *
    |     2382 |        - |      321 |      321 |       0.12 | *
    |     2441 |        - |      320 |      320 |       0.12 | *
    |     2495 |        - |      318 |      318 |       0.12 | *
    |     2517 |        - |      313 |      313 |       0.12 | *
    |     2647 |        - |      310 |      310 |       0.12 | *
    |     2651 |        - |      309 |      309 |       0.12 | *
    |     2696 |        - |      303 |      303 |       0.12 | *
    |     2704 |        - |      302 |      302 |       0.12 | *
    |     2778 |        - |      301 |      301 |       0.12 | *
    |     2789 |        - |      297 |      297 |       0.12 | *
    |     2864 |        - |      296 |      296 |       0.12 | *
    |     2971 |        - |      295 |      295 |       0.13 | *
    |     3018 |        - |      294 |      294 |       0.13 | *
    |     3029 |        - |      291 |      291 |       0.13 | *
    |     3114 |        - |      290 |      290 |       0.13 | *
    |     3116 |        - |      289 |      289 |       0.13 | *
    |     3305 |        - |      287 |      287 |       0.13 | *
    |     3323 |        - |      286 |      286 |       0.13 | *
    |     3493 |        - |      285 |      285 |       0.13 | *
    |     3824 |        - |      283 |      283 |       0.13 | *
    |     3826 |        - |      282 |      282 |       0.13 | *
    |     4105 |        - |      279 |      279 |       0.14 | *
    |     4186 |        - |      277 |      277 |       0.14 | *
    |     4377 |        - |      276 |      276 |       0.14 | *
    |     4540 |        - |      275 |      275 |       0.14 | *
    |     4665 |        - |      274 |      274 |       0.14 | *
    |     4707 |        - |      272 |      272 |       0.14 | *
    |     4973 |        - |      271 |      271 |       0.14 | *
    |     4987 |        - |      270 |      270 |       0.14 | *
    |     5000 |        - |      269 |      269 |       0.14 | *
    |     5156 |        - |      266 |      266 |       0.14 | *
    |     5254 |        - |      265 |      265 |       0.14 | *
    |     5277 |        - |      264 |      264 |       0.14 | *
    |     5314 |        - |      263 |      263 |       0.14 | *
    |     5655 |        - |      261 |      261 |       0.15 | *
    |     5668 |        - |      257 |      257 |       0.15 | *
    |     5730 |        - |      256 |      256 |       0.15 | *
    |     6495 |        - |      255 |      255 |       0.15 | *
    |     6618 |        - |      253 |      253 |       0.15 | *
    |     6706 |        - |      252 |      252 |       0.15 | *
    |     6718 |        - |      251 |      251 |       0.15 | *
    |     6884 |        - |      248 |      248 |       0.15 | *
    |     6942 |        - |      247 |      247 |       0.16 | *
    |     7001 |        - |      246 |      246 |       0.16 | *
    |     7002 |        - |      244 |      244 |       0.16 | *
    |     7023 |        - |      243 |      243 |       0.16 | *
    |     7248 |        - |      242 |      242 |       0.16 | *
    |     7518 |        - |      241 |      241 |       0.16 | *
    |     7771 |        - |      240 |      240 |       0.16 | *
    |     8128 |        - |      239 |      239 |       0.16 | *
    |     8497 |        - |      238 |      238 |       0.16 | *
    |     8776 |        - |      237 |      237 |       0.16 | *
    |     9529 |        - |      236 |      236 |       0.17 | *
    |     9775 |        - |      234 |      234 |       0.17 | *
    |     9808 |        - |      232 |      232 |       0.17 | *
    |      10K |        - |      231 |      231 |       0.17 | *
    |      10K |        - |      230 |      230 |       0.17 | *
    |      10K |        - |      228 |      228 |       0.17 | *
    |      11K |        - |      227 |      227 |       0.17 | *
    |      11K |        - |      226 |      226 |       0.17 | *
    |      11K |        - |      225 |      225 |       0.18 | *
    |      12K |        - |      223 |      223 |       0.18 | *
    |      12K |        - |      222 |      222 |       0.18 | *
    |      12K |        - |      221 |      221 |       0.18 | *
    |      12K |        - |      220 |      220 |       0.18 | *
    |      12K |        - |      219 |      219 |       0.18 | *
    |      13K |        - |      218 |      218 |       0.18 | *
    |      13K |        - |      217 |      217 |       0.18 | *
    |      13K |        - |      215 |      215 |       0.18 | *
    |      14K |        - |      214 |      214 |       0.19 | *
    |      14K |        - |      213 |      213 |       0.19 | *
    |      14K |        - |      212 |      212 |       0.19 | *
    |      14K |        - |      211 |      211 |       0.19 | *
    |      15K |        - |      210 |      210 |       0.19 | *
    |      16K |        - |      208 |      208 |       0.19 | *
    |      16K |        - |      207 |      207 |       0.20 | *
    |      17K |        - |      206 |      206 |       0.20 | *
    |      17K |        - |      205 |      205 |       0.20 | *
    |      17K |        - |      204 |      204 |       0.20 | *
    |      18K |        - |      202 |      202 |       0.20 | *
    |      18K |        - |      201 |      201 |       0.20 | *
    |      20K |        - |      200 |      200 |       0.21 | *
    |      20K |        - |      199 |      199 |       0.21 | *
    |      20K |        - |      197 |      197 |       0.21 | *
    |      21K |        - |      196 |      196 |       0.21 | *
    |      21K |        - |      195 |      195 |       0.21 | *
    |      21K |        - |      194 |      194 |       0.21 | *
    |      21K |        - |      193 |      193 |       0.21 | *
    |      22K |        - |      190 |      190 |       0.21 | *
    |      23K |        - |      189 |      189 |       0.22 | *
    |      23K |        - |      188 |      188 |       0.22 | *
    |      23K |        - |      187 |      187 |       0.22 | *
    |      23K |        - |      186 |      186 |       0.22 | *
    |      23K |        - |      185 |      185 |       0.22 | *
    |      23K |        - |      184 |      184 |       0.22 | *
    |      25K |        - |      183 |      183 |       0.23 | *
    |      25K |        - |      181 |      181 |       0.23 | *
    |      26K |        - |      180 |      180 |       0.23 | *
    |      26K |        - |      178 |      178 |       0.23 | *
    |      26K |        - |      177 |      177 |       0.23 | *
    |      28K |        - |      176 |      176 |       0.23 | *
    |      29K |        - |      175 |      175 |       0.23 | *
    |      29K |        - |      174 |      174 |       0.23 | *
    |      30K |        - |      173 |      173 |       0.24 | *
    |      31K |        - |      172 |      172 |       0.24 | *
    |      37K |        - |      171 |      171 |       0.25 | *
    |      37K |        - |      170 |      170 |       0.25 | *
    |      39K |        - |      169 |      169 |       0.25 | *
    |      42K |        - |      168 |      168 |       0.25 | *
    |      43K |        - |      167 |      167 |       0.26 | *
    |      52K |        - |      166 |      166 |       0.27 | *
    |      54K |        - |      165 |      165 |       0.27 | *
    |      55K |        - |      164 |      164 |       0.27 | *
    |      56K |        - |      163 |      163 |       0.27 | *
    |      64K |        - |      161 |      161 |       0.28 | *
    |      69K |        - |      160 |      160 |       0.29 | *
    |      70K |        - |      158 |      158 |       0.29 | *
    |      77K |        - |      157 |      157 |       0.29 | *
    |      77K |        - |      156 |      156 |       0.29 | *
    |      84K |        - |      155 |      155 |       0.30 | *
    |      84K |        - |      154 |      154 |       0.30 | *
    |      86K |        - |      153 |      153 |       0.30 | *
    |      88K |        - |      152 |      152 |       0.31 | *
    |      88K |        - |      150 |      150 |       0.31 | *
    |      89K |        - |      149 |      149 |       0.31 | *
    |      94K |        - |      148 |      148 |       0.31 | *
    |      95K |        - |      147 |      147 |       0.31 | *
    |     100K |        - |      146 |      146 |       0.32 | *
    |     102K |        - |      145 |      145 |       0.32 | *
    |     111K |        - |      143 |      143 |       0.33 | *
    |     116K |        - |      142 |      142 |       0.34 | *
    |     117K |        - |      141 |      141 |       0.34 | *
    |     121K |        - |      140 |      140 |       0.35 | *
    |     129K |        - |      139 |      139 |       0.37 | *
    |     163K |        - |      137 |      137 |       0.48 | *
    |     164K |        - |      136 |      136 |       0.48 | *
    |     167K |        - |      135 |      135 |       0.49 | *
    |     169K |        - |      134 |      134 |       0.50 | *
    |     169K |        - |      133 |      133 |       0.50 | *
    |     169K |        - |      132 |      132 |       0.50 | *
    |     228K |        - |      131 |      131 |       0.68 | *
    |     233K |        - |      130 |      130 |       0.69 | *
    |     237K |        - |      129 |      129 |       0.70 | *
    |     237K |        - |      128 |      128 |       0.71 | *
    |     253K |        - |      127 |      127 |       0.75 | *
    |     255K |        - |      126 |      126 |       0.75 | *
    |     256K |        - |      125 |      125 |       0.75 | *
    |     257K |        - |      124 |      124 |       0.76 | *
    |     263K |        - |      123 |      123 |       0.77 | *
    |     264K |        - |      122 |      122 |       0.77 | *
    |     268K |        - |      121 |      121 |       0.78 | *
    |     269K |        - |      120 |      120 |       0.78 | *
    |     318K |        - |      119 |      119 |       0.86 | *
    |     348K |        - |      118 |      118 |       0.92 | *
    |     372K |        - |      117 |      117 |       0.95 | *
    |     743K |        - |      116 |      116 |       1.40 | *
    |     745K |        - |      115 |      115 |       1.40 | *
    |     805K |        - |      114 |      114 |       1.47 | *
    |     847K |        - |      113 |      113 |       1.51 | *
    |     895K |        - |      112 |      112 |       1.56 | *
    |    1005K |        - |      111 |      111 |       1.68 | *
    |    1007K |        - |      110 |      110 |       1.68 | *
    |    1050K |        - |      109 |      109 |       1.72 | *
    |    1106K |        - |      108 |      108 |       1.78 | *
    |    1522K |        - |      107 |      107 |       2.16 | *
    |    1529K |        - |      106 |      106 |       2.16 | *
    |    1547K |        - |      105 |      105 |       2.18 | *
    |    1554K |        - |      104 |      104 |       2.19 | *
    |    1736K |        - |      103 |      103 |       2.33 | *
    |    2002K |        - |      102 |      102 |       2.55 | *
    |    2008K |        - |      101 |      101 |       2.56 | *
    |    2232K |        - |      100 |      100 |       2.79 | *
    |    2232K |        - |       99 |       99 |       2.79 | *
    |    2656K |        - |       98 |       98 |       3.25 | *
    |    5064K |        - |       97 |       97 |       5.52 | *
    |    5092K |        - |       96 |       96 |       5.57 | *
    |    6761K |        - |       95 |       95 |       7.38 | *
    |    9188K |        - |       94 |       94 |       9.32 | *
    |    9190K |        - |       93 |       93 |       9.32 | *
    |    9195K |        - |       92 |       92 |       9.33 | *
    |    9831K |        - |       91 |       91 |       9.76 | *
    |      15M |        - |       90 |       90 |      13.47 | *
    |      19M |        - |       89 |       89 |      15.76 | *
    |      19M |        - |       88 |       88 |      15.82 | *
    |      19M |        - |       87 |       87 |      15.94 | *
    |      19M |        - |       86 |       86 |      15.95 | *
    |      57M |        - |       85 |       85 |      40.94 | *
    |     197M |        - |       84 |       84 |     133.53 | *
    |     201M |        - |       83 |       83 |     136.28 | *
    |     410M |        - |       82 |       82 |     273.06 | *
    |     486M |        - |       81 |       81 |     322.43 | *
    |     541M |        - |       80 |       80 |     359.03 | *
    |     858M |        - |       79 |       79 |     566.25 | *
    \--------------------------------------------------------/

Neighborhoods statistics (values in %):

    /----------------------------------------------------------------\
    | Move               | Improvs. | Sideways |  Accepts |  Rejects |
    |--------------------|----------|----------|----------|----------|
    | Shift(mk)          |      15K |      17K |      36K |     231M |
    | Shift              |      602 |     915K |     928K |     230M |
    | ShiftSmart(mk)     |      64K |      78K |     159K |     231M |
    | ShiftSmart         |     2511 |    3655K |    3708K |     227M |
    | SimpSwap(mk)       |      107 |      108 |      243 |     231M |
    | SimpSwap           |        8 |      826 |      918 |     231M |
    | SimpSwapSmart(mk)  |      495 |      508 |     1128 |     231M |
    | SimpSwapSmart      |       47 |     4076 |     4548 |     231M |
    | Swap(mk)           |       37 |       50 |       97 |     231M |
    | Swap               |        1 |      630 |      653 |     231M |
    | SwapSmart(mk)      |      504 |     1082 |     1737 |     231M |
    | SwapSmart          |       43 |      18K |      18K |     231M |
    | Switch(mk)         |     2766 |      34M |      34M |     197M |
    | Switch             |      135 |      40M |      40M |     191M |
    | SwitchSmart(mk)    |      11K |      16K |      31K |     231M |
    | SwitchSmart        |      425 |     238K |     247K |     231M |
    | TaskMove(mk)       |     1266 |     1683 |     3480 |     231M |
    | TaskMove           |       59 |      23K |      24K |     231M |
    | TaskMoveSmart(mk)  |     6777 |     9419 |      19K |     231M |
    | TaskMoveSmart      |      289 |     128K |     132K |     231M |
    | 2-Shift(mk)        |     7845 |    5987K |    5996K |     225M |
    | 2-Shift            |      295 |    9264K |    9270K |     222M |
    | 2-ShiftSmart(mk)   |      26K |    2461K |    2490K |     229M |
    | 2-ShiftSmart       |     1069 |      14M |      15M |     217M |
    \----------------------------------------------------------------/

Best makespan.....: 79
N. of Iterations..: 5545447169
Total runtime.....: 3600.02s
Using Python-MIP package version 1.6.8
Começo a execução
    /-------------------------------------------------------------------------------\
    |    Makespan   |   Free Jobs   | Free Machines |    It. Time   |   Total Time  | 
    |-------------------------------------------------------------------------------|
    \-------------------------------------------------------------------------------/
Terminou a execução
