if __name__ == '__main__':
    # jobs = [50, 100, 150, 200, 250]
    # machines = [10, 15, 20, 25, 30]
    jobs = [6, 8, 10, 12]
    machines = [2, 3, 4, 5]
    times = [9, 49, 99, 124]
    instances = [i+1 for i in range(10)]
    total_time = '-total_time 3600'
    fanjul_init = 'pypy3 exec_master_sequence.py'
    path_save = 'spider'
    script_string = ''
    # fanjul
    save = 'spider'
    for j in jobs:
        for m in machines:
            for t in times:
                for i in instances:
                    ints = f'I_{j}_{m}_S_1-{t}_{i}.txt'
                    file_inst = f'instances/{ints}'
                    solution = f'-output solution_julho_2021/fanjul/{save}/{ints}'
                    result = f'-save_result result_julho_2021/{save}'
                    command = ' '.join([fanjul_init, file_inst, total_time, solution, result])
                    out_file = f'> output_2021/fanjul/{ints}'
                    script_string += ' '.join([command, out_file, '\n'])
    
    # init_string = 'pypy3 main.py'
    # repeat = 5

    # # fix opt
    # save = 'sls'
    # for rep in range(repeat):
    #     for j in jobs:
    #         for m in machines:
    #             for t in times:
    #                 for i in instances:
    #                     ints = f'I_{j}_{m}_S_1-{t}_{i}'
    #                     file_inst = f'instances/{ints}.txt'
    #                     solution = f'-output solution_2021/{save}/{ints}_{rep}.txt'
    #                     result = f'-save_result result_2021/{save}'
    #                     command = ' '.join([init_string, file_inst, total_time, solution, result])
    #                     out_file = f'> output_julho_2021/{save}/{ints}_{rep}.txt'
    #                     script_string += ' '.join([command, out_file, '\n'])

    # for path_save, begin, repeat in [('spider', 5, 5)]:
    #     script_string = ''
    #     # SLS
    #     sls_string = '-init_sol SLS -sls_time 3600'
    #     save = f'sls/{path_save}'
    #     for rep in range(begin, repeat+1):
    #         for j in jobs:
    #             for m in machines:
    #                 for t in times:
    #                     for i in instances:
    #                         ints = f'I_{j}_{m}_S_1-{t}_{i}'
    #                         file_inst = f'instances/{ints}.txt'
    #                         solution = f'-output solution_julho_2021/{save}/{ints}_{rep}.txt'
    #                         result = f'-save_result result_julho_2021/{save}'
    #                         command = ' '.join([init_string, file_inst, total_time, sls_string, solution, result])
    #                         out_file = f'> output_julho_2021/{save}/{ints}_{rep}.txt'
    #                         script_string += ' '.join([command, out_file, '\n'])

    file_script = open(f'script_{path_save}_2021.sh', 'w')
    file_script.write(script_string)
    file_script.close()