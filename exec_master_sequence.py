from os import system
import sys
import timeit

from util import thebest
from problem import Problem
from solution import Solution
from master_sequence import MasterSequence

import logging

def func(argv):
    logging.disable(sys.maxsize)
    file_instance = argv[1]
    total_time = 10
    seed = -1
    file_inst = file_instance.split("/")[-1]
    output = "solution_2021/fanjul/" + file_inst
    i = 2
    while i < len(argv):
        if "-total_time" == argv[i]:
            i += 1
            total_time = float(argv[i])
        if "-output" == argv[i]:
            i += 1
            output = argv[i]
        i += 1

    time_start = timeit.default_timer()
    problem = Problem(directory=file_instance)
    master = MasterSequence(problem)
    time_end = timeit.default_timer()

    job = problem.jobs
    machine = problem.machines
    num = problem.num
    instance = problem.instance
    name = "I_%d_%d_S_1-%d_%d"
    inst = name % (job, machine, num, instance)

    elapsed_time = time_end - time_start
    master_max_time = total_time - elapsed_time

    print("Time to create the master model: %.2f" % (elapsed_time))

    time_start = timeit.default_timer()
    master.run(master_max_time)
    time_end = timeit.default_timer()
    master_sequence_time = time_end - time_start

    obj = master.solution.makespan
    lower_bound = master.model.objective_bound
    # print("Makespan: {}".format(obj))

    # Save Solution
    # Path(output).mkdir(parents=True, exist_ok=True)
    # # Gets and creates the output path (if needed)
    # create_path = False
    # output_path = ""
    # for i in range(len(output) - 1, 0, -1):
    #     if output[i] == '/':
    #         output_path = output[0:i + 1]
    #         path = True
    #         break
    # if create_path:
    #     directory = os.path.dirname(output_path)
    #     if not os.path.exists(directory):
    #         os.makedirs(directory)
    master.solution.write(output)

    # Save Results
    total_time = master_sequence_time + elapsed_time
    file = open('result_2021/fanjul/%d_%d_%d_%d.csv' %
                (job, machine, num, instance), 'a+')
    file.write('%d;%.2f;%.2f;%.2f\n' %
               (
                   obj,  # C
                   master_sequence_time,  # D
                   lower_bound,  # E
                   total_time  # F
               )
               )
    file.close()


if __name__ == "__main__":
    func(sys.argv)
