Instance....: instances/I_100_10_S_1-124_5.txt
Algorithm...: Simulated Annealing (alpha=0.990, saMax=10M, t0=1)
Other params: maxIters=100M, seed=1, timeLimit=3600.00s

    /--------------------------------------------------------\
    |     Iter |   RDP(%) |       S* |       S' |       Time | 
    |----------|----------|----------|----------|------------|
    |        0 |        - |     1400 |     1400 |       0.00 | s0
    |        0 |        - |     1332 |     1332 |       0.02 | *
    |        1 |        - |     1258 |     1258 |       0.02 | *
    |        5 |        - |     1247 |     1247 |       0.02 | *
    |        7 |        - |     1191 |     1191 |       0.02 | *
    |        9 |        - |     1160 |     1160 |       0.02 | *
    |       13 |        - |     1133 |     1133 |       0.02 | *
    |       15 |        - |     1126 |     1126 |       0.02 | *
    |       19 |        - |     1105 |     1105 |       0.02 | *
    |       23 |        - |     1087 |     1087 |       0.02 | *
    |       28 |        - |     1068 |     1068 |       0.02 | *
    |       29 |        - |     1047 |     1047 |       0.02 | *
    |       39 |        - |     1045 |     1045 |       0.02 | *
    |       45 |        - |     1027 |     1027 |       0.02 | *
    |       55 |        - |     1021 |     1021 |       0.02 | *
    |       58 |        - |     1013 |     1013 |       0.02 | *
    |       62 |        - |     1000 |     1000 |       0.03 | *
    |       66 |        - |      989 |      989 |       0.03 | *
    |       72 |        - |      975 |      975 |       0.03 | *
    |       74 |        - |      964 |      964 |       0.03 | *
    |       80 |        - |      958 |      958 |       0.03 | *
    |       82 |        - |      938 |      938 |       0.03 | *
    |       85 |        - |      914 |      914 |       0.03 | *
    |       89 |        - |      903 |      903 |       0.03 | *
    |       90 |        - |      893 |      893 |       0.03 | *
    |       93 |        - |      890 |      890 |       0.03 | *
    |      105 |        - |      888 |      888 |       0.03 | *
    |      109 |        - |      867 |      867 |       0.03 | *
    |      112 |        - |      860 |      860 |       0.03 | *
    |      125 |        - |      831 |      831 |       0.03 | *
    |      131 |        - |      826 |      826 |       0.03 | *
    |      132 |        - |      825 |      825 |       0.03 | *
    |      134 |        - |      822 |      822 |       0.03 | *
    |      139 |        - |      820 |      820 |       0.03 | *
    |      141 |        - |      811 |      811 |       0.03 | *
    |      142 |        - |      806 |      806 |       0.03 | *
    |      144 |        - |      802 |      802 |       0.03 | *
    |      173 |        - |      801 |      801 |       0.03 | *
    |      178 |        - |      768 |      768 |       0.03 | *
    |      179 |        - |      755 |      755 |       0.03 | *
    |      183 |        - |      747 |      747 |       0.03 | *
    |      186 |        - |      736 |      736 |       0.03 | *
    |      201 |        - |      733 |      733 |       0.03 | *
    |      217 |        - |      715 |      715 |       0.03 | *
    |      223 |        - |      709 |      709 |       0.03 | *
    |      250 |        - |      699 |      699 |       0.03 | *
    |      259 |        - |      693 |      693 |       0.03 | *
    |      265 |        - |      689 |      689 |       0.03 | *
    |      271 |        - |      680 |      680 |       0.03 | *
    |      273 |        - |      677 |      677 |       0.03 | *
    |      295 |        - |      675 |      675 |       0.03 | *
    |      367 |        - |      669 |      669 |       0.03 | *
    |      385 |        - |      667 |      667 |       0.03 | *
    |      388 |        - |      662 |      662 |       0.03 | *
    |      433 |        - |      659 |      659 |       0.03 | *
    |      439 |        - |      652 |      652 |       0.03 | *
    |      461 |        - |      650 |      650 |       0.03 | *
    |      462 |        - |      648 |      648 |       0.03 | *
    |      466 |        - |      646 |      646 |       0.03 | *
    |      515 |        - |      642 |      642 |       0.03 | *
    |      664 |        - |      641 |      641 |       0.03 | *
    |      666 |        - |      637 |      637 |       0.03 | *
    |      682 |        - |      635 |      635 |       0.03 | *
    |      774 |        - |      634 |      634 |       0.03 | *
    |      781 |        - |      632 |      632 |       0.03 | *
    |      807 |        - |      629 |      629 |       0.03 | *
    |      808 |        - |      627 |      627 |       0.03 | *
    |      809 |        - |      622 |      622 |       0.05 | *
    |      812 |        - |      616 |      616 |       0.05 | *
    |      836 |        - |      611 |      611 |       0.05 | *
    |      927 |        - |      606 |      606 |       0.05 | *
    |      934 |        - |      603 |      603 |       0.05 | *
    |      986 |        - |      595 |      595 |       0.05 | *
    |     1087 |        - |      594 |      594 |       0.05 | *
    |     1172 |        - |      590 |      590 |       0.05 | *
    |     1207 |        - |      581 |      581 |       0.05 | *
    |     1232 |        - |      575 |      575 |       0.05 | *
    |     1250 |        - |      574 |      574 |       0.05 | *
    |     1281 |        - |      565 |      565 |       0.05 | *
    |     1299 |        - |      563 |      563 |       0.05 | *
    |     1318 |        - |      558 |      558 |       0.05 | *
    |     1360 |        - |      551 |      551 |       0.05 | *
    |     1361 |        - |      549 |      549 |       0.05 | *
    |     1367 |        - |      543 |      543 |       0.05 | *
    |     1393 |        - |      539 |      539 |       0.05 | *
    |     1438 |        - |      535 |      535 |       0.05 | *
    |     1486 |        - |      534 |      534 |       0.05 | *
    |     1535 |        - |      533 |      533 |       0.05 | *
    |     1557 |        - |      532 |      532 |       0.05 | *
    |     1579 |        - |      531 |      531 |       0.05 | *
    |     1642 |        - |      526 |      526 |       0.05 | *
    |     1703 |        - |      517 |      517 |       0.05 | *
    |     1819 |        - |      515 |      515 |       0.05 | *
    |     1915 |        - |      512 |      512 |       0.05 | *
    |     1988 |        - |      511 |      511 |       0.05 | *
    |     1990 |        - |      504 |      504 |       0.05 | *
    |     2011 |        - |      496 |      496 |       0.05 | *
    |     2129 |        - |      485 |      485 |       0.05 | *
    |     2196 |        - |      483 |      483 |       0.05 | *
    |     2201 |        - |      482 |      482 |       0.05 | *
    |     2389 |        - |      478 |      478 |       0.05 | *
    |     2494 |        - |      475 |      475 |       0.05 | *
    |     2751 |        - |      473 |      473 |       0.05 | *
    |     2753 |        - |      470 |      470 |       0.05 | *
    |     2758 |        - |      468 |      468 |       0.05 | *
    |     2989 |        - |      467 |      467 |       0.05 | *
    |     3044 |        - |      466 |      466 |       0.05 | *
    |     3230 |        - |      462 |      462 |       0.05 | *
    |     3337 |        - |      459 |      459 |       0.05 | *
    |     3374 |        - |      458 |      458 |       0.05 | *
    |     3381 |        - |      454 |      454 |       0.05 | *
    |     3417 |        - |      452 |      452 |       0.05 | *
    |     3434 |        - |      451 |      451 |       0.05 | *
    |     3552 |        - |      449 |      449 |       0.05 | *
    |     3742 |        - |      445 |      445 |       0.05 | *
    |     3812 |        - |      436 |      436 |       0.05 | *
    |     3815 |        - |      434 |      434 |       0.05 | *
    |     3823 |        - |      433 |      433 |       0.05 | *
    |     3965 |        - |      432 |      432 |       0.05 | *
    |     4020 |        - |      431 |      431 |       0.05 | *
    |     4833 |        - |      429 |      429 |       0.05 | *
    |     4861 |        - |      427 |      427 |       0.05 | *
    |     4898 |        - |      423 |      423 |       0.05 | *
    |     4912 |        - |      418 |      418 |       0.05 | *
    |     4986 |        - |      417 |      417 |       0.05 | *
    |     5003 |        - |      410 |      410 |       0.05 | *
    |     5225 |        - |      409 |      409 |       0.05 | *
    |     5825 |        - |      406 |      406 |       0.06 | *
    |     5906 |        - |      403 |      403 |       0.06 | *
    |     6410 |        - |      401 |      401 |       0.06 | *
    |     6415 |        - |      397 |      397 |       0.06 | *
    |     6452 |        - |      396 |      396 |       0.06 | *
    |     6684 |        - |      395 |      395 |       0.06 | *
    |     6731 |        - |      394 |      394 |       0.06 | *
    |     6781 |        - |      391 |      391 |       0.06 | *
    |     6813 |        - |      388 |      388 |       0.06 | *
    |     7099 |        - |      386 |      386 |       0.06 | *
    |     7116 |        - |      385 |      385 |       0.06 | *
    |     7188 |        - |      382 |      382 |       0.06 | *
    |     7780 |        - |      380 |      380 |       0.06 | *
    |     7913 |        - |      378 |      378 |       0.06 | *
    |     8787 |        - |      376 |      376 |       0.06 | *
    |     9286 |        - |      374 |      374 |       0.06 | *
    |     9387 |        - |      372 |      372 |       0.06 | *
    |     9606 |        - |      371 |      371 |       0.06 | *
    |     9716 |        - |      369 |      369 |       0.06 | *
    |     9773 |        - |      366 |      366 |       0.06 | *
    |     9798 |        - |      362 |      362 |       0.06 | *
    |     9967 |        - |      361 |      361 |       0.06 | *
    |      10K |        - |      360 |      360 |       0.06 | *
    |      10K |        - |      356 |      356 |       0.06 | *
    |      11K |        - |      355 |      355 |       0.06 | *
    |      11K |        - |      354 |      354 |       0.06 | *
    |      11K |        - |      353 |      353 |       0.06 | *
    |      11K |        - |      351 |      351 |       0.06 | *
    |      11K |        - |      348 |      348 |       0.06 | *
    |      11K |        - |      344 |      344 |       0.06 | *
    |      11K |        - |      342 |      342 |       0.06 | *
    |      12K |        - |      339 |      339 |       0.06 | *
    |      13K |        - |      336 |      336 |       0.06 | *
    |      13K |        - |      335 |      335 |       0.06 | *
    |      14K |        - |      334 |      334 |       0.06 | *
    |      14K |        - |      332 |      332 |       0.06 | *
    |      15K |        - |      329 |      329 |       0.06 | *
    |      18K |        - |      328 |      328 |       0.06 | *
    |      18K |        - |      326 |      326 |       0.06 | *
    |      19K |        - |      325 |      325 |       0.08 | *
    |      20K |        - |      322 |      322 |       0.08 | *
    |      20K |        - |      321 |      321 |       0.08 | *
    |      20K |        - |      318 |      318 |       0.08 | *
    |      21K |        - |      317 |      317 |       0.08 | *
    |      27K |        - |      315 |      315 |       0.08 | *
    |      30K |        - |      313 |      313 |       0.08 | *
    |      31K |        - |      312 |      312 |       0.08 | *
    |      31K |        - |      311 |      311 |       0.08 | *
    |      32K |        - |      310 |      310 |       0.08 | *
    |      45K |        - |      309 |      309 |       0.09 | *
    |      45K |        - |      307 |      307 |       0.09 | *
    |      47K |        - |      306 |      306 |       0.09 | *
    |      47K |        - |      305 |      305 |       0.11 | *
    |      47K |        - |      302 |      302 |       0.11 | *
    |      50K |        - |      300 |      300 |       0.11 | *
    |      53K |        - |      298 |      298 |       0.11 | *
    |      53K |        - |      291 |      291 |       0.11 | *
    |      54K |        - |      290 |      290 |       0.11 | *
    |      64K |        - |      287 |      287 |       0.11 | *
    |      64K |        - |      286 |      286 |       0.11 | *
    |      65K |        - |      284 |      284 |       0.11 | *
    |      67K |        - |      283 |      283 |       0.11 | *
    |      70K |        - |      281 |      281 |       0.13 | *
    |      71K |        - |      279 |      279 |       0.13 | *
    |      71K |        - |      278 |      278 |       0.13 | *
    |      75K |        - |      277 |      277 |       0.13 | *
    |      76K |        - |      273 |      273 |       0.13 | *
    |      84K |        - |      272 |      272 |       0.13 | *
    |      84K |        - |      271 |      271 |       0.13 | *
    |     150K |        - |      270 |      270 |       0.16 | *
    |     153K |        - |      269 |      269 |       0.16 | *
    |     154K |        - |      267 |      267 |       0.16 | *
    |     155K |        - |      266 |      266 |       0.16 | *
    |     170K |        - |      264 |      264 |       0.17 | *
    |     170K |        - |      263 |      263 |       0.17 | *
    |     175K |        - |      262 |      262 |       0.17 | *
    |     190K |        - |      260 |      260 |       0.17 | *
    |     190K |        - |      259 |      259 |       0.19 | *
    |     239K |        - |      258 |      258 |       0.20 | *
    |     254K |        - |      257 |      257 |       0.22 | *
    |     254K |        - |      252 |      252 |       0.22 | *
    |     261K |        - |      250 |      250 |       0.22 | *
    |     344K |        - |      249 |      249 |       0.27 | *
    |     345K |        - |      248 |      248 |       0.27 | *
    |     406K |        - |      247 |      247 |       0.30 | *
    |     407K |        - |      246 |      246 |       0.30 | *
    |     455K |        - |      245 |      245 |       0.31 | *
    |     483K |        - |      244 |      244 |       0.33 | *
    |     484K |        - |      243 |      243 |       0.33 | *
    |     486K |        - |      241 |      241 |       0.33 | *
    |     486K |        - |      240 |      240 |       0.33 | *
    |     944K |        - |      239 |      239 |       0.48 | *
    |     999K |        - |      238 |      238 |       0.50 | *
    |    1348K |        - |      236 |      236 |       0.61 | *
    |    1348K |        - |      235 |      235 |       0.61 | *
    |    1449K |        - |      233 |      233 |       0.66 | *
    |    1616K |        - |      231 |      231 |       0.70 | *
    |    1667K |        - |      229 |      229 |       0.72 | *
    |    1696K |        - |      228 |      228 |       0.73 | *
    |    1721K |        - |      227 |      227 |       0.73 | *
    |    2948K |        - |      226 |      226 |       1.14 | *
    |    2981K |        - |      225 |      225 |       1.16 | *
    |    5310K |        - |      224 |      224 |       1.94 | *
    |    5330K |        - |      222 |      222 |       1.94 | *
    |    5428K |        - |      221 |      221 |       1.97 | *
    |    5505K |        - |      220 |      220 |       2.00 | *
    |    6071K |        - |      219 |      219 |       2.19 | *
    |    6081K |        - |      218 |      218 |       2.19 | *
    |    8350K |        - |      217 |      217 |       2.92 | *
    |    9089K |        - |      216 |      216 |       3.17 | *
    |    9267K |        - |      215 |      215 |       3.24 | *
    |      10M |        - |      214 |      214 |       3.55 | *
    |      22M |        - |      213 |      213 |       7.71 | *
    |      47M |        - |      212 |      212 |      17.07 | *
    |      47M |        - |      211 |      211 |      17.07 | *
    |      47M |        - |      210 |      210 |      17.16 | *
    \--------------------------------------------------------/

Neighborhoods statistics (values in %):

    /----------------------------------------------------------------\
    | Move               | Improvs. | Sideways |  Accepts |  Rejects |
    |--------------------|----------|----------|----------|----------|
    | Shift(mk)          |     6200 |      94K |     102K |     379M |
    | Shift              |      624 |     197K |     203K |     379M |
    | ShiftSmart(mk)     |      50K |     838K |     902K |     378M |
    | ShiftSmart         |     5023 |    1643K |    1690K |     377M |
    | SimpSwap(mk)       |      229 |       27 |      274 |     379M |
    | SimpSwap           |       53 |     104K |     104K |     379M |
    | SimpSwapSmart(mk)  |     2344 |      222 |     2764 |     379M |
    | SimpSwapSmart      |      387 |     937K |     938K |     378M |
    | Swap(mk)           |       11 |        8 |       21 |     379M |
    | Swap               |        3 |     1401 |     1410 |     379M |
    | SwapSmart(mk)      |      522 |      464 |     1110 |     379M |
    | SwapSmart          |      108 |     106K |     107K |     379M |
    | Switch(mk)         |     1992 |      32M |      32M |     347M |
    | Switch             |      223 |      39M |      39M |     340M |
    | SwitchSmart(mk)    |      17K |     8822 |      34K |     379M |
    | SwitchSmart        |     1717 |     165K |     177K |     379M |
    | TaskMove(mk)       |      296 |      408 |      835 |     379M |
    | TaskMove           |       32 |      15K |      15K |     379M |
    | TaskMoveSmart(mk)  |     2561 |     3138 |     6710 |     379M |
    | TaskMoveSmart      |      366 |     177K |     179K |     379M |
    | 2-Shift(mk)        |     1999 |    2988K |    2990K |     376M |
    | 2-Shift            |      201 |    4810K |    4812K |     375M |
    | 2-ShiftSmart(mk)   |      12K |      14M |      14M |     365M |
    | 2-ShiftSmart       |     1126 |    9660K |    9673K |     369M |
    \----------------------------------------------------------------/

Best makespan.....: 210
N. of Iterations..: 9101657801
Total runtime.....: 3600.02s
