from util import thebest


def read_file(exec_name, job, machine, num, inst):
    name_file = f'result_2021/{exec_name}/{job}_{machine}_{num}_{inst}.csv'
    file = open(name_file)
    lines = [l[:-1].split(';') for l in file.readlines()]
    file.close()
    return lines


def get_values(values):
    best = min(values)
    n = len(values)
    average = sum(values)/n
    sum_desvio = sum([pow(x-average, 2) for x in values])/n
    standard = pow(sum_desvio/n, 0.5)
    return best, average, standard



if __name__ == '__main__':
    default_top_1 = (
        "\\begin{table}[!ht]\n"
        "    \\centering\n"
        "    \\caption{Melhor valor, media, desvio padrão e media de tempo (em segundos) da execucão do Fix-Opt utilizando o modelo de "
        "\\cite{fanjul2019reformulations} para as instancias com "
    )
    default_top_2 = (
        "}\n"
        "    \\scalebox{0.9}{\n"
        "        \\begin{tabular}{lrrrrr}\n"
        "            \\toprule\n"
        "            Instancia & Melhor & Media & Desvio Padrão & Literatura & \\emph{gap}(\\%) \\\\\n"
        "            \\midrule\n"
    )
    default_bottom_1 = (
        "            \\midrule\n"
        "            {media:28} &      &         & {standard:4,.2f} &      & {standard_gap:7,.2f} \\\\\n"
        "            \\bottomrule\n"
    )
    default_bottom_2 = (
        "        \\end{tabular}\n"
        "    }\n"
        "    \\label{tab:"
    )
    label = "total_{exec_name}_f_{job}_{machine}"
    default_bottom_3 = (
        "}\n"
        "\\end{table}\n"
    )
    caption = "${job}$ tarefas e ${machine}$ maquinas."
    jobs = [50, 100, 150, 200, 250]
    machines = [10, 15, 20, 25, 30]
    nums = [9, 49, 99, 124]
    instances = [i+1 for i in range(10)]
    format_instance = 'I\\_{job}\\_{machine}\\_S\\_1-{num}\\_{inst}'
    format_line = '            {name_instance:28} & {best:4d} & {average:7,.2f} & {standard:4,.2f} & {literature:4d} & {gap:7.2f} \\\\'
    literature = thebest()
    exec_name = 'fix_opt'
    write = ''
    counter = 0
    for job in jobs:
        for machine in machines:
            string_caption = caption.format(job=job, machine=machine)
            string_file = ''.join([default_top_1, string_caption, default_top_2])
            sum_standard = 0
            sum_gap = 0
            num_instance = 0
            for num in nums:
                for inst in instances:
                    lines = read_file(exec_name, job, machine, num, inst)
                    values = [int(l[2]) for l in lines]
                    best, average, standard = get_values(values)
                    sum_standard += standard
                    num_instance += 1
                    name_instance = format_instance.format(
                        job=job,
                        machine=machine,
                        num=num,
                        inst=inst
                    )
                    name = name_instance.replace('\\','')
                    the_best = literature[name]
                    gap = (best - the_best)/best*100
                    if gap < 0:
                        print(name_instance, gap, values, the_best)
                        counter += 1

                    sum_gap += gap
                    line = format_line.format(
                        name_instance=name_instance,
                        best=best,
                        average=average,
                        standard=standard,
                        literature=the_best,
                        gap=gap
                    )
                    string_file += line + '\n'
            average_standard = sum_standard/num_instance
            average_gap = sum_gap/num_instance
            bottom = default_bottom_1.format(
                media='\\textbf{Media}',
                standard=average_standard,
                standard_gap=average_gap
            )
            string_label = label.format(exec_name=exec_name, job=job, machine=machine)
            string_file+=''.join([bottom, default_bottom_2, string_label, default_bottom_3])
            write += string_file + '\n'
    file = open(f'table_result_{exec_name}_fanjul.tex', 'w')
    file.write(write)
    file.close()
    print(f'{counter}/1000 - {counter/10}')
